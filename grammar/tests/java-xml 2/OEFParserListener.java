// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OEFParser}.
 */
public interface OEFParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void enterDocument(OEFParser.DocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void exitDocument(OEFParser.DocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#prolog}.
	 * @param ctx the parse tree
	 */
	void enterProlog(OEFParser.PrologContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#prolog}.
	 * @param ctx the parse tree
	 */
	void exitProlog(OEFParser.PrologContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#content}.
	 * @param ctx the parse tree
	 */
	void enterContent(OEFParser.ContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#content}.
	 * @param ctx the parse tree
	 */
	void exitContent(OEFParser.ContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#element}.
	 * @param ctx the parse tree
	 */
	void enterElement(OEFParser.ElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#element}.
	 * @param ctx the parse tree
	 */
	void exitElement(OEFParser.ElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#reference}.
	 * @param ctx the parse tree
	 */
	void enterReference(OEFParser.ReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#reference}.
	 * @param ctx the parse tree
	 */
	void exitReference(OEFParser.ReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#attribute}.
	 * @param ctx the parse tree
	 */
	void enterAttribute(OEFParser.AttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#attribute}.
	 * @param ctx the parse tree
	 */
	void exitAttribute(OEFParser.AttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#chardata}.
	 * @param ctx the parse tree
	 */
	void enterChardata(OEFParser.ChardataContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#chardata}.
	 * @param ctx the parse tree
	 */
	void exitChardata(OEFParser.ChardataContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#misc}.
	 * @param ctx the parse tree
	 */
	void enterMisc(OEFParser.MiscContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#misc}.
	 * @param ctx the parse tree
	 */
	void exitMisc(OEFParser.MiscContext ctx);
}