// Generated from OEFLexer.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TITLE=1, LANGUAGE=2, AUTHOR=3, EMAIL=4, COMPUTEANSWER=5, PRECISION=6, 
		RANGEDECLARATION=7, INTEGER=8, S=9, CB=10, CP=11, RANDINT=12, DRAW=13, 
		IF=14, BSLASH=15, ID=16, CR_OTHER=17;
	public static final int
		CALLREFERENCE=1;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE", "CALLREFERENCE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"TITLE", "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
			"RANGEDECLARATION", "INTEGER", "OB", "OP", "S", "CB", "CP", "RANDINT", 
			"DRAW", "IF", "BSLASH", "ID", "CR_OTHER"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, "'}'", "')'", 
			null, null, null, "'\\'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "TITLE", "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
			"RANGEDECLARATION", "INTEGER", "S", "CB", "CP", "RANDINT", "DRAW", "IF", 
			"BSLASH", "ID", "CR_OTHER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public OEFLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "OEFLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\23\u00f8\b\1\b\1"+
		"\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t"+
		"\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4"+
		"\22\t\22\4\23\t\23\4\24\t\24\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\63\n"+
		"\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3D\n"+
		"\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4S\n\4\3\4\3"+
		"\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5a\n\5\3\5\3\5\3\5\3\5\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6w\n"+
		"\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7"+
		"\u0089\n\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a2\n\b\3\b\3\b\3\b\3\b\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00b2\n\t\3\t\3\t\3\t\3\t\3\n\3\n"+
		"\3\13\3\13\3\f\6\f\u00bd\n\f\r\f\16\f\u00be\3\r\3\r\3\r\3\r\3\16\3\16"+
		"\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00d2\n\17"+
		"\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00df\n\20"+
		"\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\5\21\u00ea\n\21\3\21\3\21"+
		"\3\21\3\21\3\22\3\22\3\23\6\23\u00f3\n\23\r\23\16\23\u00f4\3\24\3\24\2"+
		"\2\25\4\3\6\4\b\5\n\6\f\7\16\b\20\t\22\n\24\2\26\2\30\13\32\f\34\r\36"+
		"\16 \17\"\20$\21&\22(\23\4\2\3\4\5\2\13\f\17\17\"\"\4\2C\\c|\2\u0101\2"+
		"\4\3\2\2\2\2\6\3\2\2\2\2\b\3\2\2\2\2\n\3\2\2\2\2\f\3\2\2\2\2\16\3\2\2"+
		"\2\2\20\3\2\2\2\2\22\3\2\2\2\2\30\3\2\2\2\3\32\3\2\2\2\3\34\3\2\2\2\3"+
		"\36\3\2\2\2\3 \3\2\2\2\3\"\3\2\2\2\3$\3\2\2\2\3&\3\2\2\2\3(\3\2\2\2\4"+
		"*\3\2\2\2\68\3\2\2\2\bI\3\2\2\2\nX\3\2\2\2\ff\3\2\2\2\16|\3\2\2\2\20\u008e"+
		"\3\2\2\2\22\u00a7\3\2\2\2\24\u00b7\3\2\2\2\26\u00b9\3\2\2\2\30\u00bc\3"+
		"\2\2\2\32\u00c0\3\2\2\2\34\u00c4\3\2\2\2\36\u00c8\3\2\2\2 \u00d7\3\2\2"+
		"\2\"\u00e4\3\2\2\2$\u00ef\3\2\2\2&\u00f2\3\2\2\2(\u00f6\3\2\2\2*+\7^\2"+
		"\2+,\7v\2\2,-\7k\2\2-.\7v\2\2./\7n\2\2/\60\7g\2\2\60\62\3\2\2\2\61\63"+
		"\5\30\f\2\62\61\3\2\2\2\62\63\3\2\2\2\63\64\3\2\2\2\64\65\5\24\n\2\65"+
		"\66\3\2\2\2\66\67\b\2\2\2\67\5\3\2\2\289\7^\2\29:\7n\2\2:;\7c\2\2;<\7"+
		"p\2\2<=\7i\2\2=>\7w\2\2>?\7c\2\2?@\7i\2\2@A\7g\2\2AC\3\2\2\2BD\5\30\f"+
		"\2CB\3\2\2\2CD\3\2\2\2DE\3\2\2\2EF\5\24\n\2FG\3\2\2\2GH\b\3\2\2H\7\3\2"+
		"\2\2IJ\7^\2\2JK\7c\2\2KL\7w\2\2LM\7v\2\2MN\7j\2\2NO\7q\2\2OP\7t\2\2PR"+
		"\3\2\2\2QS\5\30\f\2RQ\3\2\2\2RS\3\2\2\2ST\3\2\2\2TU\5\24\n\2UV\3\2\2\2"+
		"VW\b\4\2\2W\t\3\2\2\2XY\7^\2\2YZ\7g\2\2Z[\7o\2\2[\\\7c\2\2\\]\7k\2\2]"+
		"^\7n\2\2^`\3\2\2\2_a\5\30\f\2`_\3\2\2\2`a\3\2\2\2ab\3\2\2\2bc\5\24\n\2"+
		"cd\3\2\2\2de\b\5\2\2e\13\3\2\2\2fg\7^\2\2gh\7e\2\2hi\7q\2\2ij\7o\2\2j"+
		"k\7r\2\2kl\7w\2\2lm\7v\2\2mn\7g\2\2no\7c\2\2op\7p\2\2pq\7u\2\2qr\7y\2"+
		"\2rs\7g\2\2st\7t\2\2tv\3\2\2\2uw\5\30\f\2vu\3\2\2\2vw\3\2\2\2wx\3\2\2"+
		"\2xy\5\24\n\2yz\3\2\2\2z{\b\6\2\2{\r\3\2\2\2|}\7^\2\2}~\7r\2\2~\177\7"+
		"t\2\2\177\u0080\7g\2\2\u0080\u0081\7e\2\2\u0081\u0082\7k\2\2\u0082\u0083"+
		"\7u\2\2\u0083\u0084\7k\2\2\u0084\u0085\7q\2\2\u0085\u0086\7p\2\2\u0086"+
		"\u0088\3\2\2\2\u0087\u0089\5\30\f\2\u0088\u0087\3\2\2\2\u0088\u0089\3"+
		"\2\2\2\u0089\u008a\3\2\2\2\u008a\u008b\5\24\n\2\u008b\u008c\3\2\2\2\u008c"+
		"\u008d\b\7\2\2\u008d\17\3\2\2\2\u008e\u008f\7^\2\2\u008f\u0090\7t\2\2"+
		"\u0090\u0091\7c\2\2\u0091\u0092\7p\2\2\u0092\u0093\7i\2\2\u0093\u0094"+
		"\7g\2\2\u0094\u0095\7f\2\2\u0095\u0096\7g\2\2\u0096\u0097\7e\2\2\u0097"+
		"\u0098\7n\2\2\u0098\u0099\7c\2\2\u0099\u009a\7t\2\2\u009a\u009b\7c\2\2"+
		"\u009b\u009c\7v\2\2\u009c\u009d\7k\2\2\u009d\u009e\7q\2\2\u009e\u009f"+
		"\7p\2\2\u009f\u00a1\3\2\2\2\u00a0\u00a2\5\30\f\2\u00a1\u00a0\3\2\2\2\u00a1"+
		"\u00a2\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a4\5\24\n\2\u00a4\u00a5\3"+
		"\2\2\2\u00a5\u00a6\b\b\2\2\u00a6\21\3\2\2\2\u00a7\u00a8\7^\2\2\u00a8\u00a9"+
		"\7k\2\2\u00a9\u00aa\7p\2\2\u00aa\u00ab\7v\2\2\u00ab\u00ac\7g\2\2\u00ac"+
		"\u00ad\7i\2\2\u00ad\u00ae\7g\2\2\u00ae\u00af\7t\2\2\u00af\u00b1\3\2\2"+
		"\2\u00b0\u00b2\5\30\f\2\u00b1\u00b0\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2"+
		"\u00b3\3\2\2\2\u00b3\u00b4\5\24\n\2\u00b4\u00b5\3\2\2\2\u00b5\u00b6\b"+
		"\t\2\2\u00b6\23\3\2\2\2\u00b7\u00b8\7}\2\2\u00b8\25\3\2\2\2\u00b9\u00ba"+
		"\7*\2\2\u00ba\27\3\2\2\2\u00bb\u00bd\t\2\2\2\u00bc\u00bb\3\2\2\2\u00bd"+
		"\u00be\3\2\2\2\u00be\u00bc\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf\31\3\2\2"+
		"\2\u00c0\u00c1\7\177\2\2\u00c1\u00c2\3\2\2\2\u00c2\u00c3\b\r\3\2\u00c3"+
		"\33\3\2\2\2\u00c4\u00c5\7+\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00c7\b\16\3"+
		"\2\u00c7\35\3\2\2\2\u00c8\u00c9\7t\2\2\u00c9\u00ca\7c\2\2\u00ca\u00cb"+
		"\7p\2\2\u00cb\u00cc\7f\2\2\u00cc\u00cd\7k\2\2\u00cd\u00ce\7p\2\2\u00ce"+
		"\u00cf\7v\2\2\u00cf\u00d1\3\2\2\2\u00d0\u00d2\5\30\f\2\u00d1\u00d0\3\2"+
		"\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d4\5\26\13\2\u00d4"+
		"\u00d5\3\2\2\2\u00d5\u00d6\b\17\2\2\u00d6\37\3\2\2\2\u00d7\u00d8\7^\2"+
		"\2\u00d8\u00d9\7f\2\2\u00d9\u00da\7t\2\2\u00da\u00db\7c\2\2\u00db\u00dc"+
		"\7y\2\2\u00dc\u00de\3\2\2\2\u00dd\u00df\5\30\f\2\u00de\u00dd\3\2\2\2\u00de"+
		"\u00df\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0\u00e1\5\26\13\2\u00e1\u00e2\3"+
		"\2\2\2\u00e2\u00e3\b\20\2\2\u00e3!\3\2\2\2\u00e4\u00e5\7^\2\2\u00e5\u00e6"+
		"\7k\2\2\u00e6\u00e7\7h\2\2\u00e7\u00e9\3\2\2\2\u00e8\u00ea\5\30\f\2\u00e9"+
		"\u00e8\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00ec\5\24"+
		"\n\2\u00ec\u00ed\3\2\2\2\u00ed\u00ee\b\21\2\2\u00ee#\3\2\2\2\u00ef\u00f0"+
		"\7^\2\2\u00f0%\3\2\2\2\u00f1\u00f3\t\3\2\2\u00f2\u00f1\3\2\2\2\u00f3\u00f4"+
		"\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\'\3\2\2\2\u00f6"+
		"\u00f7\13\2\2\2\u00f7)\3\2\2\2\21\2\3\62CR`v\u0088\u00a1\u00b1\u00be\u00d1"+
		"\u00de\u00e9\u00f4\4\7\3\2\6\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}