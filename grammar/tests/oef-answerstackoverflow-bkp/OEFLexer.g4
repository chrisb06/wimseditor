lexer grammar OEFLexer;


TITLE   			 : '\\title' S? OB 				-> pushMode(CALLREFERENCE);
LANGUAGE  			 : '\\language' S? OB 			-> pushMode(CALLREFERENCE);
AUTHOR  			 : '\\author' S? OB 			-> pushMode(CALLREFERENCE);
EMAIL   			 : '\\email' S? OB 				-> pushMode(CALLREFERENCE);
COMPUTEANSWER 		 : '\\computeanswer' S? OB 		-> pushMode(CALLREFERENCE);
PRECISION 			 : '\\precision' S? OB 			-> pushMode(CALLREFERENCE);
RANGEDECLARATION 	 : '\\rangedeclaration' S? OB 	-> pushMode(CALLREFERENCE);
INTEGER 			 : '\\integer' S? OB 			-> pushMode(CALLREFERENCE);


fragment OB : '{';
fragment OP : '(';
S : [ \t\r\n]+;

mode CALLREFERENCE;

  CB       : '}'          -> popMode;
  CP       : ')'          -> popMode;

  RANDINT 	: 'randint' S? OP -> pushMode(CALLREFERENCE);

  DRAW     : '\\draw' S? OP -> pushMode(CALLREFERENCE);
  IF       : '\\if' S? OB   -> pushMode(CALLREFERENCE);


  BSLASH   : '\\';
  ID       : [a-zA-Z]+;
  CR_OTHER : .;