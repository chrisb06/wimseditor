// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OEFParser}.
 */
public interface OEFParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void enterDocument(OEFParser.DocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void exitDocument(OEFParser.DocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#pre}.
	 * @param ctx the parse tree
	 */
	void enterPre(OEFParser.PreContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#pre}.
	 * @param ctx the parse tree
	 */
	void exitPre(OEFParser.PreContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#title}.
	 * @param ctx the parse tree
	 */
	void enterTitle(OEFParser.TitleContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#title}.
	 * @param ctx the parse tree
	 */
	void exitTitle(OEFParser.TitleContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#language}.
	 * @param ctx the parse tree
	 */
	void enterLanguage(OEFParser.LanguageContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#language}.
	 * @param ctx the parse tree
	 */
	void exitLanguage(OEFParser.LanguageContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#author}.
	 * @param ctx the parse tree
	 */
	void enterAuthor(OEFParser.AuthorContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#author}.
	 * @param ctx the parse tree
	 */
	void exitAuthor(OEFParser.AuthorContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#email}.
	 * @param ctx the parse tree
	 */
	void enterEmail(OEFParser.EmailContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#email}.
	 * @param ctx the parse tree
	 */
	void exitEmail(OEFParser.EmailContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#computeanswer}.
	 * @param ctx the parse tree
	 */
	void enterComputeanswer(OEFParser.ComputeanswerContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#computeanswer}.
	 * @param ctx the parse tree
	 */
	void exitComputeanswer(OEFParser.ComputeanswerContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#precision}.
	 * @param ctx the parse tree
	 */
	void enterPrecision(OEFParser.PrecisionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#precision}.
	 * @param ctx the parse tree
	 */
	void exitPrecision(OEFParser.PrecisionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#rangedeclaration}.
	 * @param ctx the parse tree
	 */
	void enterRangedeclaration(OEFParser.RangedeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#rangedeclaration}.
	 * @param ctx the parse tree
	 */
	void exitRangedeclaration(OEFParser.RangedeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#integer}.
	 * @param ctx the parse tree
	 */
	void enterInteger(OEFParser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#integer}.
	 * @param ctx the parse tree
	 */
	void exitInteger(OEFParser.IntegerContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(OEFParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(OEFParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#commandDraw}.
	 * @param ctx the parse tree
	 */
	void enterCommandDraw(OEFParser.CommandDrawContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#commandDraw}.
	 * @param ctx the parse tree
	 */
	void exitCommandDraw(OEFParser.CommandDrawContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#commandIf}.
	 * @param ctx the parse tree
	 */
	void enterCommandIf(OEFParser.CommandIfContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#commandIf}.
	 * @param ctx the parse tree
	 */
	void exitCommandIf(OEFParser.CommandIfContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#randint}.
	 * @param ctx the parse tree
	 */
	void enterRandint(OEFParser.RandintContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#randint}.
	 * @param ctx the parse tree
	 */
	void exitRandint(OEFParser.RandintContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#callreference}.
	 * @param ctx the parse tree
	 */
	void enterCallreference(OEFParser.CallreferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#callreference}.
	 * @param ctx the parse tree
	 */
	void exitCallreference(OEFParser.CallreferenceContext ctx);
}