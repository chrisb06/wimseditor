// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TITLE=1, LANGUAGE=2, AUTHOR=3, EMAIL=4, COMPUTEANSWER=5, PRECISION=6, 
		RANGEDECLARATION=7, INTEGER=8, S=9, CB=10, CP=11, RANDINT=12, DRAW=13, 
		IF=14, BSLASH=15, ID=16, CR_OTHER=17;
	public static final int
		RULE_document = 0, RULE_pre = 1, RULE_title = 2, RULE_language = 3, RULE_author = 4, 
		RULE_email = 5, RULE_computeanswer = 6, RULE_precision = 7, RULE_rangedeclaration = 8, 
		RULE_integer = 9, RULE_string = 10, RULE_commandDraw = 11, RULE_commandIf = 12, 
		RULE_randint = 13, RULE_callreference = 14;
	private static String[] makeRuleNames() {
		return new String[] {
			"document", "pre", "title", "language", "author", "email", "computeanswer", 
			"precision", "rangedeclaration", "integer", "string", "commandDraw", 
			"commandIf", "randint", "callreference"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, "'}'", "')'", 
			null, null, null, "'\\'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "TITLE", "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
			"RANGEDECLARATION", "INTEGER", "S", "CB", "CP", "RANDINT", "DRAW", "IF", 
			"BSLASH", "ID", "CR_OTHER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "OEFParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public OEFParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class DocumentContext extends ParserRuleContext {
		public PreContext pre() {
			return getRuleContext(PreContext.class,0);
		}
		public TerminalNode EOF() { return getToken(OEFParser.EOF, 0); }
		public DocumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_document; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterDocument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitDocument(this);
		}
	}

	public final DocumentContext document() throws RecognitionException {
		DocumentContext _localctx = new DocumentContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_document);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30);
			pre();
			setState(31);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreContext extends ParserRuleContext {
		public TitleContext title() {
			return getRuleContext(TitleContext.class,0);
		}
		public LanguageContext language() {
			return getRuleContext(LanguageContext.class,0);
		}
		public AuthorContext author() {
			return getRuleContext(AuthorContext.class,0);
		}
		public EmailContext email() {
			return getRuleContext(EmailContext.class,0);
		}
		public ComputeanswerContext computeanswer() {
			return getRuleContext(ComputeanswerContext.class,0);
		}
		public PrecisionContext precision() {
			return getRuleContext(PrecisionContext.class,0);
		}
		public List<TerminalNode> S() { return getTokens(OEFParser.S); }
		public TerminalNode S(int i) {
			return getToken(OEFParser.S, i);
		}
		public List<RangedeclarationContext> rangedeclaration() {
			return getRuleContexts(RangedeclarationContext.class);
		}
		public RangedeclarationContext rangedeclaration(int i) {
			return getRuleContext(RangedeclarationContext.class,i);
		}
		public List<IntegerContext> integer() {
			return getRuleContexts(IntegerContext.class);
		}
		public IntegerContext integer(int i) {
			return getRuleContext(IntegerContext.class,i);
		}
		public PreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pre; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPre(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPre(this);
		}
	}

	public final PreContext pre() throws RecognitionException {
		PreContext _localctx = new PreContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_pre);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33);
			title();
			setState(35);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==S) {
				{
				setState(34);
				match(S);
				}
			}

			setState(37);
			language();
			setState(39);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==S) {
				{
				setState(38);
				match(S);
				}
			}

			setState(41);
			author();
			setState(43);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==S) {
				{
				setState(42);
				match(S);
				}
			}

			setState(45);
			email();
			setState(47);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==S) {
				{
				setState(46);
				match(S);
				}
			}

			setState(49);
			computeanswer();
			setState(51);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==S) {
				{
				setState(50);
				match(S);
				}
			}

			setState(53);
			precision();
			setState(55);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==S) {
				{
				setState(54);
				match(S);
				}
			}

			setState(63);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==RANGEDECLARATION) {
				{
				{
				setState(57);
				rangedeclaration();
				setState(59);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==S) {
					{
					setState(58);
					match(S);
					}
				}

				}
				}
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(72);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INTEGER) {
				{
				{
				setState(66);
				integer();
				setState(68);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==S) {
					{
					setState(67);
					match(S);
					}
				}

				}
				}
				setState(74);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TitleContext extends ParserRuleContext {
		public TerminalNode TITLE() { return getToken(OEFParser.TITLE, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public TitleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_title; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterTitle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitTitle(this);
		}
	}

	public final TitleContext title() throws RecognitionException {
		TitleContext _localctx = new TitleContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_title);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			match(TITLE);
			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RANDINT) | (1L << DRAW) | (1L << IF) | (1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(78);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case RANDINT:
				case DRAW:
				case IF:
				case BSLASH:
					{
					setState(76);
					callreference();
					}
					break;
				case ID:
				case CR_OTHER:
					{
					setState(77);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(82);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(83);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LanguageContext extends ParserRuleContext {
		public TerminalNode LANGUAGE() { return getToken(OEFParser.LANGUAGE, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public LanguageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_language; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterLanguage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitLanguage(this);
		}
	}

	public final LanguageContext language() throws RecognitionException {
		LanguageContext _localctx = new LanguageContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_language);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			match(LANGUAGE);
			setState(86);
			string();
			setState(87);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AuthorContext extends ParserRuleContext {
		public TerminalNode AUTHOR() { return getToken(OEFParser.AUTHOR, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public AuthorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_author; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAuthor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAuthor(this);
		}
	}

	public final AuthorContext author() throws RecognitionException {
		AuthorContext _localctx = new AuthorContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_author);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			match(AUTHOR);
			setState(93);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID || _la==CR_OTHER) {
				{
				{
				setState(90);
				string();
				}
				}
				setState(95);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(96);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EmailContext extends ParserRuleContext {
		public TerminalNode EMAIL() { return getToken(OEFParser.EMAIL, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public EmailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_email; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterEmail(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitEmail(this);
		}
	}

	public final EmailContext email() throws RecognitionException {
		EmailContext _localctx = new EmailContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_email);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			match(EMAIL);
			setState(102);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID || _la==CR_OTHER) {
				{
				{
				setState(99);
				string();
				}
				}
				setState(104);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(105);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComputeanswerContext extends ParserRuleContext {
		public TerminalNode COMPUTEANSWER() { return getToken(OEFParser.COMPUTEANSWER, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public ComputeanswerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_computeanswer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterComputeanswer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitComputeanswer(this);
		}
	}

	public final ComputeanswerContext computeanswer() throws RecognitionException {
		ComputeanswerContext _localctx = new ComputeanswerContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_computeanswer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(COMPUTEANSWER);
			setState(108);
			string();
			setState(109);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrecisionContext extends ParserRuleContext {
		public TerminalNode PRECISION() { return getToken(OEFParser.PRECISION, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public PrecisionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precision; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPrecision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPrecision(this);
		}
	}

	public final PrecisionContext precision() throws RecognitionException {
		PrecisionContext _localctx = new PrecisionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_precision);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(PRECISION);
			setState(112);
			string();
			setState(113);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangedeclarationContext extends ParserRuleContext {
		public TerminalNode RANGEDECLARATION() { return getToken(OEFParser.RANGEDECLARATION, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public RangedeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rangedeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterRangedeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitRangedeclaration(this);
		}
	}

	public final RangedeclarationContext rangedeclaration() throws RecognitionException {
		RangedeclarationContext _localctx = new RangedeclarationContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rangedeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			match(RANGEDECLARATION);
			setState(119);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID || _la==CR_OTHER) {
				{
				{
				setState(116);
				string();
				}
				}
				setState(121);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(122);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(OEFParser.INTEGER, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public IntegerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterInteger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitInteger(this);
		}
	}

	public final IntegerContext integer() throws RecognitionException {
		IntegerContext _localctx = new IntegerContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_integer);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			match(INTEGER);
			setState(129);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RANDINT) | (1L << DRAW) | (1L << IF) | (1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(127);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case RANDINT:
				case DRAW:
				case IF:
				case BSLASH:
					{
					setState(125);
					callreference();
					}
					break;
				case ID:
				case CR_OTHER:
					{
					setState(126);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(131);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(132);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public List<TerminalNode> CR_OTHER() { return getTokens(OEFParser.CR_OTHER); }
		public TerminalNode CR_OTHER(int i) {
			return getToken(OEFParser.CR_OTHER, i);
		}
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_string);
		try {
			int _alt;
			setState(140);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CR_OTHER:
				enterOuterAlt(_localctx, 1);
				{
				setState(135); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(134);
						match(CR_OTHER);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(137); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(139);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandDrawContext extends ParserRuleContext {
		public TerminalNode DRAW() { return getToken(OEFParser.DRAW, 0); }
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public CommandDrawContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandDraw; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCommandDraw(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCommandDraw(this);
		}
	}

	public final CommandDrawContext commandDraw() throws RecognitionException {
		CommandDrawContext _localctx = new CommandDrawContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_commandDraw);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(142);
			match(DRAW);
			setState(147);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RANDINT) | (1L << DRAW) | (1L << IF) | (1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(145);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case RANDINT:
				case DRAW:
				case IF:
				case BSLASH:
					{
					setState(143);
					callreference();
					}
					break;
				case ID:
				case CR_OTHER:
					{
					setState(144);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(149);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(150);
			match(CP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandIfContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(OEFParser.IF, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public CommandIfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandIf; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCommandIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCommandIf(this);
		}
	}

	public final CommandIfContext commandIf() throws RecognitionException {
		CommandIfContext _localctx = new CommandIfContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_commandIf);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			match(IF);
			setState(157);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RANDINT) | (1L << DRAW) | (1L << IF) | (1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(155);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case RANDINT:
				case DRAW:
				case IF:
				case BSLASH:
					{
					setState(153);
					callreference();
					}
					break;
				case ID:
				case CR_OTHER:
					{
					setState(154);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(159);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(160);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RandintContext extends ParserRuleContext {
		public TerminalNode RANDINT() { return getToken(OEFParser.RANDINT, 0); }
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public RandintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_randint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterRandint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitRandint(this);
		}
	}

	public final RandintContext randint() throws RecognitionException {
		RandintContext _localctx = new RandintContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_randint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(162);
			match(RANDINT);
			setState(167);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RANDINT) | (1L << DRAW) | (1L << IF) | (1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(165);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case RANDINT:
				case DRAW:
				case IF:
				case BSLASH:
					{
					setState(163);
					callreference();
					}
					break;
				case ID:
				case CR_OTHER:
					{
					setState(164);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(169);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(170);
			match(CP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallreferenceContext extends ParserRuleContext {
		public TerminalNode BSLASH() { return getToken(OEFParser.BSLASH, 0); }
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public CommandDrawContext commandDraw() {
			return getRuleContext(CommandDrawContext.class,0);
		}
		public CommandIfContext commandIf() {
			return getRuleContext(CommandIfContext.class,0);
		}
		public RandintContext randint() {
			return getRuleContext(RandintContext.class,0);
		}
		public CallreferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callreference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallreference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallreference(this);
		}
	}

	public final CallreferenceContext callreference() throws RecognitionException {
		CallreferenceContext _localctx = new CallreferenceContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_callreference);
		try {
			setState(177);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BSLASH:
				enterOuterAlt(_localctx, 1);
				{
				setState(172);
				match(BSLASH);
				setState(173);
				match(ID);
				}
				break;
			case DRAW:
				enterOuterAlt(_localctx, 2);
				{
				setState(174);
				commandDraw();
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 3);
				{
				setState(175);
				commandIf();
				}
				break;
			case RANDINT:
				enterOuterAlt(_localctx, 4);
				{
				setState(176);
				randint();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\23\u00b6\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\3\3"+
		"\3\5\3&\n\3\3\3\3\3\5\3*\n\3\3\3\3\3\5\3.\n\3\3\3\3\3\5\3\62\n\3\3\3\3"+
		"\3\5\3\66\n\3\3\3\3\3\5\3:\n\3\3\3\3\3\5\3>\n\3\7\3@\n\3\f\3\16\3C\13"+
		"\3\3\3\3\3\5\3G\n\3\7\3I\n\3\f\3\16\3L\13\3\3\4\3\4\3\4\7\4Q\n\4\f\4\16"+
		"\4T\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\7\6^\n\6\f\6\16\6a\13\6\3\6\3"+
		"\6\3\7\3\7\7\7g\n\7\f\7\16\7j\13\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t"+
		"\3\t\3\n\3\n\7\nx\n\n\f\n\16\n{\13\n\3\n\3\n\3\13\3\13\3\13\7\13\u0082"+
		"\n\13\f\13\16\13\u0085\13\13\3\13\3\13\3\f\6\f\u008a\n\f\r\f\16\f\u008b"+
		"\3\f\5\f\u008f\n\f\3\r\3\r\3\r\7\r\u0094\n\r\f\r\16\r\u0097\13\r\3\r\3"+
		"\r\3\16\3\16\3\16\7\16\u009e\n\16\f\16\16\16\u00a1\13\16\3\16\3\16\3\17"+
		"\3\17\3\17\7\17\u00a8\n\17\f\17\16\17\u00ab\13\17\3\17\3\17\3\20\3\20"+
		"\3\20\3\20\3\20\5\20\u00b4\n\20\3\20\2\2\21\2\4\6\b\n\f\16\20\22\24\26"+
		"\30\32\34\36\2\2\2\u00c2\2 \3\2\2\2\4#\3\2\2\2\6M\3\2\2\2\bW\3\2\2\2\n"+
		"[\3\2\2\2\fd\3\2\2\2\16m\3\2\2\2\20q\3\2\2\2\22u\3\2\2\2\24~\3\2\2\2\26"+
		"\u008e\3\2\2\2\30\u0090\3\2\2\2\32\u009a\3\2\2\2\34\u00a4\3\2\2\2\36\u00b3"+
		"\3\2\2\2 !\5\4\3\2!\"\7\2\2\3\"\3\3\2\2\2#%\5\6\4\2$&\7\13\2\2%$\3\2\2"+
		"\2%&\3\2\2\2&\'\3\2\2\2\')\5\b\5\2(*\7\13\2\2)(\3\2\2\2)*\3\2\2\2*+\3"+
		"\2\2\2+-\5\n\6\2,.\7\13\2\2-,\3\2\2\2-.\3\2\2\2./\3\2\2\2/\61\5\f\7\2"+
		"\60\62\7\13\2\2\61\60\3\2\2\2\61\62\3\2\2\2\62\63\3\2\2\2\63\65\5\16\b"+
		"\2\64\66\7\13\2\2\65\64\3\2\2\2\65\66\3\2\2\2\66\67\3\2\2\2\679\5\20\t"+
		"\28:\7\13\2\298\3\2\2\29:\3\2\2\2:A\3\2\2\2;=\5\22\n\2<>\7\13\2\2=<\3"+
		"\2\2\2=>\3\2\2\2>@\3\2\2\2?;\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2BJ\3"+
		"\2\2\2CA\3\2\2\2DF\5\24\13\2EG\7\13\2\2FE\3\2\2\2FG\3\2\2\2GI\3\2\2\2"+
		"HD\3\2\2\2IL\3\2\2\2JH\3\2\2\2JK\3\2\2\2K\5\3\2\2\2LJ\3\2\2\2MR\7\3\2"+
		"\2NQ\5\36\20\2OQ\5\26\f\2PN\3\2\2\2PO\3\2\2\2QT\3\2\2\2RP\3\2\2\2RS\3"+
		"\2\2\2SU\3\2\2\2TR\3\2\2\2UV\7\f\2\2V\7\3\2\2\2WX\7\4\2\2XY\5\26\f\2Y"+
		"Z\7\f\2\2Z\t\3\2\2\2[_\7\5\2\2\\^\5\26\f\2]\\\3\2\2\2^a\3\2\2\2_]\3\2"+
		"\2\2_`\3\2\2\2`b\3\2\2\2a_\3\2\2\2bc\7\f\2\2c\13\3\2\2\2dh\7\6\2\2eg\5"+
		"\26\f\2fe\3\2\2\2gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2ik\3\2\2\2jh\3\2\2\2kl"+
		"\7\f\2\2l\r\3\2\2\2mn\7\7\2\2no\5\26\f\2op\7\f\2\2p\17\3\2\2\2qr\7\b\2"+
		"\2rs\5\26\f\2st\7\f\2\2t\21\3\2\2\2uy\7\t\2\2vx\5\26\f\2wv\3\2\2\2x{\3"+
		"\2\2\2yw\3\2\2\2yz\3\2\2\2z|\3\2\2\2{y\3\2\2\2|}\7\f\2\2}\23\3\2\2\2~"+
		"\u0083\7\n\2\2\177\u0082\5\36\20\2\u0080\u0082\5\26\f\2\u0081\177\3\2"+
		"\2\2\u0081\u0080\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0083"+
		"\u0084\3\2\2\2\u0084\u0086\3\2\2\2\u0085\u0083\3\2\2\2\u0086\u0087\7\f"+
		"\2\2\u0087\25\3\2\2\2\u0088\u008a\7\23\2\2\u0089\u0088\3\2\2\2\u008a\u008b"+
		"\3\2\2\2\u008b\u0089\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008f\3\2\2\2\u008d"+
		"\u008f\7\22\2\2\u008e\u0089\3\2\2\2\u008e\u008d\3\2\2\2\u008f\27\3\2\2"+
		"\2\u0090\u0095\7\17\2\2\u0091\u0094\5\36\20\2\u0092\u0094\5\26\f\2\u0093"+
		"\u0091\3\2\2\2\u0093\u0092\3\2\2\2\u0094\u0097\3\2\2\2\u0095\u0093\3\2"+
		"\2\2\u0095\u0096\3\2\2\2\u0096\u0098\3\2\2\2\u0097\u0095\3\2\2\2\u0098"+
		"\u0099\7\r\2\2\u0099\31\3\2\2\2\u009a\u009f\7\20\2\2\u009b\u009e\5\36"+
		"\20\2\u009c\u009e\5\26\f\2\u009d\u009b\3\2\2\2\u009d\u009c\3\2\2\2\u009e"+
		"\u00a1\3\2\2\2\u009f\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a2\3\2"+
		"\2\2\u00a1\u009f\3\2\2\2\u00a2\u00a3\7\f\2\2\u00a3\33\3\2\2\2\u00a4\u00a9"+
		"\7\16\2\2\u00a5\u00a8\5\36\20\2\u00a6\u00a8\5\26\f\2\u00a7\u00a5\3\2\2"+
		"\2\u00a7\u00a6\3\2\2\2\u00a8\u00ab\3\2\2\2\u00a9\u00a7\3\2\2\2\u00a9\u00aa"+
		"\3\2\2\2\u00aa\u00ac\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ac\u00ad\7\r\2\2\u00ad"+
		"\35\3\2\2\2\u00ae\u00af\7\21\2\2\u00af\u00b4\7\22\2\2\u00b0\u00b4\5\30"+
		"\r\2\u00b1\u00b4\5\32\16\2\u00b2\u00b4\5\34\17\2\u00b3\u00ae\3\2\2\2\u00b3"+
		"\u00b0\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b2\3\2\2\2\u00b4\37\3\2\2"+
		"\2\34%)-\61\659=AFJPR_hy\u0081\u0083\u008b\u008e\u0093\u0095\u009d\u009f"+
		"\u00a7\u00a9\u00b3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}