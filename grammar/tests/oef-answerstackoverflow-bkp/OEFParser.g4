parser grammar OEFParser;

options { tokenVocab=OEFLexer; }


document 		: 	pre EOF ;

pre 			:	title S? language S? author S? email S? computeanswer S? precision S? (rangedeclaration S?)* (integer S?)*;



title 				: TITLE 			( callreference | string )* CB ;
language 			: LANGUAGE 			string CB ;
author				: AUTHOR 			(string)* CB;
email				: EMAIL 			(string)* CB;
computeanswer 		: COMPUTEANSWER 	string CB;
precision			: PRECISION 	 	string CB;
rangedeclaration 	: RANGEDECLARATION 	(string)* CB;
integer 			: INTEGER 			(callreference | string)* CB;

string
 : CR_OTHER+
 | ID
 ;

commandDraw
 : DRAW ( callreference | string )* CP
 ;

commandIf
 : IF ( callreference | string )* CB
 ;

randint
: RANDINT ( callreference | string )* CP
;

callreference
 : BSLASH ID
 | commandDraw
 | commandIf
 | randint
 ;