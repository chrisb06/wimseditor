// Generated from OEFLexer.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TITLE=1, LANGUAGE=2, AUTHOR=3, EMAIL=4, COMPUTEANSWER=5, PRECISION=6, 
		RANGEDECLARATION=7, INTEGER=8, MATRIX=9, STATEMENT=10, OP=11, S=12, CB=13, 
		CP=14, NUM_INT=15, NUM_REAL=16, COMMA=17, DOTS=18, EQUAL=19, PLUS=20, 
		MINUS=21, STAR=22, SLASH=23, POWER=24, AND=25, OR=26, LESS=27, LESSEQUAL=28, 
		GREATER=29, GREATEREQUAL=30, DBEQUAL=31, NOTEQUAL=32, DRAW=33, RANDINT=34, 
		POINT=35, IF=36, NEWLINE=37, BSLASH=38, ID=39, CR_OTHER=40;
	public static final int
		CALLINSTRUCTION=1;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE", "CALLINSTRUCTION"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"TITLE", "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
			"RANGEDECLARATION", "INTEGER", "MATRIX", "STATEMENT", "OB", "OP", "S", 
			"CB", "CP", "NUM_INT", "NUM_REAL", "EXPONENT", "COMMA", "DOTS", "EQUAL", 
			"PLUS", "MINUS", "STAR", "SLASH", "POWER", "AND", "OR", "LESS", "LESSEQUAL", 
			"GREATER", "GREATEREQUAL", "DBEQUAL", "NOTEQUAL", "DRAW", "RANDINT", 
			"POINT", "IF", "NEWLINE", "BSLASH", "ID", "CR_OTHER"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, "'('", 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"'^'", null, null, null, null, null, null, null, null, null, null, null, 
			null, "'\n'", "'\\'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "TITLE", "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
			"RANGEDECLARATION", "INTEGER", "MATRIX", "STATEMENT", "OP", "S", "CB", 
			"CP", "NUM_INT", "NUM_REAL", "COMMA", "DOTS", "EQUAL", "PLUS", "MINUS", 
			"STAR", "SLASH", "POWER", "AND", "OR", "LESS", "LESSEQUAL", "GREATER", 
			"GREATEREQUAL", "DBEQUAL", "NOTEQUAL", "DRAW", "RANDINT", "POINT", "IF", 
			"NEWLINE", "BSLASH", "ID", "CR_OTHER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public OEFLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "OEFLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 7:
			INTEGER_action((RuleContext)_localctx, actionIndex);
			break;
		case 8:
			MATRIX_action((RuleContext)_localctx, actionIndex);
			break;
		case 13:
			CB_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void INTEGER_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			System.out.println("INTEGER ENTER");
			break;
		}
	}
	private void MATRIX_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			System.out.println("Matrix ENTER");
			break;
		}
	}
	private void CB_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 2:
			System.out.println("pop CB");
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2*\u0218\b\1\b\1\4"+
		"\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n"+
		"\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2a\n\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3r\n\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\5\4\u0081\n\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\5\5\u008f\n\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u00a5\n\6\3\6\3\6\3\6\3\6"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00b7\n\7\3\7\3\7"+
		"\5\7\u00bb\n\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00d2\n\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00e2\n\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00f2\n\n\3\n\3\n\3\n\3\n\3\n\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u0105\n\13"+
		"\3\13\3\13\3\13\3\13\3\f\3\f\3\f\7\f\u010e\n\f\f\f\16\f\u0111\13\f\3\r"+
		"\3\r\3\16\6\16\u0116\n\16\r\16\16\16\u0117\3\17\3\17\7\17\u011c\n\17\f"+
		"\17\16\17\u011f\13\17\3\17\3\17\3\17\7\17\u0124\n\17\f\17\16\17\u0127"+
		"\13\17\3\17\3\17\3\17\3\17\3\20\3\20\7\20\u012f\n\20\f\20\16\20\u0132"+
		"\13\20\3\20\3\20\3\20\7\20\u0137\n\20\f\20\16\20\u013a\13\20\3\21\5\21"+
		"\u013d\n\21\3\21\6\21\u0140\n\21\r\21\16\21\u0141\3\21\5\21\u0145\n\21"+
		"\3\22\5\22\u0148\n\22\3\22\6\22\u014b\n\22\r\22\16\22\u014c\3\22\3\22"+
		"\6\22\u0151\n\22\r\22\16\22\u0152\3\22\5\22\u0156\n\22\5\22\u0158\n\22"+
		"\3\22\5\22\u015b\n\22\3\22\5\22\u015e\n\22\3\23\3\23\5\23\u0162\n\23\3"+
		"\23\6\23\u0165\n\23\r\23\16\23\u0166\3\24\5\24\u016a\n\24\3\24\3\24\5"+
		"\24\u016e\n\24\3\25\5\25\u0171\n\25\3\25\3\25\3\25\3\25\5\25\u0177\n\25"+
		"\3\26\5\26\u017a\n\26\3\26\3\26\5\26\u017e\n\26\3\27\5\27\u0181\n\27\3"+
		"\27\3\27\5\27\u0185\n\27\3\30\5\30\u0188\n\30\3\30\3\30\5\30\u018c\n\30"+
		"\3\31\5\31\u018f\n\31\3\31\3\31\5\31\u0193\n\31\3\32\5\32\u0196\n\32\3"+
		"\32\3\32\5\32\u019a\n\32\3\33\3\33\3\34\5\34\u019f\n\34\3\34\3\34\3\34"+
		"\3\34\3\34\5\34\u01a6\n\34\3\35\5\35\u01a9\n\35\3\35\3\35\3\35\3\35\5"+
		"\35\u01af\n\35\3\36\5\36\u01b2\n\36\3\36\3\36\5\36\u01b6\n\36\3\37\5\37"+
		"\u01b9\n\37\3\37\3\37\3\37\3\37\5\37\u01bf\n\37\3 \5 \u01c2\n \3 \3 \5"+
		" \u01c6\n \3!\5!\u01c9\n!\3!\3!\3!\3!\5!\u01cf\n!\3\"\5\"\u01d2\n\"\3"+
		"\"\3\"\3\"\3\"\5\"\u01d8\n\"\3#\5#\u01db\n#\3#\3#\3#\3#\5#\u01e1\n#\3"+
		"$\3$\3$\3$\3$\3$\5$\u01e9\n$\3$\3$\3%\3%\3%\3%\3%\3%\3%\3%\3%\5%\u01f6"+
		"\n%\3%\3%\3&\5&\u01fb\n&\3&\3&\3&\3&\3&\3&\3&\5&\u0204\n&\3\'\3\'\3\'"+
		"\3\'\3\'\3\'\3\'\3\'\3(\3(\3)\3)\3*\6*\u0213\n*\r*\16*\u0214\3+\3+\2\2"+
		",\4\3\6\4\b\5\n\6\f\7\16\b\20\t\22\n\24\13\26\f\30\2\32\r\34\16\36\17"+
		" \20\"\21$\22&\2(\23*\24,\25.\26\60\27\62\30\64\31\66\328\33:\34<\35>"+
		"\36@\37B D!F\"H#J$L%N&P\'R(T)V*\4\2\3\6\5\2\13\13\17\17\"\"\3\2\62;\4"+
		"\2--//\4\2C\\c|\2\u0259\2\4\3\2\2\2\2\6\3\2\2\2\2\b\3\2\2\2\2\n\3\2\2"+
		"\2\2\f\3\2\2\2\2\16\3\2\2\2\2\20\3\2\2\2\2\22\3\2\2\2\2\24\3\2\2\2\2\26"+
		"\3\2\2\2\2\32\3\2\2\2\2\34\3\2\2\2\3\36\3\2\2\2\3 \3\2\2\2\3\"\3\2\2\2"+
		"\3$\3\2\2\2\3(\3\2\2\2\3*\3\2\2\2\3,\3\2\2\2\3.\3\2\2\2\3\60\3\2\2\2\3"+
		"\62\3\2\2\2\3\64\3\2\2\2\3\66\3\2\2\2\38\3\2\2\2\3:\3\2\2\2\3<\3\2\2\2"+
		"\3>\3\2\2\2\3@\3\2\2\2\3B\3\2\2\2\3D\3\2\2\2\3F\3\2\2\2\3H\3\2\2\2\3J"+
		"\3\2\2\2\3L\3\2\2\2\3N\3\2\2\2\3P\3\2\2\2\3R\3\2\2\2\3T\3\2\2\2\3V\3\2"+
		"\2\2\4X\3\2\2\2\6f\3\2\2\2\bw\3\2\2\2\n\u0086\3\2\2\2\f\u0094\3\2\2\2"+
		"\16\u00aa\3\2\2\2\20\u00be\3\2\2\2\22\u00d7\3\2\2\2\24\u00e8\3\2\2\2\26"+
		"\u00f8\3\2\2\2\30\u010a\3\2\2\2\32\u0112\3\2\2\2\34\u0115\3\2\2\2\36\u011d"+
		"\3\2\2\2 \u0130\3\2\2\2\"\u013c\3\2\2\2$\u0147\3\2\2\2&\u015f\3\2\2\2"+
		"(\u0169\3\2\2\2*\u0170\3\2\2\2,\u0179\3\2\2\2.\u0180\3\2\2\2\60\u0187"+
		"\3\2\2\2\62\u018e\3\2\2\2\64\u0195\3\2\2\2\66\u019b\3\2\2\28\u019e\3\2"+
		"\2\2:\u01a8\3\2\2\2<\u01b1\3\2\2\2>\u01b8\3\2\2\2@\u01c1\3\2\2\2B\u01c8"+
		"\3\2\2\2D\u01d1\3\2\2\2F\u01da\3\2\2\2H\u01e2\3\2\2\2J\u01ec\3\2\2\2L"+
		"\u01fa\3\2\2\2N\u0205\3\2\2\2P\u020d\3\2\2\2R\u020f\3\2\2\2T\u0212\3\2"+
		"\2\2V\u0216\3\2\2\2XY\7^\2\2YZ\7v\2\2Z[\7k\2\2[\\\7v\2\2\\]\7n\2\2]^\7"+
		"g\2\2^`\3\2\2\2_a\5\34\16\2`_\3\2\2\2`a\3\2\2\2ab\3\2\2\2bc\5\30\f\2c"+
		"d\3\2\2\2de\b\2\2\2e\5\3\2\2\2fg\7^\2\2gh\7n\2\2hi\7c\2\2ij\7p\2\2jk\7"+
		"i\2\2kl\7w\2\2lm\7c\2\2mn\7i\2\2no\7g\2\2oq\3\2\2\2pr\5\34\16\2qp\3\2"+
		"\2\2qr\3\2\2\2rs\3\2\2\2st\5\30\f\2tu\3\2\2\2uv\b\3\2\2v\7\3\2\2\2wx\7"+
		"^\2\2xy\7c\2\2yz\7w\2\2z{\7v\2\2{|\7j\2\2|}\7q\2\2}~\7t\2\2~\u0080\3\2"+
		"\2\2\177\u0081\5\34\16\2\u0080\177\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0083\5\30\f\2\u0083\u0084\3\2\2\2\u0084\u0085\b\4\2\2"+
		"\u0085\t\3\2\2\2\u0086\u0087\7^\2\2\u0087\u0088\7g\2\2\u0088\u0089\7o"+
		"\2\2\u0089\u008a\7c\2\2\u008a\u008b\7k\2\2\u008b\u008c\7n\2\2\u008c\u008e"+
		"\3\2\2\2\u008d\u008f\5\34\16\2\u008e\u008d\3\2\2\2\u008e\u008f\3\2\2\2"+
		"\u008f\u0090\3\2\2\2\u0090\u0091\5\30\f\2\u0091\u0092\3\2\2\2\u0092\u0093"+
		"\b\5\2\2\u0093\13\3\2\2\2\u0094\u0095\7^\2\2\u0095\u0096\7e\2\2\u0096"+
		"\u0097\7q\2\2\u0097\u0098\7o\2\2\u0098\u0099\7r\2\2\u0099\u009a\7w\2\2"+
		"\u009a\u009b\7v\2\2\u009b\u009c\7g\2\2\u009c\u009d\7c\2\2\u009d\u009e"+
		"\7p\2\2\u009e\u009f\7u\2\2\u009f\u00a0\7y\2\2\u00a0\u00a1\7g\2\2\u00a1"+
		"\u00a2\7t\2\2\u00a2\u00a4\3\2\2\2\u00a3\u00a5\5\34\16\2\u00a4\u00a3\3"+
		"\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a7\5\30\f\2\u00a7"+
		"\u00a8\3\2\2\2\u00a8\u00a9\b\6\2\2\u00a9\r\3\2\2\2\u00aa\u00ab\7^\2\2"+
		"\u00ab\u00ac\7r\2\2\u00ac\u00ad\7t\2\2\u00ad\u00ae\7g\2\2\u00ae\u00af"+
		"\7e\2\2\u00af\u00b0\7k\2\2\u00b0\u00b1\7u\2\2\u00b1\u00b2\7k\2\2\u00b2"+
		"\u00b3\7q\2\2\u00b3\u00b4\7p\2\2\u00b4\u00b6\3\2\2\2\u00b5\u00b7\5\34"+
		"\16\2\u00b6\u00b5\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8"+
		"\u00ba\5\30\f\2\u00b9\u00bb\5\34\16\2\u00ba\u00b9\3\2\2\2\u00ba\u00bb"+
		"\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00bd\b\7\2\2\u00bd\17\3\2\2\2\u00be"+
		"\u00bf\7^\2\2\u00bf\u00c0\7t\2\2\u00c0\u00c1\7c\2\2\u00c1\u00c2\7p\2\2"+
		"\u00c2\u00c3\7i\2\2\u00c3\u00c4\7g\2\2\u00c4\u00c5\7f\2\2\u00c5\u00c6"+
		"\7g\2\2\u00c6\u00c7\7e\2\2\u00c7\u00c8\7n\2\2\u00c8\u00c9\7c\2\2\u00c9"+
		"\u00ca\7t\2\2\u00ca\u00cb\7c\2\2\u00cb\u00cc\7v\2\2\u00cc\u00cd\7k\2\2"+
		"\u00cd\u00ce\7q\2\2\u00ce\u00cf\7p\2\2\u00cf\u00d1\3\2\2\2\u00d0\u00d2"+
		"\5\34\16\2\u00d1\u00d0\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d3\3\2\2\2"+
		"\u00d3\u00d4\5\30\f\2\u00d4\u00d5\3\2\2\2\u00d5\u00d6\b\b\2\2\u00d6\21"+
		"\3\2\2\2\u00d7\u00d8\7^\2\2\u00d8\u00d9\7k\2\2\u00d9\u00da\7p\2\2\u00da"+
		"\u00db\7v\2\2\u00db\u00dc\7g\2\2\u00dc\u00dd\7i\2\2\u00dd\u00de\7g\2\2"+
		"\u00de\u00df\7t\2\2\u00df\u00e1\3\2\2\2\u00e0\u00e2\5\34\16\2\u00e1\u00e0"+
		"\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\u00e4\5\30\f\2"+
		"\u00e4\u00e5\b\t\3\2\u00e5\u00e6\3\2\2\2\u00e6\u00e7\b\t\2\2\u00e7\23"+
		"\3\2\2\2\u00e8\u00e9\7^\2\2\u00e9\u00ea\7o\2\2\u00ea\u00eb\7c\2\2\u00eb"+
		"\u00ec\7v\2\2\u00ec\u00ed\7t\2\2\u00ed\u00ee\7k\2\2\u00ee\u00ef\7z\2\2"+
		"\u00ef\u00f1\3\2\2\2\u00f0\u00f2\5\34\16\2\u00f1\u00f0\3\2\2\2\u00f1\u00f2"+
		"\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f4\5\30\f\2\u00f4\u00f5\b\n\4\2"+
		"\u00f5\u00f6\3\2\2\2\u00f6\u00f7\b\n\2\2\u00f7\25\3\2\2\2\u00f8\u00f9"+
		"\7^\2\2\u00f9\u00fa\7u\2\2\u00fa\u00fb\7v\2\2\u00fb\u00fc\7c\2\2\u00fc"+
		"\u00fd\7v\2\2\u00fd\u00fe\7g\2\2\u00fe\u00ff\7o\2\2\u00ff\u0100\7g\2\2"+
		"\u0100\u0101\7p\2\2\u0101\u0102\7v\2\2\u0102\u0104\3\2\2\2\u0103\u0105"+
		"\5\34\16\2\u0104\u0103\3\2\2\2\u0104\u0105\3\2\2\2\u0105\u0106\3\2\2\2"+
		"\u0106\u0107\5\30\f\2\u0107\u0108\3\2\2\2\u0108\u0109\b\13\2\2\u0109\27"+
		"\3\2\2\2\u010a\u010f\7}\2\2\u010b\u010e\5\34\16\2\u010c\u010e\5P(\2\u010d"+
		"\u010b\3\2\2\2\u010d\u010c\3\2\2\2\u010e\u0111\3\2\2\2\u010f\u010d\3\2"+
		"\2\2\u010f\u0110\3\2\2\2\u0110\31\3\2\2\2\u0111\u010f\3\2\2\2\u0112\u0113"+
		"\7*\2\2\u0113\33\3\2\2\2\u0114\u0116\t\2\2\2\u0115\u0114\3\2\2\2\u0116"+
		"\u0117\3\2\2\2\u0117\u0115\3\2\2\2\u0117\u0118\3\2\2\2\u0118\35\3\2\2"+
		"\2\u0119\u011c\5\34\16\2\u011a\u011c\5P(\2\u011b\u0119\3\2\2\2\u011b\u011a"+
		"\3\2\2\2\u011c\u011f\3\2\2\2\u011d\u011b\3\2\2\2\u011d\u011e\3\2\2\2\u011e"+
		"\u0120\3\2\2\2\u011f\u011d\3\2\2\2\u0120\u0125\7\177\2\2\u0121\u0124\5"+
		"\34\16\2\u0122\u0124\5P(\2\u0123\u0121\3\2\2\2\u0123\u0122\3\2\2\2\u0124"+
		"\u0127\3\2\2\2\u0125\u0123\3\2\2\2\u0125\u0126\3\2\2\2\u0126\u0128\3\2"+
		"\2\2\u0127\u0125\3\2\2\2\u0128\u0129\b\17\5\2\u0129\u012a\3\2\2\2\u012a"+
		"\u012b\b\17\6\2\u012b\37\3\2\2\2\u012c\u012f\5\34\16\2\u012d\u012f\5P"+
		"(\2\u012e\u012c\3\2\2\2\u012e\u012d\3\2\2\2\u012f\u0132\3\2\2\2\u0130"+
		"\u012e\3\2\2\2\u0130\u0131\3\2\2\2\u0131\u0133\3\2\2\2\u0132\u0130\3\2"+
		"\2\2\u0133\u0138\7+\2\2\u0134\u0137\5\34\16\2\u0135\u0137\5P(\2\u0136"+
		"\u0134\3\2\2\2\u0136\u0135\3\2\2\2\u0137\u013a\3\2\2\2\u0138\u0136\3\2"+
		"\2\2\u0138\u0139\3\2\2\2\u0139!\3\2\2\2\u013a\u0138\3\2\2\2\u013b\u013d"+
		"\5\34\16\2\u013c\u013b\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013f\3\2\2\2"+
		"\u013e\u0140\t\3\2\2\u013f\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141\u013f"+
		"\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0144\3\2\2\2\u0143\u0145\5\34\16\2"+
		"\u0144\u0143\3\2\2\2\u0144\u0145\3\2\2\2\u0145#\3\2\2\2\u0146\u0148\5"+
		"\34\16\2\u0147\u0146\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u014a\3\2\2\2\u0149"+
		"\u014b\t\3\2\2\u014a\u0149\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u014a\3\2"+
		"\2\2\u014c\u014d\3\2\2\2\u014d\u015a\3\2\2\2\u014e\u0150\7\60\2\2\u014f"+
		"\u0151\t\3\2\2\u0150\u014f\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0150\3\2"+
		"\2\2\u0152\u0153\3\2\2\2\u0153\u0155\3\2\2\2\u0154\u0156\5&\23\2\u0155"+
		"\u0154\3\2\2\2\u0155\u0156\3\2\2\2\u0156\u0158\3\2\2\2\u0157\u014e\3\2"+
		"\2\2\u0157\u0158\3\2\2\2\u0158\u015b\3\2\2\2\u0159\u015b\5&\23\2\u015a"+
		"\u0157\3\2\2\2\u015a\u0159\3\2\2\2\u015b\u015d\3\2\2\2\u015c\u015e\5\34"+
		"\16\2\u015d\u015c\3\2\2\2\u015d\u015e\3\2\2\2\u015e%\3\2\2\2\u015f\u0161"+
		"\7g\2\2\u0160\u0162\t\4\2\2\u0161\u0160\3\2\2\2\u0161\u0162\3\2\2\2\u0162"+
		"\u0164\3\2\2\2\u0163\u0165\t\3\2\2\u0164\u0163\3\2\2\2\u0165\u0166\3\2"+
		"\2\2\u0166\u0164\3\2\2\2\u0166\u0167\3\2\2\2\u0167\'\3\2\2\2\u0168\u016a"+
		"\5\34\16\2\u0169\u0168\3\2\2\2\u0169\u016a\3\2\2\2\u016a\u016b\3\2\2\2"+
		"\u016b\u016d\7.\2\2\u016c\u016e\5\34\16\2\u016d\u016c\3\2\2\2\u016d\u016e"+
		"\3\2\2\2\u016e)\3\2\2\2\u016f\u0171\5\34\16\2\u0170\u016f\3\2\2\2\u0170"+
		"\u0171\3\2\2\2\u0171\u0172\3\2\2\2\u0172\u0173\7\60\2\2\u0173\u0174\7"+
		"\60\2\2\u0174\u0176\3\2\2\2\u0175\u0177\5\34\16\2\u0176\u0175\3\2\2\2"+
		"\u0176\u0177\3\2\2\2\u0177+\3\2\2\2\u0178\u017a\5\34\16\2\u0179\u0178"+
		"\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u017b\3\2\2\2\u017b\u017d\7?\2\2\u017c"+
		"\u017e\5\34\16\2\u017d\u017c\3\2\2\2\u017d\u017e\3\2\2\2\u017e-\3\2\2"+
		"\2\u017f\u0181\5\34\16\2\u0180\u017f\3\2\2\2\u0180\u0181\3\2\2\2\u0181"+
		"\u0182\3\2\2\2\u0182\u0184\7-\2\2\u0183\u0185\5\34\16\2\u0184\u0183\3"+
		"\2\2\2\u0184\u0185\3\2\2\2\u0185/\3\2\2\2\u0186\u0188\5\34\16\2\u0187"+
		"\u0186\3\2\2\2\u0187\u0188\3\2\2\2\u0188\u0189\3\2\2\2\u0189\u018b\7/"+
		"\2\2\u018a\u018c\5\34\16\2\u018b\u018a\3\2\2\2\u018b\u018c\3\2\2\2\u018c"+
		"\61\3\2\2\2\u018d\u018f\5\34\16\2\u018e\u018d\3\2\2\2\u018e\u018f\3\2"+
		"\2\2\u018f\u0190\3\2\2\2\u0190\u0192\7,\2\2\u0191\u0193\5\34\16\2\u0192"+
		"\u0191\3\2\2\2\u0192\u0193\3\2\2\2\u0193\63\3\2\2\2\u0194\u0196\5\34\16"+
		"\2\u0195\u0194\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u0197\3\2\2\2\u0197\u0199"+
		"\7\61\2\2\u0198\u019a\5\34\16\2\u0199\u0198\3\2\2\2\u0199\u019a\3\2\2"+
		"\2\u019a\65\3\2\2\2\u019b\u019c\7`\2\2\u019c\67\3\2\2\2\u019d\u019f\5"+
		"\34\16\2\u019e\u019d\3\2\2\2\u019e\u019f\3\2\2\2\u019f\u01a0\3\2\2\2\u01a0"+
		"\u01a1\7c\2\2\u01a1\u01a2\7p\2\2\u01a2\u01a3\7f\2\2\u01a3\u01a5\3\2\2"+
		"\2\u01a4\u01a6\5\34\16\2\u01a5\u01a4\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6"+
		"9\3\2\2\2\u01a7\u01a9\5\34\16\2\u01a8\u01a7\3\2\2\2\u01a8\u01a9\3\2\2"+
		"\2\u01a9\u01aa\3\2\2\2\u01aa\u01ab\7q\2\2\u01ab\u01ac\7t\2\2\u01ac\u01ae"+
		"\3\2\2\2\u01ad\u01af\5\34\16\2\u01ae\u01ad\3\2\2\2\u01ae\u01af\3\2\2\2"+
		"\u01af;\3\2\2\2\u01b0\u01b2\5\34\16\2\u01b1\u01b0\3\2\2\2\u01b1\u01b2"+
		"\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b5\7>\2\2\u01b4\u01b6\5\34\16\2"+
		"\u01b5\u01b4\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6=\3\2\2\2\u01b7\u01b9\5"+
		"\34\16\2\u01b8\u01b7\3\2\2\2\u01b8\u01b9\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba"+
		"\u01bb\7>\2\2\u01bb\u01bc\7?\2\2\u01bc\u01be\3\2\2\2\u01bd\u01bf\5\34"+
		"\16\2\u01be\u01bd\3\2\2\2\u01be\u01bf\3\2\2\2\u01bf?\3\2\2\2\u01c0\u01c2"+
		"\5\34\16\2\u01c1\u01c0\3\2\2\2\u01c1\u01c2\3\2\2\2\u01c2\u01c3\3\2\2\2"+
		"\u01c3\u01c5\7@\2\2\u01c4\u01c6\5\34\16\2\u01c5\u01c4\3\2\2\2\u01c5\u01c6"+
		"\3\2\2\2\u01c6A\3\2\2\2\u01c7\u01c9\5\34\16\2\u01c8\u01c7\3\2\2\2\u01c8"+
		"\u01c9\3\2\2\2\u01c9\u01ca\3\2\2\2\u01ca\u01cb\7@\2\2\u01cb\u01cc\7?\2"+
		"\2\u01cc\u01ce\3\2\2\2\u01cd\u01cf\5\34\16\2\u01ce\u01cd\3\2\2\2\u01ce"+
		"\u01cf\3\2\2\2\u01cfC\3\2\2\2\u01d0\u01d2\5\34\16\2\u01d1\u01d0\3\2\2"+
		"\2\u01d1\u01d2\3\2\2\2\u01d2\u01d3\3\2\2\2\u01d3\u01d4\7?\2\2\u01d4\u01d5"+
		"\7?\2\2\u01d5\u01d7\3\2\2\2\u01d6\u01d8\5\34\16\2\u01d7\u01d6\3\2\2\2"+
		"\u01d7\u01d8\3\2\2\2\u01d8E\3\2\2\2\u01d9\u01db\5\34\16\2\u01da\u01d9"+
		"\3\2\2\2\u01da\u01db\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc\u01dd\7#\2\2\u01dd"+
		"\u01de\7?\2\2\u01de\u01e0\3\2\2\2\u01df\u01e1\5\34\16\2\u01e0\u01df\3"+
		"\2\2\2\u01e0\u01e1\3\2\2\2\u01e1G\3\2\2\2\u01e2\u01e3\7f\2\2\u01e3\u01e4"+
		"\7t\2\2\u01e4\u01e5\7c\2\2\u01e5\u01e6\7y\2\2\u01e6\u01e8\3\2\2\2\u01e7"+
		"\u01e9\5\34\16\2\u01e8\u01e7\3\2\2\2\u01e8\u01e9\3\2\2\2\u01e9\u01ea\3"+
		"\2\2\2\u01ea\u01eb\5\32\r\2\u01ebI\3\2\2\2\u01ec\u01ed\7t\2\2\u01ed\u01ee"+
		"\7c\2\2\u01ee\u01ef\7p\2\2\u01ef\u01f0\7f\2\2\u01f0\u01f1\7k\2\2\u01f1"+
		"\u01f2\7p\2\2\u01f2\u01f3\7v\2\2\u01f3\u01f5\3\2\2\2\u01f4\u01f6\5\34"+
		"\16\2\u01f5\u01f4\3\2\2\2\u01f5\u01f6\3\2\2\2\u01f6\u01f7\3\2\2\2\u01f7"+
		"\u01f8\5\32\r\2\u01f8K\3\2\2\2\u01f9\u01fb\5\34\16\2\u01fa\u01f9\3\2\2"+
		"\2\u01fa\u01fb\3\2\2\2\u01fb\u01fc\3\2\2\2\u01fc\u01fd\7r\2\2\u01fd\u01fe"+
		"\7q\2\2\u01fe\u01ff\7k\2\2\u01ff\u0200\7p\2\2\u0200\u0201\7v\2\2\u0201"+
		"\u0203\3\2\2\2\u0202\u0204\5\34\16\2\u0203\u0202\3\2\2\2\u0203\u0204\3"+
		"\2\2\2\u0204M\3\2\2\2\u0205\u0206\7^\2\2\u0206\u0207\7k\2\2\u0207\u0208"+
		"\7h\2\2\u0208\u0209\3\2\2\2\u0209\u020a\5\30\f\2\u020a\u020b\3\2\2\2\u020b"+
		"\u020c\b\'\2\2\u020cO\3\2\2\2\u020d\u020e\7\f\2\2\u020eQ\3\2\2\2\u020f"+
		"\u0210\7^\2\2\u0210S\3\2\2\2\u0211\u0213\t\5\2\2\u0212\u0211\3\2\2\2\u0213"+
		"\u0214\3\2\2\2\u0214\u0212\3\2\2\2\u0214\u0215\3\2\2\2\u0215U\3\2\2\2"+
		"\u0216\u0217\13\2\2\2\u0217W\3\2\2\2I\2\3`q\u0080\u008e\u00a4\u00b6\u00ba"+
		"\u00d1\u00e1\u00f1\u0104\u010d\u010f\u0117\u011b\u011d\u0123\u0125\u012e"+
		"\u0130\u0136\u0138\u013c\u0141\u0144\u0147\u014c\u0152\u0155\u0157\u015a"+
		"\u015d\u0161\u0166\u0169\u016d\u0170\u0176\u0179\u017d\u0180\u0184\u0187"+
		"\u018b\u018e\u0192\u0195\u0199\u019e\u01a5\u01a8\u01ae\u01b1\u01b5\u01b8"+
		"\u01be\u01c1\u01c5\u01c8\u01ce\u01d1\u01d7\u01da\u01e0\u01e8\u01f5\u01fa"+
		"\u0203\u0214\7\7\3\2\3\t\2\3\n\3\3\17\4\6\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}