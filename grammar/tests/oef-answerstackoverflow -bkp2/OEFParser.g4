parser grammar OEFParser;

options { tokenVocab=OEFLexer; }


document 		: 	pre EOF ;

pre 			:	title 
					language 
					author  email computeanswer  precision (rangedeclaration)* (integerinstruction | matrixinstruction )*;


//INSTRUCTIONS RULES
title 				: TITLE 			(string | callvariable)* CB;
language 			: LANGUAGE 			string CB;
author				: AUTHOR 			(string)*  CB;
email				: EMAIL 			(string)*  CB;
computeanswer 		: COMPUTEANSWER 	string CB;
precision			: PRECISION 	 	NUM_INT  CB;
rangedeclaration 	: RANGEDECLARATION 	(string)*  CB;
integerinstruction	: INTEGER 			assignementinteger CB;  // {System.out.println("Integer");}
matrixinstruction	: MATRIX 			assignementmatrix CB;
statement 			: STATEMENT 		(string | callvariable)*  CB;


expression
	: expression op=(PLUS | MINUS) expression			#addSub 
	| expression op=(STAR | SLASH) expression			#mulDiv
	| expression POWER expression						#power //operator is right associative normally add <assoc=right> but generate warning
	| (PLUS | MINUS) expression							#pluMin
	| OP expression CP									#parens //{System.out.println("expression-( )");}	
	| callcommand 										#keyRandint //{System.out.println("expression-command");}	
	| callvariable										#id
	| NUM_INT											#numInt
	;	

matrix
	: expression (COMMA expression)* (NEWLINE expression (COMMA expression)*)+			#expMatrix // At least a matrix of 1 col and 2 rows
	;


callvariable
 :  BSLASH ID // {System.out.println("callvariable:"+$ID.text);}	
 ;


relationalExpression
	: relationalExpression rel=(OR | AND) relationalExpression										#orAnd
	| relationalExpression rel=(LESS | LESSEQUAL | GREATER | GREATEREQUAL ) relationalExpression	#lessGreater
    | relationalExpression rel=(DBEQUAL | NOTEQUAL) relationalExpression							#equalNotEqual
  	| OP relationalExpression CP																	#parensRE
    | expression 																					#relExp
    ;

assignementinteger
	: ID EQUAL (expression  | callcommand )*	{System.out.println("asignement:"+$ID.text);}							#assignInteger
	;

assignementmatrix
	: ID EQUAL (matrix  | callcommand )*	{System.out.println("asignement:"+$ID.text);}							#assignMatrix
	;

string
 : CR_OTHER+
 | ID
 ;


///LIST OF COMMANDS
callcommand
 : DRAW expression COMMA expression (NEWLINE callcommandflydraw)* CP
 | RANDINT expression DOTS expression CP	
 ;

callcommandflydraw
:  POINT NUM_INT COMMA NUM_INT
; 


