// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TITLE=1, LANGUAGE=2, AUTHOR=3, EMAIL=4, COMPUTEANSWER=5, PRECISION=6, 
		RANGEDECLARATION=7, INTEGER=8, MATRIX=9, STATEMENT=10, OP=11, S=12, CB=13, 
		CP=14, NUM_INT=15, NUM_REAL=16, COMMA=17, DOTS=18, EQUAL=19, PLUS=20, 
		MINUS=21, STAR=22, SLASH=23, POWER=24, AND=25, OR=26, LESS=27, LESSEQUAL=28, 
		GREATER=29, GREATEREQUAL=30, DBEQUAL=31, NOTEQUAL=32, DRAW=33, RANDINT=34, 
		POINT=35, IF=36, NEWLINE=37, BSLASH=38, ID=39, CR_OTHER=40;
	public static final int
		RULE_document = 0, RULE_pre = 1, RULE_title = 2, RULE_language = 3, RULE_author = 4, 
		RULE_email = 5, RULE_computeanswer = 6, RULE_precision = 7, RULE_rangedeclaration = 8, 
		RULE_integerinstruction = 9, RULE_matrixinstruction = 10, RULE_statement = 11, 
		RULE_expression = 12, RULE_matrix = 13, RULE_callvariable = 14, RULE_relationalExpression = 15, 
		RULE_assignementinteger = 16, RULE_assignementmatrix = 17, RULE_string = 18, 
		RULE_callcommand = 19, RULE_callcommandflydraw = 20;
	private static String[] makeRuleNames() {
		return new String[] {
			"document", "pre", "title", "language", "author", "email", "computeanswer", 
			"precision", "rangedeclaration", "integerinstruction", "matrixinstruction", 
			"statement", "expression", "matrix", "callvariable", "relationalExpression", 
			"assignementinteger", "assignementmatrix", "string", "callcommand", "callcommandflydraw"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, "'('", 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"'^'", null, null, null, null, null, null, null, null, null, null, null, 
			null, "'\n'", "'\\'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "TITLE", "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
			"RANGEDECLARATION", "INTEGER", "MATRIX", "STATEMENT", "OP", "S", "CB", 
			"CP", "NUM_INT", "NUM_REAL", "COMMA", "DOTS", "EQUAL", "PLUS", "MINUS", 
			"STAR", "SLASH", "POWER", "AND", "OR", "LESS", "LESSEQUAL", "GREATER", 
			"GREATEREQUAL", "DBEQUAL", "NOTEQUAL", "DRAW", "RANDINT", "POINT", "IF", 
			"NEWLINE", "BSLASH", "ID", "CR_OTHER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "OEFParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public OEFParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class DocumentContext extends ParserRuleContext {
		public PreContext pre() {
			return getRuleContext(PreContext.class,0);
		}
		public TerminalNode EOF() { return getToken(OEFParser.EOF, 0); }
		public DocumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_document; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterDocument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitDocument(this);
		}
	}

	public final DocumentContext document() throws RecognitionException {
		DocumentContext _localctx = new DocumentContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_document);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42);
			pre();
			setState(43);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreContext extends ParserRuleContext {
		public TitleContext title() {
			return getRuleContext(TitleContext.class,0);
		}
		public LanguageContext language() {
			return getRuleContext(LanguageContext.class,0);
		}
		public AuthorContext author() {
			return getRuleContext(AuthorContext.class,0);
		}
		public EmailContext email() {
			return getRuleContext(EmailContext.class,0);
		}
		public ComputeanswerContext computeanswer() {
			return getRuleContext(ComputeanswerContext.class,0);
		}
		public PrecisionContext precision() {
			return getRuleContext(PrecisionContext.class,0);
		}
		public List<RangedeclarationContext> rangedeclaration() {
			return getRuleContexts(RangedeclarationContext.class);
		}
		public RangedeclarationContext rangedeclaration(int i) {
			return getRuleContext(RangedeclarationContext.class,i);
		}
		public List<IntegerinstructionContext> integerinstruction() {
			return getRuleContexts(IntegerinstructionContext.class);
		}
		public IntegerinstructionContext integerinstruction(int i) {
			return getRuleContext(IntegerinstructionContext.class,i);
		}
		public List<MatrixinstructionContext> matrixinstruction() {
			return getRuleContexts(MatrixinstructionContext.class);
		}
		public MatrixinstructionContext matrixinstruction(int i) {
			return getRuleContext(MatrixinstructionContext.class,i);
		}
		public PreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pre; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPre(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPre(this);
		}
	}

	public final PreContext pre() throws RecognitionException {
		PreContext _localctx = new PreContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_pre);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(45);
			title();
			setState(46);
			language();
			setState(47);
			author();
			setState(48);
			email();
			setState(49);
			computeanswer();
			setState(50);
			precision();
			setState(54);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==RANGEDECLARATION) {
				{
				{
				setState(51);
				rangedeclaration();
				}
				}
				setState(56);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(61);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INTEGER || _la==MATRIX) {
				{
				setState(59);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case INTEGER:
					{
					setState(57);
					integerinstruction();
					}
					break;
				case MATRIX:
					{
					setState(58);
					matrixinstruction();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(63);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TitleContext extends ParserRuleContext {
		public TerminalNode TITLE() { return getToken(OEFParser.TITLE, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public List<CallvariableContext> callvariable() {
			return getRuleContexts(CallvariableContext.class);
		}
		public CallvariableContext callvariable(int i) {
			return getRuleContext(CallvariableContext.class,i);
		}
		public TitleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_title; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterTitle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitTitle(this);
		}
	}

	public final TitleContext title() throws RecognitionException {
		TitleContext _localctx = new TitleContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_title);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			match(TITLE);
			setState(69);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(67);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ID:
				case CR_OTHER:
					{
					setState(65);
					string();
					}
					break;
				case BSLASH:
					{
					setState(66);
					callvariable();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(71);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(72);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LanguageContext extends ParserRuleContext {
		public TerminalNode LANGUAGE() { return getToken(OEFParser.LANGUAGE, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public LanguageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_language; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterLanguage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitLanguage(this);
		}
	}

	public final LanguageContext language() throws RecognitionException {
		LanguageContext _localctx = new LanguageContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_language);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			match(LANGUAGE);
			setState(75);
			string();
			setState(76);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AuthorContext extends ParserRuleContext {
		public TerminalNode AUTHOR() { return getToken(OEFParser.AUTHOR, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public AuthorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_author; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAuthor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAuthor(this);
		}
	}

	public final AuthorContext author() throws RecognitionException {
		AuthorContext _localctx = new AuthorContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_author);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			match(AUTHOR);
			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID || _la==CR_OTHER) {
				{
				{
				setState(79);
				string();
				}
				}
				setState(84);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(85);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EmailContext extends ParserRuleContext {
		public TerminalNode EMAIL() { return getToken(OEFParser.EMAIL, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public EmailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_email; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterEmail(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitEmail(this);
		}
	}

	public final EmailContext email() throws RecognitionException {
		EmailContext _localctx = new EmailContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_email);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			match(EMAIL);
			setState(91);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID || _la==CR_OTHER) {
				{
				{
				setState(88);
				string();
				}
				}
				setState(93);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(94);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComputeanswerContext extends ParserRuleContext {
		public TerminalNode COMPUTEANSWER() { return getToken(OEFParser.COMPUTEANSWER, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public ComputeanswerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_computeanswer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterComputeanswer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitComputeanswer(this);
		}
	}

	public final ComputeanswerContext computeanswer() throws RecognitionException {
		ComputeanswerContext _localctx = new ComputeanswerContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_computeanswer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			match(COMPUTEANSWER);
			setState(97);
			string();
			setState(98);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrecisionContext extends ParserRuleContext {
		public TerminalNode PRECISION() { return getToken(OEFParser.PRECISION, 0); }
		public TerminalNode NUM_INT() { return getToken(OEFParser.NUM_INT, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public PrecisionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precision; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPrecision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPrecision(this);
		}
	}

	public final PrecisionContext precision() throws RecognitionException {
		PrecisionContext _localctx = new PrecisionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_precision);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			match(PRECISION);
			setState(101);
			match(NUM_INT);
			setState(102);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangedeclarationContext extends ParserRuleContext {
		public TerminalNode RANGEDECLARATION() { return getToken(OEFParser.RANGEDECLARATION, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public RangedeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rangedeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterRangedeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitRangedeclaration(this);
		}
	}

	public final RangedeclarationContext rangedeclaration() throws RecognitionException {
		RangedeclarationContext _localctx = new RangedeclarationContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rangedeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			match(RANGEDECLARATION);
			setState(108);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID || _la==CR_OTHER) {
				{
				{
				setState(105);
				string();
				}
				}
				setState(110);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(111);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerinstructionContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(OEFParser.INTEGER, 0); }
		public AssignementintegerContext assignementinteger() {
			return getRuleContext(AssignementintegerContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public IntegerinstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerinstruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterIntegerinstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitIntegerinstruction(this);
		}
	}

	public final IntegerinstructionContext integerinstruction() throws RecognitionException {
		IntegerinstructionContext _localctx = new IntegerinstructionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_integerinstruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			match(INTEGER);
			setState(114);
			assignementinteger();
			setState(115);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixinstructionContext extends ParserRuleContext {
		public TerminalNode MATRIX() { return getToken(OEFParser.MATRIX, 0); }
		public AssignementmatrixContext assignementmatrix() {
			return getRuleContext(AssignementmatrixContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public MatrixinstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixinstruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterMatrixinstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitMatrixinstruction(this);
		}
	}

	public final MatrixinstructionContext matrixinstruction() throws RecognitionException {
		MatrixinstructionContext _localctx = new MatrixinstructionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_matrixinstruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			match(MATRIX);
			setState(118);
			assignementmatrix();
			setState(119);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public TerminalNode STATEMENT() { return getToken(OEFParser.STATEMENT, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public List<CallvariableContext> callvariable() {
			return getRuleContexts(CallvariableContext.class);
		}
		public CallvariableContext callvariable(int i) {
			return getRuleContext(CallvariableContext.class,i);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121);
			match(STATEMENT);
			setState(126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(124);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ID:
				case CR_OTHER:
					{
					setState(122);
					string();
					}
					break;
				case BSLASH:
					{
					setState(123);
					callvariable();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(128);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(129);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NumIntContext extends ExpressionContext {
		public TerminalNode NUM_INT() { return getToken(OEFParser.NUM_INT, 0); }
		public NumIntContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterNumInt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitNumInt(this);
		}
	}
	public static class ParensContext extends ExpressionContext {
		public TerminalNode OP() { return getToken(OEFParser.OP, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public ParensContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterParens(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitParens(this);
		}
	}
	public static class KeyRandintContext extends ExpressionContext {
		public CallcommandContext callcommand() {
			return getRuleContext(CallcommandContext.class,0);
		}
		public KeyRandintContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterKeyRandint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitKeyRandint(this);
		}
	}
	public static class AddSubContext extends ExpressionContext {
		public Token op;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(OEFParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(OEFParser.MINUS, 0); }
		public AddSubContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAddSub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAddSub(this);
		}
	}
	public static class IdContext extends ExpressionContext {
		public CallvariableContext callvariable() {
			return getRuleContext(CallvariableContext.class,0);
		}
		public IdContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitId(this);
		}
	}
	public static class PowerContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode POWER() { return getToken(OEFParser.POWER, 0); }
		public PowerContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPower(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPower(this);
		}
	}
	public static class PluMinContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(OEFParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(OEFParser.MINUS, 0); }
		public PluMinContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPluMin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPluMin(this);
		}
	}
	public static class MulDivContext extends ExpressionContext {
		public Token op;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode STAR() { return getToken(OEFParser.STAR, 0); }
		public TerminalNode SLASH() { return getToken(OEFParser.SLASH, 0); }
		public MulDivContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterMulDiv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitMulDiv(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
			case MINUS:
				{
				_localctx = new PluMinContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(132);
				_la = _input.LA(1);
				if ( !(_la==PLUS || _la==MINUS) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(133);
				expression(5);
				}
				break;
			case OP:
				{
				_localctx = new ParensContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(134);
				match(OP);
				setState(135);
				expression(0);
				setState(136);
				match(CP);
				}
				break;
			case DRAW:
			case RANDINT:
				{
				_localctx = new KeyRandintContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(138);
				callcommand();
				}
				break;
			case BSLASH:
				{
				_localctx = new IdContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(139);
				callvariable();
				}
				break;
			case NUM_INT:
				{
				_localctx = new NumIntContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(140);
				match(NUM_INT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(154);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(152);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
					case 1:
						{
						_localctx = new AddSubContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(143);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(144);
						((AddSubContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
							((AddSubContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(145);
						expression(9);
						}
						break;
					case 2:
						{
						_localctx = new MulDivContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(146);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(147);
						((MulDivContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==STAR || _la==SLASH) ) {
							((MulDivContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(148);
						expression(8);
						}
						break;
					case 3:
						{
						_localctx = new PowerContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(149);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(150);
						match(POWER);
						setState(151);
						expression(7);
						}
						break;
					}
					} 
				}
				setState(156);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MatrixContext extends ParserRuleContext {
		public MatrixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrix; }
	 
		public MatrixContext() { }
		public void copyFrom(MatrixContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExpMatrixContext extends MatrixContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(OEFParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(OEFParser.COMMA, i);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(OEFParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(OEFParser.NEWLINE, i);
		}
		public ExpMatrixContext(MatrixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterExpMatrix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitExpMatrix(this);
		}
	}

	public final MatrixContext matrix() throws RecognitionException {
		MatrixContext _localctx = new MatrixContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_matrix);
		int _la;
		try {
			_localctx = new ExpMatrixContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			expression(0);
			setState(162);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(158);
				match(COMMA);
				setState(159);
				expression(0);
				}
				}
				setState(164);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(174); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(165);
				match(NEWLINE);
				setState(166);
				expression(0);
				setState(171);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(167);
					match(COMMA);
					setState(168);
					expression(0);
					}
					}
					setState(173);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				setState(176); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==NEWLINE );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallvariableContext extends ParserRuleContext {
		public TerminalNode BSLASH() { return getToken(OEFParser.BSLASH, 0); }
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public CallvariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callvariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallvariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallvariable(this);
		}
	}

	public final CallvariableContext callvariable() throws RecognitionException {
		CallvariableContext _localctx = new CallvariableContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_callvariable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			match(BSLASH);
			setState(179);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
	 
		public RelationalExpressionContext() { }
		public void copyFrom(RelationalExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EqualNotEqualContext extends RelationalExpressionContext {
		public Token rel;
		public List<RelationalExpressionContext> relationalExpression() {
			return getRuleContexts(RelationalExpressionContext.class);
		}
		public RelationalExpressionContext relationalExpression(int i) {
			return getRuleContext(RelationalExpressionContext.class,i);
		}
		public TerminalNode DBEQUAL() { return getToken(OEFParser.DBEQUAL, 0); }
		public TerminalNode NOTEQUAL() { return getToken(OEFParser.NOTEQUAL, 0); }
		public EqualNotEqualContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterEqualNotEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitEqualNotEqual(this);
		}
	}
	public static class OrAndContext extends RelationalExpressionContext {
		public Token rel;
		public List<RelationalExpressionContext> relationalExpression() {
			return getRuleContexts(RelationalExpressionContext.class);
		}
		public RelationalExpressionContext relationalExpression(int i) {
			return getRuleContext(RelationalExpressionContext.class,i);
		}
		public TerminalNode OR() { return getToken(OEFParser.OR, 0); }
		public TerminalNode AND() { return getToken(OEFParser.AND, 0); }
		public OrAndContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterOrAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitOrAnd(this);
		}
	}
	public static class ParensREContext extends RelationalExpressionContext {
		public TerminalNode OP() { return getToken(OEFParser.OP, 0); }
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public ParensREContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterParensRE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitParensRE(this);
		}
	}
	public static class LessGreaterContext extends RelationalExpressionContext {
		public Token rel;
		public List<RelationalExpressionContext> relationalExpression() {
			return getRuleContexts(RelationalExpressionContext.class);
		}
		public RelationalExpressionContext relationalExpression(int i) {
			return getRuleContext(RelationalExpressionContext.class,i);
		}
		public TerminalNode LESS() { return getToken(OEFParser.LESS, 0); }
		public TerminalNode LESSEQUAL() { return getToken(OEFParser.LESSEQUAL, 0); }
		public TerminalNode GREATER() { return getToken(OEFParser.GREATER, 0); }
		public TerminalNode GREATEREQUAL() { return getToken(OEFParser.GREATEREQUAL, 0); }
		public LessGreaterContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterLessGreater(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitLessGreater(this);
		}
	}
	public static class RelExpContext extends RelationalExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RelExpContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterRelExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitRelExp(this);
		}
	}

	public final RelationalExpressionContext relationalExpression() throws RecognitionException {
		return relationalExpression(0);
	}

	private RelationalExpressionContext relationalExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, _parentState);
		RelationalExpressionContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_relationalExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				_localctx = new ParensREContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(182);
				match(OP);
				setState(183);
				relationalExpression(0);
				setState(184);
				match(CP);
				}
				break;
			case 2:
				{
				_localctx = new RelExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(186);
				expression(0);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(200);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(198);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
					case 1:
						{
						_localctx = new OrAndContext(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(189);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(190);
						((OrAndContext)_localctx).rel = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
							((OrAndContext)_localctx).rel = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(191);
						relationalExpression(6);
						}
						break;
					case 2:
						{
						_localctx = new LessGreaterContext(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(192);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(193);
						((LessGreaterContext)_localctx).rel = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LESS) | (1L << LESSEQUAL) | (1L << GREATER) | (1L << GREATEREQUAL))) != 0)) ) {
							((LessGreaterContext)_localctx).rel = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(194);
						relationalExpression(5);
						}
						break;
					case 3:
						{
						_localctx = new EqualNotEqualContext(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(195);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(196);
						((EqualNotEqualContext)_localctx).rel = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==DBEQUAL || _la==NOTEQUAL) ) {
							((EqualNotEqualContext)_localctx).rel = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(197);
						relationalExpression(4);
						}
						break;
					}
					} 
				}
				setState(202);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AssignementintegerContext extends ParserRuleContext {
		public AssignementintegerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignementinteger; }
	 
		public AssignementintegerContext() { }
		public void copyFrom(AssignementintegerContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssignIntegerContext extends AssignementintegerContext {
		public Token ID;
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(OEFParser.EQUAL, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<CallcommandContext> callcommand() {
			return getRuleContexts(CallcommandContext.class);
		}
		public CallcommandContext callcommand(int i) {
			return getRuleContext(CallcommandContext.class,i);
		}
		public AssignIntegerContext(AssignementintegerContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAssignInteger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAssignInteger(this);
		}
	}

	public final AssignementintegerContext assignementinteger() throws RecognitionException {
		AssignementintegerContext _localctx = new AssignementintegerContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_assignementinteger);
		int _la;
		try {
			_localctx = new AssignIntegerContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(203);
			((AssignIntegerContext)_localctx).ID = match(ID);
			setState(204);
			match(EQUAL);
			setState(209);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OP) | (1L << NUM_INT) | (1L << PLUS) | (1L << MINUS) | (1L << DRAW) | (1L << RANDINT) | (1L << BSLASH))) != 0)) {
				{
				setState(207);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
				case 1:
					{
					setState(205);
					expression(0);
					}
					break;
				case 2:
					{
					setState(206);
					callcommand();
					}
					break;
				}
				}
				setState(211);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			System.out.println("asignement:"+(((AssignIntegerContext)_localctx).ID!=null?((AssignIntegerContext)_localctx).ID.getText():null));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignementmatrixContext extends ParserRuleContext {
		public AssignementmatrixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignementmatrix; }
	 
		public AssignementmatrixContext() { }
		public void copyFrom(AssignementmatrixContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssignMatrixContext extends AssignementmatrixContext {
		public Token ID;
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(OEFParser.EQUAL, 0); }
		public List<MatrixContext> matrix() {
			return getRuleContexts(MatrixContext.class);
		}
		public MatrixContext matrix(int i) {
			return getRuleContext(MatrixContext.class,i);
		}
		public List<CallcommandContext> callcommand() {
			return getRuleContexts(CallcommandContext.class);
		}
		public CallcommandContext callcommand(int i) {
			return getRuleContext(CallcommandContext.class,i);
		}
		public AssignMatrixContext(AssignementmatrixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAssignMatrix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAssignMatrix(this);
		}
	}

	public final AssignementmatrixContext assignementmatrix() throws RecognitionException {
		AssignementmatrixContext _localctx = new AssignementmatrixContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_assignementmatrix);
		int _la;
		try {
			_localctx = new AssignMatrixContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(214);
			((AssignMatrixContext)_localctx).ID = match(ID);
			setState(215);
			match(EQUAL);
			setState(220);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OP) | (1L << NUM_INT) | (1L << PLUS) | (1L << MINUS) | (1L << DRAW) | (1L << RANDINT) | (1L << BSLASH))) != 0)) {
				{
				setState(218);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
				case 1:
					{
					setState(216);
					matrix();
					}
					break;
				case 2:
					{
					setState(217);
					callcommand();
					}
					break;
				}
				}
				setState(222);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			System.out.println("asignement:"+(((AssignMatrixContext)_localctx).ID!=null?((AssignMatrixContext)_localctx).ID.getText():null));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public List<TerminalNode> CR_OTHER() { return getTokens(OEFParser.CR_OTHER); }
		public TerminalNode CR_OTHER(int i) {
			return getToken(OEFParser.CR_OTHER, i);
		}
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_string);
		try {
			int _alt;
			setState(231);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CR_OTHER:
				enterOuterAlt(_localctx, 1);
				{
				setState(226); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(225);
						match(CR_OTHER);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(228); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(230);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallcommandContext extends ParserRuleContext {
		public TerminalNode DRAW() { return getToken(OEFParser.DRAW, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode COMMA() { return getToken(OEFParser.COMMA, 0); }
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(OEFParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(OEFParser.NEWLINE, i);
		}
		public List<CallcommandflydrawContext> callcommandflydraw() {
			return getRuleContexts(CallcommandflydrawContext.class);
		}
		public CallcommandflydrawContext callcommandflydraw(int i) {
			return getRuleContext(CallcommandflydrawContext.class,i);
		}
		public TerminalNode RANDINT() { return getToken(OEFParser.RANDINT, 0); }
		public TerminalNode DOTS() { return getToken(OEFParser.DOTS, 0); }
		public CallcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallcommand(this);
		}
	}

	public final CallcommandContext callcommand() throws RecognitionException {
		CallcommandContext _localctx = new CallcommandContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_callcommand);
		int _la;
		try {
			setState(252);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DRAW:
				enterOuterAlt(_localctx, 1);
				{
				setState(233);
				match(DRAW);
				setState(234);
				expression(0);
				setState(235);
				match(COMMA);
				setState(236);
				expression(0);
				setState(241);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NEWLINE) {
					{
					{
					setState(237);
					match(NEWLINE);
					setState(238);
					callcommandflydraw();
					}
					}
					setState(243);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(244);
				match(CP);
				}
				break;
			case RANDINT:
				enterOuterAlt(_localctx, 2);
				{
				setState(246);
				match(RANDINT);
				setState(247);
				expression(0);
				setState(248);
				match(DOTS);
				setState(249);
				expression(0);
				setState(250);
				match(CP);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallcommandflydrawContext extends ParserRuleContext {
		public TerminalNode POINT() { return getToken(OEFParser.POINT, 0); }
		public List<TerminalNode> NUM_INT() { return getTokens(OEFParser.NUM_INT); }
		public TerminalNode NUM_INT(int i) {
			return getToken(OEFParser.NUM_INT, i);
		}
		public TerminalNode COMMA() { return getToken(OEFParser.COMMA, 0); }
		public CallcommandflydrawContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callcommandflydraw; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallcommandflydraw(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallcommandflydraw(this);
		}
	}

	public final CallcommandflydrawContext callcommandflydraw() throws RecognitionException {
		CallcommandflydrawContext _localctx = new CallcommandflydrawContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_callcommandflydraw);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(254);
			match(POINT);
			setState(255);
			match(NUM_INT);
			setState(256);
			match(COMMA);
			setState(257);
			match(NUM_INT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 12:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		case 15:
			return relationalExpression_sempred((RelationalExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		}
		return true;
	}
	private boolean relationalExpression_sempred(RelationalExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 4);
		case 5:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3*\u0106\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\7\3\67\n\3\f\3\16\3:\13\3\3\3\3\3\7\3>\n\3\f\3\16\3A\13\3\3"+
		"\4\3\4\3\4\7\4F\n\4\f\4\16\4I\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\7\6"+
		"S\n\6\f\6\16\6V\13\6\3\6\3\6\3\7\3\7\7\7\\\n\7\f\7\16\7_\13\7\3\7\3\7"+
		"\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\7\nm\n\n\f\n\16\np\13\n\3\n\3"+
		"\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\7\r\177\n\r\f\r\16"+
		"\r\u0082\13\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\5\16\u0090\n\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u009b"+
		"\n\16\f\16\16\16\u009e\13\16\3\17\3\17\3\17\7\17\u00a3\n\17\f\17\16\17"+
		"\u00a6\13\17\3\17\3\17\3\17\3\17\7\17\u00ac\n\17\f\17\16\17\u00af\13\17"+
		"\6\17\u00b1\n\17\r\17\16\17\u00b2\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3"+
		"\21\3\21\5\21\u00be\n\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\7\21\u00c9\n\21\f\21\16\21\u00cc\13\21\3\22\3\22\3\22\3\22\7\22\u00d2"+
		"\n\22\f\22\16\22\u00d5\13\22\3\22\3\22\3\23\3\23\3\23\3\23\7\23\u00dd"+
		"\n\23\f\23\16\23\u00e0\13\23\3\23\3\23\3\24\6\24\u00e5\n\24\r\24\16\24"+
		"\u00e6\3\24\5\24\u00ea\n\24\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u00f2\n"+
		"\25\f\25\16\25\u00f5\13\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25"+
		"\u00ff\n\25\3\26\3\26\3\26\3\26\3\26\3\26\2\4\32 \27\2\4\6\b\n\f\16\20"+
		"\22\24\26\30\32\34\36 \"$&(*\2\7\3\2\26\27\3\2\30\31\3\2\33\34\3\2\35"+
		" \3\2!\"\2\u0110\2,\3\2\2\2\4/\3\2\2\2\6B\3\2\2\2\bL\3\2\2\2\nP\3\2\2"+
		"\2\fY\3\2\2\2\16b\3\2\2\2\20f\3\2\2\2\22j\3\2\2\2\24s\3\2\2\2\26w\3\2"+
		"\2\2\30{\3\2\2\2\32\u008f\3\2\2\2\34\u009f\3\2\2\2\36\u00b4\3\2\2\2 \u00bd"+
		"\3\2\2\2\"\u00cd\3\2\2\2$\u00d8\3\2\2\2&\u00e9\3\2\2\2(\u00fe\3\2\2\2"+
		"*\u0100\3\2\2\2,-\5\4\3\2-.\7\2\2\3.\3\3\2\2\2/\60\5\6\4\2\60\61\5\b\5"+
		"\2\61\62\5\n\6\2\62\63\5\f\7\2\63\64\5\16\b\2\648\5\20\t\2\65\67\5\22"+
		"\n\2\66\65\3\2\2\2\67:\3\2\2\28\66\3\2\2\289\3\2\2\29?\3\2\2\2:8\3\2\2"+
		"\2;>\5\24\13\2<>\5\26\f\2=;\3\2\2\2=<\3\2\2\2>A\3\2\2\2?=\3\2\2\2?@\3"+
		"\2\2\2@\5\3\2\2\2A?\3\2\2\2BG\7\3\2\2CF\5&\24\2DF\5\36\20\2EC\3\2\2\2"+
		"ED\3\2\2\2FI\3\2\2\2GE\3\2\2\2GH\3\2\2\2HJ\3\2\2\2IG\3\2\2\2JK\7\17\2"+
		"\2K\7\3\2\2\2LM\7\4\2\2MN\5&\24\2NO\7\17\2\2O\t\3\2\2\2PT\7\5\2\2QS\5"+
		"&\24\2RQ\3\2\2\2SV\3\2\2\2TR\3\2\2\2TU\3\2\2\2UW\3\2\2\2VT\3\2\2\2WX\7"+
		"\17\2\2X\13\3\2\2\2Y]\7\6\2\2Z\\\5&\24\2[Z\3\2\2\2\\_\3\2\2\2][\3\2\2"+
		"\2]^\3\2\2\2^`\3\2\2\2_]\3\2\2\2`a\7\17\2\2a\r\3\2\2\2bc\7\7\2\2cd\5&"+
		"\24\2de\7\17\2\2e\17\3\2\2\2fg\7\b\2\2gh\7\21\2\2hi\7\17\2\2i\21\3\2\2"+
		"\2jn\7\t\2\2km\5&\24\2lk\3\2\2\2mp\3\2\2\2nl\3\2\2\2no\3\2\2\2oq\3\2\2"+
		"\2pn\3\2\2\2qr\7\17\2\2r\23\3\2\2\2st\7\n\2\2tu\5\"\22\2uv\7\17\2\2v\25"+
		"\3\2\2\2wx\7\13\2\2xy\5$\23\2yz\7\17\2\2z\27\3\2\2\2{\u0080\7\f\2\2|\177"+
		"\5&\24\2}\177\5\36\20\2~|\3\2\2\2~}\3\2\2\2\177\u0082\3\2\2\2\u0080~\3"+
		"\2\2\2\u0080\u0081\3\2\2\2\u0081\u0083\3\2\2\2\u0082\u0080\3\2\2\2\u0083"+
		"\u0084\7\17\2\2\u0084\31\3\2\2\2\u0085\u0086\b\16\1\2\u0086\u0087\t\2"+
		"\2\2\u0087\u0090\5\32\16\7\u0088\u0089\7\r\2\2\u0089\u008a\5\32\16\2\u008a"+
		"\u008b\7\20\2\2\u008b\u0090\3\2\2\2\u008c\u0090\5(\25\2\u008d\u0090\5"+
		"\36\20\2\u008e\u0090\7\21\2\2\u008f\u0085\3\2\2\2\u008f\u0088\3\2\2\2"+
		"\u008f\u008c\3\2\2\2\u008f\u008d\3\2\2\2\u008f\u008e\3\2\2\2\u0090\u009c"+
		"\3\2\2\2\u0091\u0092\f\n\2\2\u0092\u0093\t\2\2\2\u0093\u009b\5\32\16\13"+
		"\u0094\u0095\f\t\2\2\u0095\u0096\t\3\2\2\u0096\u009b\5\32\16\n\u0097\u0098"+
		"\f\b\2\2\u0098\u0099\7\32\2\2\u0099\u009b\5\32\16\t\u009a\u0091\3\2\2"+
		"\2\u009a\u0094\3\2\2\2\u009a\u0097\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a"+
		"\3\2\2\2\u009c\u009d\3\2\2\2\u009d\33\3\2\2\2\u009e\u009c\3\2\2\2\u009f"+
		"\u00a4\5\32\16\2\u00a0\u00a1\7\23\2\2\u00a1\u00a3\5\32\16\2\u00a2\u00a0"+
		"\3\2\2\2\u00a3\u00a6\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5"+
		"\u00b0\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a7\u00a8\7\'\2\2\u00a8\u00ad\5\32"+
		"\16\2\u00a9\u00aa\7\23\2\2\u00aa\u00ac\5\32\16\2\u00ab\u00a9\3\2\2\2\u00ac"+
		"\u00af\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00b1\3\2"+
		"\2\2\u00af\u00ad\3\2\2\2\u00b0\u00a7\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2"+
		"\u00b0\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\35\3\2\2\2\u00b4\u00b5\7(\2\2"+
		"\u00b5\u00b6\7)\2\2\u00b6\37\3\2\2\2\u00b7\u00b8\b\21\1\2\u00b8\u00b9"+
		"\7\r\2\2\u00b9\u00ba\5 \21\2\u00ba\u00bb\7\20\2\2\u00bb\u00be\3\2\2\2"+
		"\u00bc\u00be\5\32\16\2\u00bd\u00b7\3\2\2\2\u00bd\u00bc\3\2\2\2\u00be\u00ca"+
		"\3\2\2\2\u00bf\u00c0\f\7\2\2\u00c0\u00c1\t\4\2\2\u00c1\u00c9\5 \21\b\u00c2"+
		"\u00c3\f\6\2\2\u00c3\u00c4\t\5\2\2\u00c4\u00c9\5 \21\7\u00c5\u00c6\f\5"+
		"\2\2\u00c6\u00c7\t\6\2\2\u00c7\u00c9\5 \21\6\u00c8\u00bf\3\2\2\2\u00c8"+
		"\u00c2\3\2\2\2\u00c8\u00c5\3\2\2\2\u00c9\u00cc\3\2\2\2\u00ca\u00c8\3\2"+
		"\2\2\u00ca\u00cb\3\2\2\2\u00cb!\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cd\u00ce"+
		"\7)\2\2\u00ce\u00d3\7\25\2\2\u00cf\u00d2\5\32\16\2\u00d0\u00d2\5(\25\2"+
		"\u00d1\u00cf\3\2\2\2\u00d1\u00d0\3\2\2\2\u00d2\u00d5\3\2\2\2\u00d3\u00d1"+
		"\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d6\3\2\2\2\u00d5\u00d3\3\2\2\2\u00d6"+
		"\u00d7\b\22\1\2\u00d7#\3\2\2\2\u00d8\u00d9\7)\2\2\u00d9\u00de\7\25\2\2"+
		"\u00da\u00dd\5\34\17\2\u00db\u00dd\5(\25\2\u00dc\u00da\3\2\2\2\u00dc\u00db"+
		"\3\2\2\2\u00dd\u00e0\3\2\2\2\u00de\u00dc\3\2\2\2\u00de\u00df\3\2\2\2\u00df"+
		"\u00e1\3\2\2\2\u00e0\u00de\3\2\2\2\u00e1\u00e2\b\23\1\2\u00e2%\3\2\2\2"+
		"\u00e3\u00e5\7*\2\2\u00e4\u00e3\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00e4"+
		"\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00ea\3\2\2\2\u00e8\u00ea\7)\2\2\u00e9"+
		"\u00e4\3\2\2\2\u00e9\u00e8\3\2\2\2\u00ea\'\3\2\2\2\u00eb\u00ec\7#\2\2"+
		"\u00ec\u00ed\5\32\16\2\u00ed\u00ee\7\23\2\2\u00ee\u00f3\5\32\16\2\u00ef"+
		"\u00f0\7\'\2\2\u00f0\u00f2\5*\26\2\u00f1\u00ef\3\2\2\2\u00f2\u00f5\3\2"+
		"\2\2\u00f3\u00f1\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f6\3\2\2\2\u00f5"+
		"\u00f3\3\2\2\2\u00f6\u00f7\7\20\2\2\u00f7\u00ff\3\2\2\2\u00f8\u00f9\7"+
		"$\2\2\u00f9\u00fa\5\32\16\2\u00fa\u00fb\7\24\2\2\u00fb\u00fc\5\32\16\2"+
		"\u00fc\u00fd\7\20\2\2\u00fd\u00ff\3\2\2\2\u00fe\u00eb\3\2\2\2\u00fe\u00f8"+
		"\3\2\2\2\u00ff)\3\2\2\2\u0100\u0101\7%\2\2\u0101\u0102\7\21\2\2\u0102"+
		"\u0103\7\23\2\2\u0103\u0104\7\21\2\2\u0104+\3\2\2\2\358=?EGT]n~\u0080"+
		"\u008f\u009a\u009c\u00a4\u00ad\u00b2\u00bd\u00c8\u00ca\u00d1\u00d3\u00dc"+
		"\u00de\u00e6\u00e9\u00f3\u00fe";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}