// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OEFParser}.
 */
public interface OEFParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void enterDocument(OEFParser.DocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void exitDocument(OEFParser.DocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#pre}.
	 * @param ctx the parse tree
	 */
	void enterPre(OEFParser.PreContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#pre}.
	 * @param ctx the parse tree
	 */
	void exitPre(OEFParser.PreContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#title}.
	 * @param ctx the parse tree
	 */
	void enterTitle(OEFParser.TitleContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#title}.
	 * @param ctx the parse tree
	 */
	void exitTitle(OEFParser.TitleContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#language}.
	 * @param ctx the parse tree
	 */
	void enterLanguage(OEFParser.LanguageContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#language}.
	 * @param ctx the parse tree
	 */
	void exitLanguage(OEFParser.LanguageContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#author}.
	 * @param ctx the parse tree
	 */
	void enterAuthor(OEFParser.AuthorContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#author}.
	 * @param ctx the parse tree
	 */
	void exitAuthor(OEFParser.AuthorContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#email}.
	 * @param ctx the parse tree
	 */
	void enterEmail(OEFParser.EmailContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#email}.
	 * @param ctx the parse tree
	 */
	void exitEmail(OEFParser.EmailContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#computeanswer}.
	 * @param ctx the parse tree
	 */
	void enterComputeanswer(OEFParser.ComputeanswerContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#computeanswer}.
	 * @param ctx the parse tree
	 */
	void exitComputeanswer(OEFParser.ComputeanswerContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#precision}.
	 * @param ctx the parse tree
	 */
	void enterPrecision(OEFParser.PrecisionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#precision}.
	 * @param ctx the parse tree
	 */
	void exitPrecision(OEFParser.PrecisionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#rangedeclaration}.
	 * @param ctx the parse tree
	 */
	void enterRangedeclaration(OEFParser.RangedeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#rangedeclaration}.
	 * @param ctx the parse tree
	 */
	void exitRangedeclaration(OEFParser.RangedeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#integerinstruction}.
	 * @param ctx the parse tree
	 */
	void enterIntegerinstruction(OEFParser.IntegerinstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#integerinstruction}.
	 * @param ctx the parse tree
	 */
	void exitIntegerinstruction(OEFParser.IntegerinstructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#matrixinstruction}.
	 * @param ctx the parse tree
	 */
	void enterMatrixinstruction(OEFParser.MatrixinstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#matrixinstruction}.
	 * @param ctx the parse tree
	 */
	void exitMatrixinstruction(OEFParser.MatrixinstructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(OEFParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(OEFParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numInt}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumInt(OEFParser.NumIntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numInt}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumInt(OEFParser.NumIntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parens}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParens(OEFParser.ParensContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parens}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParens(OEFParser.ParensContext ctx);
	/**
	 * Enter a parse tree produced by the {@code keyRandint}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterKeyRandint(OEFParser.KeyRandintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code keyRandint}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitKeyRandint(OEFParser.KeyRandintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addSub}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddSub(OEFParser.AddSubContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addSub}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddSub(OEFParser.AddSubContext ctx);
	/**
	 * Enter a parse tree produced by the {@code id}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterId(OEFParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code id}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitId(OEFParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code power}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPower(OEFParser.PowerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code power}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPower(OEFParser.PowerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code pluMin}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPluMin(OEFParser.PluMinContext ctx);
	/**
	 * Exit a parse tree produced by the {@code pluMin}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPluMin(OEFParser.PluMinContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mulDiv}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMulDiv(OEFParser.MulDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mulDiv}
	 * labeled alternative in {@link OEFParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMulDiv(OEFParser.MulDivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expMatrix}
	 * labeled alternative in {@link OEFParser#matrix}.
	 * @param ctx the parse tree
	 */
	void enterExpMatrix(OEFParser.ExpMatrixContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expMatrix}
	 * labeled alternative in {@link OEFParser#matrix}.
	 * @param ctx the parse tree
	 */
	void exitExpMatrix(OEFParser.ExpMatrixContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#callvariable}.
	 * @param ctx the parse tree
	 */
	void enterCallvariable(OEFParser.CallvariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#callvariable}.
	 * @param ctx the parse tree
	 */
	void exitCallvariable(OEFParser.CallvariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalNotEqual}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqualNotEqual(OEFParser.EqualNotEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalNotEqual}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqualNotEqual(OEFParser.EqualNotEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orAnd}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterOrAnd(OEFParser.OrAndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orAnd}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitOrAnd(OEFParser.OrAndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parensRE}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterParensRE(OEFParser.ParensREContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parensRE}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitParensRE(OEFParser.ParensREContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lessGreater}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterLessGreater(OEFParser.LessGreaterContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lessGreater}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitLessGreater(OEFParser.LessGreaterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relExp}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelExp(OEFParser.RelExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relExp}
	 * labeled alternative in {@link OEFParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelExp(OEFParser.RelExpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignInteger}
	 * labeled alternative in {@link OEFParser#assignementinteger}.
	 * @param ctx the parse tree
	 */
	void enterAssignInteger(OEFParser.AssignIntegerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignInteger}
	 * labeled alternative in {@link OEFParser#assignementinteger}.
	 * @param ctx the parse tree
	 */
	void exitAssignInteger(OEFParser.AssignIntegerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignMatrix}
	 * labeled alternative in {@link OEFParser#assignementmatrix}.
	 * @param ctx the parse tree
	 */
	void enterAssignMatrix(OEFParser.AssignMatrixContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignMatrix}
	 * labeled alternative in {@link OEFParser#assignementmatrix}.
	 * @param ctx the parse tree
	 */
	void exitAssignMatrix(OEFParser.AssignMatrixContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(OEFParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(OEFParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#callcommand}.
	 * @param ctx the parse tree
	 */
	void enterCallcommand(OEFParser.CallcommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#callcommand}.
	 * @param ctx the parse tree
	 */
	void exitCallcommand(OEFParser.CallcommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#callcommandflydraw}.
	 * @param ctx the parse tree
	 */
	void enterCallcommandflydraw(OEFParser.CallcommandflydrawContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#callcommandflydraw}.
	 * @param ctx the parse tree
	 */
	void exitCallcommandflydraw(OEFParser.CallcommandflydrawContext ctx);
}