// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SEA_WS=1, TITLE=2, OB=3, OP=4, BSLASH=5, TEXT=6, CLOSECALLVAR=7, CB=8, 
		CP=9, DRAW=10, IF=11, ID=12;
	public static final int
		RULE_document = 0, RULE_string = 1, RULE_var = 2, RULE_commandDraw = 3, 
		RULE_commandIf = 4, RULE_callreference = 5;
	private static String[] makeRuleNames() {
		return new String[] {
			"document", "string", "var", "commandDraw", "commandIf", "callreference"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, "'\\title'", "'{'", "'('", "'\\'", null, "' '", "'}'", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "SEA_WS", "TITLE", "OB", "OP", "BSLASH", "TEXT", "CLOSECALLVAR", 
			"CB", "CP", "DRAW", "IF", "ID"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "OEFParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public OEFParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class DocumentContext extends ParserRuleContext {
		public TerminalNode TITLE() { return getToken(OEFParser.TITLE, 0); }
		public TerminalNode OB() { return getToken(OEFParser.OB, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public DocumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_document; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterDocument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitDocument(this);
		}
	}

	public final DocumentContext document() throws RecognitionException {
		DocumentContext _localctx = new DocumentContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_document);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(12);
			match(TITLE);
			setState(13);
			match(OB);
			setState(18);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BSLASH || _la==TEXT) {
				{
				setState(16);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case BSLASH:
					{
					setState(14);
					callreference();
					}
					break;
				case TEXT:
					{
					setState(15);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(20);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(21);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public TerminalNode TEXT() { return getToken(OEFParser.TEXT, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_string);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23);
			match(TEXT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public VarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitVar(this);
		}
	}

	public final VarContext var() throws RecognitionException {
		VarContext _localctx = new VarContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandDrawContext extends ParserRuleContext {
		public TerminalNode DRAW() { return getToken(OEFParser.DRAW, 0); }
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public CommandDrawContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandDraw; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCommandDraw(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCommandDraw(this);
		}
	}

	public final CommandDrawContext commandDraw() throws RecognitionException {
		CommandDrawContext _localctx = new CommandDrawContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_commandDraw);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(27);
			match(DRAW);
			setState(32);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BSLASH || _la==TEXT) {
				{
				setState(30);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case BSLASH:
					{
					setState(28);
					callreference();
					}
					break;
				case TEXT:
					{
					setState(29);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(34);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(35);
			match(CP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandIfContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(OEFParser.IF, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<CallreferenceContext> callreference() {
			return getRuleContexts(CallreferenceContext.class);
		}
		public CallreferenceContext callreference(int i) {
			return getRuleContext(CallreferenceContext.class,i);
		}
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public CommandIfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandIf; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCommandIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCommandIf(this);
		}
	}

	public final CommandIfContext commandIf() throws RecognitionException {
		CommandIfContext _localctx = new CommandIfContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_commandIf);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37);
			match(IF);
			setState(42);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BSLASH || _la==TEXT) {
				{
				setState(40);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case BSLASH:
					{
					setState(38);
					callreference();
					}
					break;
				case TEXT:
					{
					setState(39);
					string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(44);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(45);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallreferenceContext extends ParserRuleContext {
		public TerminalNode BSLASH() { return getToken(OEFParser.BSLASH, 0); }
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public CommandDrawContext commandDraw() {
			return getRuleContext(CommandDrawContext.class,0);
		}
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public CommandIfContext commandIf() {
			return getRuleContext(CommandIfContext.class,0);
		}
		public CallreferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callreference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallreference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallreference(this);
		}
	}

	public final CallreferenceContext callreference() throws RecognitionException {
		CallreferenceContext _localctx = new CallreferenceContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_callreference);
		try {
			setState(57);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(47);
				match(BSLASH);
				setState(48);
				match(ID);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(49);
				match(BSLASH);
				setState(50);
				commandDraw();
				setState(51);
				match(CP);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(53);
				match(BSLASH);
				setState(54);
				commandIf();
				setState(55);
				match(CP);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\16>\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\3\2\3\2\3\2\7\2\23\n\2\f\2\16\2"+
		"\26\13\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\7\5!\n\5\f\5\16\5$\13\5\3"+
		"\5\3\5\3\6\3\6\3\6\7\6+\n\6\f\6\16\6.\13\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\5\7<\n\7\3\7\2\2\b\2\4\6\b\n\f\2\2\2?\2\16\3\2\2"+
		"\2\4\31\3\2\2\2\6\33\3\2\2\2\b\35\3\2\2\2\n\'\3\2\2\2\f;\3\2\2\2\16\17"+
		"\7\4\2\2\17\24\7\5\2\2\20\23\5\f\7\2\21\23\5\4\3\2\22\20\3\2\2\2\22\21"+
		"\3\2\2\2\23\26\3\2\2\2\24\22\3\2\2\2\24\25\3\2\2\2\25\27\3\2\2\2\26\24"+
		"\3\2\2\2\27\30\7\n\2\2\30\3\3\2\2\2\31\32\7\b\2\2\32\5\3\2\2\2\33\34\7"+
		"\16\2\2\34\7\3\2\2\2\35\"\7\f\2\2\36!\5\f\7\2\37!\5\4\3\2 \36\3\2\2\2"+
		" \37\3\2\2\2!$\3\2\2\2\" \3\2\2\2\"#\3\2\2\2#%\3\2\2\2$\"\3\2\2\2%&\7"+
		"\13\2\2&\t\3\2\2\2\',\7\r\2\2(+\5\f\7\2)+\5\4\3\2*(\3\2\2\2*)\3\2\2\2"+
		"+.\3\2\2\2,*\3\2\2\2,-\3\2\2\2-/\3\2\2\2.,\3\2\2\2/\60\7\n\2\2\60\13\3"+
		"\2\2\2\61\62\7\7\2\2\62<\7\16\2\2\63\64\7\7\2\2\64\65\5\b\5\2\65\66\7"+
		"\13\2\2\66<\3\2\2\2\678\7\7\2\289\5\n\6\29:\7\13\2\2:<\3\2\2\2;\61\3\2"+
		"\2\2;\63\3\2\2\2;\67\3\2\2\2<\r\3\2\2\2\t\22\24 \"*,;";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}