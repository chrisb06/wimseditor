parser grammar OEFParser;
/*==============================================================*/
/*=================== WIMS Commands ============================*/
/*==============================================================*/
options { tokenVocab=OEFLexer; }

document: TITLE OB ( callreference | string )* CB;

string 	: TEXT;
var		: ID;
commandDraw : DRAW ( callreference | string )* CP ;
commandIf	: IF ( callreference | string )* CB ;

callreference : BSLASH ID | BSLASH commandDraw CP | BSLASH commandIf CP;