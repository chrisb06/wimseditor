// Generated from OEFLexer.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SEA_WS=1, TITLE=2, OB=3, OP=4, BSLASH=5, TEXT=6, CLOSECALLVAR=7, CB=8, 
		CP=9, DRAW=10, IF=11, ID=12;
	public static final int
		CALLREFERENCE=1;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE", "CALLREFERENCE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"SEA_WS", "TITLE", "OB", "OP", "BSLASH", "TEXT", "CLOSECALLVAR", "CB", 
			"CP", "DRAW", "IF", "ID"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, "'\\title'", "'{'", "'('", "'\\'", null, "' '", "'}'", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "SEA_WS", "TITLE", "OB", "OP", "BSLASH", "TEXT", "CLOSECALLVAR", 
			"CB", "CP", "DRAW", "IF", "ID"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public OEFLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "OEFLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\16V\b\1\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\3\2\3\2\5\2\37\n\2\3\2\6\2\"\n\2\r\2\16\2#\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\3\7\6\7\66\n\7"+
		"\r\7\16\7\67\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\6\rS\n\r\r\r\16\rT\2"+
		"\2\16\4\3\6\4\b\5\n\6\f\7\16\b\20\t\22\n\24\13\26\f\30\r\32\16\4\2\3\5"+
		"\4\2\13\13\"\"\5\2**^^}}\4\2C\\c|\2Y\2\4\3\2\2\2\2\6\3\2\2\2\2\b\3\2\2"+
		"\2\2\n\3\2\2\2\2\f\3\2\2\2\2\16\3\2\2\2\3\20\3\2\2\2\3\22\3\2\2\2\3\24"+
		"\3\2\2\2\3\26\3\2\2\2\3\30\3\2\2\2\3\32\3\2\2\2\4!\3\2\2\2\6%\3\2\2\2"+
		"\b,\3\2\2\2\n.\3\2\2\2\f\60\3\2\2\2\16\65\3\2\2\2\209\3\2\2\2\22=\3\2"+
		"\2\2\24A\3\2\2\2\26E\3\2\2\2\30L\3\2\2\2\32R\3\2\2\2\34\"\t\2\2\2\35\37"+
		"\7\17\2\2\36\35\3\2\2\2\36\37\3\2\2\2\37 \3\2\2\2 \"\7\f\2\2!\34\3\2\2"+
		"\2!\36\3\2\2\2\"#\3\2\2\2#!\3\2\2\2#$\3\2\2\2$\5\3\2\2\2%&\7^\2\2&\'\7"+
		"v\2\2\'(\7k\2\2()\7v\2\2)*\7n\2\2*+\7g\2\2+\7\3\2\2\2,-\7}\2\2-\t\3\2"+
		"\2\2./\7*\2\2/\13\3\2\2\2\60\61\7^\2\2\61\62\3\2\2\2\62\63\b\6\2\2\63"+
		"\r\3\2\2\2\64\66\n\3\2\2\65\64\3\2\2\2\66\67\3\2\2\2\67\65\3\2\2\2\67"+
		"8\3\2\2\28\17\3\2\2\29:\7\"\2\2:;\3\2\2\2;<\b\b\3\2<\21\3\2\2\2=>\7\177"+
		"\2\2>?\3\2\2\2?@\b\t\3\2@\23\3\2\2\2AB\7+\2\2BC\3\2\2\2CD\b\n\3\2D\25"+
		"\3\2\2\2EF\7f\2\2FG\7t\2\2GH\7c\2\2HI\7y\2\2IJ\3\2\2\2JK\5\n\5\2K\27\3"+
		"\2\2\2LM\7k\2\2MN\7h\2\2NO\3\2\2\2OP\5\b\4\2P\31\3\2\2\2QS\t\4\2\2RQ\3"+
		"\2\2\2ST\3\2\2\2TR\3\2\2\2TU\3\2\2\2U\33\3\2\2\2\t\2\3\36!#\67T\4\4\3"+
		"\2\4\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}