// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OEFParser}.
 */
public interface OEFParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void enterDocument(OEFParser.DocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#document}.
	 * @param ctx the parse tree
	 */
	void exitDocument(OEFParser.DocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(OEFParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(OEFParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#var}.
	 * @param ctx the parse tree
	 */
	void enterVar(OEFParser.VarContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#var}.
	 * @param ctx the parse tree
	 */
	void exitVar(OEFParser.VarContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#commandDraw}.
	 * @param ctx the parse tree
	 */
	void enterCommandDraw(OEFParser.CommandDrawContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#commandDraw}.
	 * @param ctx the parse tree
	 */
	void exitCommandDraw(OEFParser.CommandDrawContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#commandIf}.
	 * @param ctx the parse tree
	 */
	void enterCommandIf(OEFParser.CommandIfContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#commandIf}.
	 * @param ctx the parse tree
	 */
	void exitCommandIf(OEFParser.CommandIfContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#callreference}.
	 * @param ctx the parse tree
	 */
	void enterCallreference(OEFParser.CallreferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#callreference}.
	 * @param ctx the parse tree
	 */
	void exitCallreference(OEFParser.CallreferenceContext ctx);
}