lexer grammar OEFLexer;

// Default mode rules (the SEA)
SEA_WS      :   (' '|'\t'|'\r'? '\n')+ ;

TITLE : '\\title';
OB    : '{';
OP	  : '(';

BSLASH  : '\\'          		-> mode(CALLREFERENCE) ;      

TEXT  : ~[\\({]+;                         // clump all text together


// ----------------- Everything Callreference ---------------------
mode CALLREFERENCE;


CLOSECALLVAR : ' '   		-> mode(DEFAULT_MODE) ; // back to SEA mode 
CB			 : '}' 			-> mode(DEFAULT_MODE) ; // back to SEA mode 
CP 			 : ')'			-> mode(DEFAULT_MODE) ; // back to SEA mode 

DRAW 	:	'draw' OP;
IF 		:	'if' OB;
ID    	: 	[a-zA-Z]+ ;                   // match/send ID in tag to parser

