import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.FileInputStream;
import java.io.InputStream;

public class Test {
    public static void main(String[] args) throws Exception {
		//Avec fichier en entrée en ligne de commande        
		System.out.println("\n\n\n");
		String inputFile = null; 
        if ( args.length>0 ) inputFile = args[0];
      
        //Pour prendre en compte l'utf8 en std : https://github.com/antlr/antlr4/blob/master/doc/unicode.md
        CharStream input = CharStreams.fromFileName(inputFile);
        
        /*
        InputStream is = System.in;
        if ( inputFile!=null ) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        */

        // create a CharStream that reads from standard input
        // ANTLRInputStream input = new ANTLRInputStream(System.in);

        // create a lexer that feeds off of input CharStream
        OEFLexer lexer = new OEFLexer(input);

        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // create a parser that feeds off the tokens buffer
        OEFParser parser = new OEFParser(tokens);
        
        ParseTree tree = parser.oef(); // begin parsing at init rule
        
        System.out.println(tree.toStringTree(parser)+"\n"); // print LISP-style tree
        
        //EvalVisitor eval = new EvalVisitor();
        //   eval.visit(tree);
        
    }
}
 