// Generated from OEFLexer.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT=1, EntityRef=2, CharRef=3, TITLE=4, SEA_WS=5, CB=6, LANGUAGE=7, 
		TEXT=8, SPECIAL_CLOSE=9, SLASH_CLOSE=10, SLASH=11, EQUALS=12, STRING=13, 
		Name=14, S=15, PI=16;
	public static final int
		INSIDE=1, PROC_INSTR=2;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE", "INSIDE", "PROC_INSTR"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"COMMENT", "EntityRef", "CharRef", "TITLE", "SEA_WS", "CB", "LANGUAGE", 
			"TEXT", "SPECIAL_CLOSE", "SLASH_CLOSE", "SLASH", "EQUALS", "STRING", 
			"Name", "S", "HEXDIGIT", "DIGIT", "NameChar", "NameStartChar", "PI", 
			"IGNORE"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, "'\\title{'", null, "'}'", "'\\language'", null, 
			null, "'/}'", "'/'", "'='"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMMENT", "EntityRef", "CharRef", "TITLE", "SEA_WS", "CB", "LANGUAGE", 
			"TEXT", "SPECIAL_CLOSE", "SLASH_CLOSE", "SLASH", "EQUALS", "STRING", 
			"Name", "S", "PI"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public OEFLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "OEFLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\22\u00bc\b\1\b\1"+
		"\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4"+
		"\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t"+
		"\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\7\2\66\n\2\f\2\16\29\13\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4"+
		"\3\4\3\4\3\4\6\4G\n\4\r\4\16\4H\3\4\3\4\3\4\3\4\3\4\3\4\3\4\6\4R\n\4\r"+
		"\4\16\4S\3\4\3\4\5\4X\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\5\6"+
		"d\n\6\3\6\6\6g\n\6\r\6\16\6h\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\t\6\tx\n\t\r\t\16\ty\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13"+
		"\3\13\3\f\3\f\3\r\3\r\3\16\3\16\7\16\u008c\n\16\f\16\16\16\u008f\13\16"+
		"\3\16\3\16\3\16\7\16\u0094\n\16\f\16\16\16\u0097\13\16\3\16\5\16\u009a"+
		"\n\16\3\17\3\17\7\17\u009e\n\17\f\17\16\17\u00a1\13\17\3\20\3\20\3\20"+
		"\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\23\5\23\u00af\n\23\3\24\5\24"+
		"\u00b2\n\24\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\67\2\27\5\3"+
		"\7\4\t\5\13\6\r\7\17\b\21\t\23\n\25\13\27\f\31\r\33\16\35\17\37\20!\21"+
		"#\2%\2\'\2)\2+\22-\2\5\2\3\4\f\4\2\13\13\"\"\4\2((\177\177\4\2$$}}\4\2"+
		"))}}\5\2\13\f\17\17\"\"\5\2\62;CHch\3\2\62;\4\2/\60aa\5\2\u00b9\u00b9"+
		"\u0302\u0371\u2041\u2042\n\2<<C\\c|\u2072\u2191\u2c02\u2ff1\u3003\ud801"+
		"\uf902\ufdd1\ufdf2\uffff\2\u00c4\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2"+
		"\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\3\25\3"+
		"\2\2\2\3\27\3\2\2\2\3\31\3\2\2\2\3\33\3\2\2\2\3\35\3\2\2\2\3\37\3\2\2"+
		"\2\3!\3\2\2\2\4+\3\2\2\2\4-\3\2\2\2\5/\3\2\2\2\7>\3\2\2\2\tW\3\2\2\2\13"+
		"Y\3\2\2\2\rf\3\2\2\2\17j\3\2\2\2\21l\3\2\2\2\23w\3\2\2\2\25{\3\2\2\2\27"+
		"\u0080\3\2\2\2\31\u0085\3\2\2\2\33\u0087\3\2\2\2\35\u0099\3\2\2\2\37\u009b"+
		"\3\2\2\2!\u00a2\3\2\2\2#\u00a6\3\2\2\2%\u00a8\3\2\2\2\'\u00ae\3\2\2\2"+
		")\u00b1\3\2\2\2+\u00b3\3\2\2\2-\u00b8\3\2\2\2/\60\7>\2\2\60\61\7#\2\2"+
		"\61\62\7/\2\2\62\63\7/\2\2\63\67\3\2\2\2\64\66\13\2\2\2\65\64\3\2\2\2"+
		"\669\3\2\2\2\678\3\2\2\2\67\65\3\2\2\28:\3\2\2\29\67\3\2\2\2:;\7/\2\2"+
		";<\7/\2\2<=\7@\2\2=\6\3\2\2\2>?\7(\2\2?@\5\37\17\2@A\7=\2\2A\b\3\2\2\2"+
		"BC\7(\2\2CD\7%\2\2DF\3\2\2\2EG\5%\22\2FE\3\2\2\2GH\3\2\2\2HF\3\2\2\2H"+
		"I\3\2\2\2IJ\3\2\2\2JK\7=\2\2KX\3\2\2\2LM\7(\2\2MN\7%\2\2NO\7z\2\2OQ\3"+
		"\2\2\2PR\5#\21\2QP\3\2\2\2RS\3\2\2\2SQ\3\2\2\2ST\3\2\2\2TU\3\2\2\2UV\7"+
		"=\2\2VX\3\2\2\2WB\3\2\2\2WL\3\2\2\2X\n\3\2\2\2YZ\7^\2\2Z[\7v\2\2[\\\7"+
		"k\2\2\\]\7v\2\2]^\7n\2\2^_\7g\2\2_`\7}\2\2`\f\3\2\2\2ag\t\2\2\2bd\7\17"+
		"\2\2cb\3\2\2\2cd\3\2\2\2de\3\2\2\2eg\7\f\2\2fa\3\2\2\2fc\3\2\2\2gh\3\2"+
		"\2\2hf\3\2\2\2hi\3\2\2\2i\16\3\2\2\2jk\7\177\2\2k\20\3\2\2\2lm\7^\2\2"+
		"mn\7n\2\2no\7c\2\2op\7p\2\2pq\7i\2\2qr\7w\2\2rs\7c\2\2st\7i\2\2tu\7g\2"+
		"\2u\22\3\2\2\2vx\n\3\2\2wv\3\2\2\2xy\3\2\2\2yw\3\2\2\2yz\3\2\2\2z\24\3"+
		"\2\2\2{|\7A\2\2|}\7\177\2\2}~\3\2\2\2~\177\b\n\2\2\177\26\3\2\2\2\u0080"+
		"\u0081\7\61\2\2\u0081\u0082\7\177\2\2\u0082\u0083\3\2\2\2\u0083\u0084"+
		"\b\13\2\2\u0084\30\3\2\2\2\u0085\u0086\7\61\2\2\u0086\32\3\2\2\2\u0087"+
		"\u0088\7?\2\2\u0088\34\3\2\2\2\u0089\u008d\7$\2\2\u008a\u008c\n\4\2\2"+
		"\u008b\u008a\3\2\2\2\u008c\u008f\3\2\2\2\u008d\u008b\3\2\2\2\u008d\u008e"+
		"\3\2\2\2\u008e\u0090\3\2\2\2\u008f\u008d\3\2\2\2\u0090\u009a\7$\2\2\u0091"+
		"\u0095\7)\2\2\u0092\u0094\n\5\2\2\u0093\u0092\3\2\2\2\u0094\u0097\3\2"+
		"\2\2\u0095\u0093\3\2\2\2\u0095\u0096\3\2\2\2\u0096\u0098\3\2\2\2\u0097"+
		"\u0095\3\2\2\2\u0098\u009a\7)\2\2\u0099\u0089\3\2\2\2\u0099\u0091\3\2"+
		"\2\2\u009a\36\3\2\2\2\u009b\u009f\5)\24\2\u009c\u009e\5\'\23\2\u009d\u009c"+
		"\3\2\2\2\u009e\u00a1\3\2\2\2\u009f\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0"+
		" \3\2\2\2\u00a1\u009f\3\2\2\2\u00a2\u00a3\t\6\2\2\u00a3\u00a4\3\2\2\2"+
		"\u00a4\u00a5\b\20\3\2\u00a5\"\3\2\2\2\u00a6\u00a7\t\7\2\2\u00a7$\3\2\2"+
		"\2\u00a8\u00a9\t\b\2\2\u00a9&\3\2\2\2\u00aa\u00af\5)\24\2\u00ab\u00af"+
		"\t\t\2\2\u00ac\u00af\5%\22\2\u00ad\u00af\t\n\2\2\u00ae\u00aa\3\2\2\2\u00ae"+
		"\u00ab\3\2\2\2\u00ae\u00ac\3\2\2\2\u00ae\u00ad\3\2\2\2\u00af(\3\2\2\2"+
		"\u00b0\u00b2\t\13\2\2\u00b1\u00b0\3\2\2\2\u00b2*\3\2\2\2\u00b3\u00b4\7"+
		"A\2\2\u00b4\u00b5\7\177\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00b7\b\25\2\2\u00b7"+
		",\3\2\2\2\u00b8\u00b9\13\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bb\b\26\4"+
		"\2\u00bb.\3\2\2\2\23\2\3\4\67HSWcfhy\u008d\u0095\u0099\u009f\u00ae\u00b1"+
		"\5\6\2\2\b\2\2\5\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}