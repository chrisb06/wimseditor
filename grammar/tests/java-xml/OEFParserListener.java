// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OEFParser}.
 */
public interface OEFParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link OEFParser#oef}.
	 * @param ctx the parse tree
	 */
	void enterOef(OEFParser.OefContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#oef}.
	 * @param ctx the parse tree
	 */
	void exitOef(OEFParser.OefContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#pre}.
	 * @param ctx the parse tree
	 */
	void enterPre(OEFParser.PreContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#pre}.
	 * @param ctx the parse tree
	 */
	void exitPre(OEFParser.PreContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#title}.
	 * @param ctx the parse tree
	 */
	void enterTitle(OEFParser.TitleContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#title}.
	 * @param ctx the parse tree
	 */
	void exitTitle(OEFParser.TitleContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#reference}.
	 * @param ctx the parse tree
	 */
	void enterReference(OEFParser.ReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#reference}.
	 * @param ctx the parse tree
	 */
	void exitReference(OEFParser.ReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#attribute}.
	 * @param ctx the parse tree
	 */
	void enterAttribute(OEFParser.AttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#attribute}.
	 * @param ctx the parse tree
	 */
	void exitAttribute(OEFParser.AttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#chardata}.
	 * @param ctx the parse tree
	 */
	void enterChardata(OEFParser.ChardataContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#chardata}.
	 * @param ctx the parse tree
	 */
	void exitChardata(OEFParser.ChardataContext ctx);
	/**
	 * Enter a parse tree produced by {@link OEFParser#misc}.
	 * @param ctx the parse tree
	 */
	void enterMisc(OEFParser.MiscContext ctx);
	/**
	 * Exit a parse tree produced by {@link OEFParser#misc}.
	 * @param ctx the parse tree
	 */
	void exitMisc(OEFParser.MiscContext ctx);
}