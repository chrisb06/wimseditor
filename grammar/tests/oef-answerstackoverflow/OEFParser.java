// Generated from OEFParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OEFParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TITLE=1, LANGUAGE=2, AUTHOR=3, EMAIL=4, COMPUTEANSWER=5, PRECISION=6, 
		RANGEDECLARATION=7, INTEGER=8, MATRIX=9, STATEMENT=10, OP=11, S=12, CB=13, 
		CP=14, NUM_INT=15, NUM_REAL=16, COMMA=17, DOTS=18, EQUAL=19, PLUS=20, 
		MINUS=21, STAR=22, SLASH=23, POWER=24, AND=25, OR=26, LESS=27, LESSEQUAL=28, 
		GREATER=29, GREATEREQUAL=30, DBEQUAL=31, NOTEQUAL=32, DRAW=33, RANDINT=34, 
		POINT=35, IF=36, NEWLINE=37, BSLASH=38, ID=39, CR_OTHER=40;
	public static final int
		RULE_document = 0, RULE_pre = 1, RULE_title = 2, RULE_language = 3, RULE_author = 4, 
		RULE_email = 5, RULE_computeanswer = 6, RULE_precision = 7, RULE_rangedeclaration = 8, 
		RULE_integerinstruction = 9, RULE_matrixinstruction = 10, RULE_statement = 11, 
		RULE_expression = 12, RULE_list = 13, RULE_matrix = 14, RULE_callvariable = 15, 
		RULE_relationalExpression = 16, RULE_assignementinteger = 17, RULE_assignementmatrix = 18, 
		RULE_stringInList = 19, RULE_stringInListGroup = 20, RULE_string = 21, 
		RULE_stringGroup = 22, RULE_callcommand = 23, RULE_callcommandflydraw = 24;
	private static String[] makeRuleNames() {
		return new String[] {
			"document", "pre", "title", "language", "author", "email", "computeanswer", 
			"precision", "rangedeclaration", "integerinstruction", "matrixinstruction", 
			"statement", "expression", "list", "matrix", "callvariable", "relationalExpression", 
			"assignementinteger", "assignementmatrix", "stringInList", "stringInListGroup", 
			"string", "stringGroup", "callcommand", "callcommandflydraw"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, "'('", 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"'^'", null, null, null, null, null, null, null, null, null, null, null, 
			null, "'\n'", "'\\'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "TITLE", "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
			"RANGEDECLARATION", "INTEGER", "MATRIX", "STATEMENT", "OP", "S", "CB", 
			"CP", "NUM_INT", "NUM_REAL", "COMMA", "DOTS", "EQUAL", "PLUS", "MINUS", 
			"STAR", "SLASH", "POWER", "AND", "OR", "LESS", "LESSEQUAL", "GREATER", 
			"GREATEREQUAL", "DBEQUAL", "NOTEQUAL", "DRAW", "RANDINT", "POINT", "IF", 
			"NEWLINE", "BSLASH", "ID", "CR_OTHER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "OEFParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public OEFParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class DocumentContext extends ParserRuleContext {
		public PreContext pre() {
			return getRuleContext(PreContext.class,0);
		}
		public TerminalNode EOF() { return getToken(OEFParser.EOF, 0); }
		public DocumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_document; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterDocument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitDocument(this);
		}
	}

	public final DocumentContext document() throws RecognitionException {
		DocumentContext _localctx = new DocumentContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_document);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			pre();
			setState(51);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreContext extends ParserRuleContext {
		public TitleContext title() {
			return getRuleContext(TitleContext.class,0);
		}
		public LanguageContext language() {
			return getRuleContext(LanguageContext.class,0);
		}
		public AuthorContext author() {
			return getRuleContext(AuthorContext.class,0);
		}
		public EmailContext email() {
			return getRuleContext(EmailContext.class,0);
		}
		public ComputeanswerContext computeanswer() {
			return getRuleContext(ComputeanswerContext.class,0);
		}
		public PrecisionContext precision() {
			return getRuleContext(PrecisionContext.class,0);
		}
		public List<RangedeclarationContext> rangedeclaration() {
			return getRuleContexts(RangedeclarationContext.class);
		}
		public RangedeclarationContext rangedeclaration(int i) {
			return getRuleContext(RangedeclarationContext.class,i);
		}
		public List<IntegerinstructionContext> integerinstruction() {
			return getRuleContexts(IntegerinstructionContext.class);
		}
		public IntegerinstructionContext integerinstruction(int i) {
			return getRuleContext(IntegerinstructionContext.class,i);
		}
		public List<MatrixinstructionContext> matrixinstruction() {
			return getRuleContexts(MatrixinstructionContext.class);
		}
		public MatrixinstructionContext matrixinstruction(int i) {
			return getRuleContext(MatrixinstructionContext.class,i);
		}
		public PreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pre; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPre(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPre(this);
		}
	}

	public final PreContext pre() throws RecognitionException {
		PreContext _localctx = new PreContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_pre);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			title();
			setState(54);
			language();
			setState(55);
			author();
			setState(56);
			email();
			setState(57);
			computeanswer();
			setState(58);
			precision();
			setState(62);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==RANGEDECLARATION) {
				{
				{
				setState(59);
				rangedeclaration();
				}
				}
				setState(64);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(69);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INTEGER || _la==MATRIX) {
				{
				setState(67);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case INTEGER:
					{
					setState(65);
					integerinstruction();
					}
					break;
				case MATRIX:
					{
					setState(66);
					matrixinstruction();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(71);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TitleContext extends ParserRuleContext {
		public TerminalNode TITLE() { return getToken(OEFParser.TITLE, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringGroupContext> stringGroup() {
			return getRuleContexts(StringGroupContext.class);
		}
		public StringGroupContext stringGroup(int i) {
			return getRuleContext(StringGroupContext.class,i);
		}
		public List<CallvariableContext> callvariable() {
			return getRuleContexts(CallvariableContext.class);
		}
		public CallvariableContext callvariable(int i) {
			return getRuleContext(CallvariableContext.class,i);
		}
		public TitleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_title; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterTitle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitTitle(this);
		}
	}

	public final TitleContext title() throws RecognitionException {
		TitleContext _localctx = new TitleContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_title);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			match(TITLE);
			setState(77);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(75);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ID:
				case CR_OTHER:
					{
					setState(73);
					stringGroup();
					}
					break;
				case BSLASH:
					{
					setState(74);
					callvariable();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(79);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(80);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LanguageContext extends ParserRuleContext {
		public TerminalNode LANGUAGE() { return getToken(OEFParser.LANGUAGE, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public LanguageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_language; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterLanguage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitLanguage(this);
		}
	}

	public final LanguageContext language() throws RecognitionException {
		LanguageContext _localctx = new LanguageContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_language);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(82);
			match(LANGUAGE);
			setState(83);
			string();
			setState(84);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AuthorContext extends ParserRuleContext {
		public TerminalNode AUTHOR() { return getToken(OEFParser.AUTHOR, 0); }
		public StringGroupContext stringGroup() {
			return getRuleContext(StringGroupContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public AuthorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_author; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAuthor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAuthor(this);
		}
	}

	public final AuthorContext author() throws RecognitionException {
		AuthorContext _localctx = new AuthorContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_author);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			match(AUTHOR);
			setState(87);
			stringGroup();
			setState(88);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EmailContext extends ParserRuleContext {
		public TerminalNode EMAIL() { return getToken(OEFParser.EMAIL, 0); }
		public StringGroupContext stringGroup() {
			return getRuleContext(StringGroupContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public EmailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_email; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterEmail(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitEmail(this);
		}
	}

	public final EmailContext email() throws RecognitionException {
		EmailContext _localctx = new EmailContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_email);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			match(EMAIL);
			setState(91);
			stringGroup();
			setState(92);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComputeanswerContext extends ParserRuleContext {
		public TerminalNode COMPUTEANSWER() { return getToken(OEFParser.COMPUTEANSWER, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public ComputeanswerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_computeanswer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterComputeanswer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitComputeanswer(this);
		}
	}

	public final ComputeanswerContext computeanswer() throws RecognitionException {
		ComputeanswerContext _localctx = new ComputeanswerContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_computeanswer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94);
			match(COMPUTEANSWER);
			setState(95);
			string();
			setState(96);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrecisionContext extends ParserRuleContext {
		public TerminalNode PRECISION() { return getToken(OEFParser.PRECISION, 0); }
		public TerminalNode NUM_INT() { return getToken(OEFParser.NUM_INT, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public PrecisionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precision; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPrecision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPrecision(this);
		}
	}

	public final PrecisionContext precision() throws RecognitionException {
		PrecisionContext _localctx = new PrecisionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_precision);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			match(PRECISION);
			setState(99);
			match(NUM_INT);
			setState(100);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangedeclarationContext extends ParserRuleContext {
		public TerminalNode RANGEDECLARATION() { return getToken(OEFParser.RANGEDECLARATION, 0); }
		public StringGroupContext stringGroup() {
			return getRuleContext(StringGroupContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public RangedeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rangedeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterRangedeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitRangedeclaration(this);
		}
	}

	public final RangedeclarationContext rangedeclaration() throws RecognitionException {
		RangedeclarationContext _localctx = new RangedeclarationContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rangedeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			match(RANGEDECLARATION);
			setState(103);
			stringGroup();
			setState(104);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerinstructionContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(OEFParser.INTEGER, 0); }
		public AssignementintegerContext assignementinteger() {
			return getRuleContext(AssignementintegerContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public IntegerinstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerinstruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterIntegerinstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitIntegerinstruction(this);
		}
	}

	public final IntegerinstructionContext integerinstruction() throws RecognitionException {
		IntegerinstructionContext _localctx = new IntegerinstructionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_integerinstruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			match(INTEGER);
			setState(107);
			assignementinteger();
			setState(108);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixinstructionContext extends ParserRuleContext {
		public TerminalNode MATRIX() { return getToken(OEFParser.MATRIX, 0); }
		public AssignementmatrixContext assignementmatrix() {
			return getRuleContext(AssignementmatrixContext.class,0);
		}
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public MatrixinstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixinstruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterMatrixinstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitMatrixinstruction(this);
		}
	}

	public final MatrixinstructionContext matrixinstruction() throws RecognitionException {
		MatrixinstructionContext _localctx = new MatrixinstructionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_matrixinstruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			match(MATRIX);
			setState(111);
			assignementmatrix();
			setState(112);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public TerminalNode STATEMENT() { return getToken(OEFParser.STATEMENT, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public List<StringGroupContext> stringGroup() {
			return getRuleContexts(StringGroupContext.class);
		}
		public StringGroupContext stringGroup(int i) {
			return getRuleContext(StringGroupContext.class,i);
		}
		public List<CallvariableContext> callvariable() {
			return getRuleContexts(CallvariableContext.class);
		}
		public CallvariableContext callvariable(int i) {
			return getRuleContext(CallvariableContext.class,i);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(STATEMENT);
			setState(119);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BSLASH) | (1L << ID) | (1L << CR_OTHER))) != 0)) {
				{
				setState(117);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ID:
				case CR_OTHER:
					{
					setState(115);
					stringGroup();
					}
					break;
				case BSLASH:
					{
					setState(116);
					callvariable();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(121);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(122);
			match(CB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NumIntContext extends ExpressionContext {
		public TerminalNode NUM_INT() { return getToken(OEFParser.NUM_INT, 0); }
		public NumIntContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterNumInt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitNumInt(this);
		}
	}
	public static class ParensContext extends ExpressionContext {
		public TerminalNode OP() { return getToken(OEFParser.OP, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public ParensContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterParens(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitParens(this);
		}
	}
	public static class KeyRandintContext extends ExpressionContext {
		public CallcommandContext callcommand() {
			return getRuleContext(CallcommandContext.class,0);
		}
		public KeyRandintContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterKeyRandint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitKeyRandint(this);
		}
	}
	public static class AddSubContext extends ExpressionContext {
		public Token op;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(OEFParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(OEFParser.MINUS, 0); }
		public AddSubContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAddSub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAddSub(this);
		}
	}
	public static class IdContext extends ExpressionContext {
		public CallvariableContext callvariable() {
			return getRuleContext(CallvariableContext.class,0);
		}
		public IdContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitId(this);
		}
	}
	public static class PowerContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode POWER() { return getToken(OEFParser.POWER, 0); }
		public PowerContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPower(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPower(this);
		}
	}
	public static class PluMinContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(OEFParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(OEFParser.MINUS, 0); }
		public PluMinContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterPluMin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitPluMin(this);
		}
	}
	public static class MulDivContext extends ExpressionContext {
		public Token op;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode STAR() { return getToken(OEFParser.STAR, 0); }
		public TerminalNode SLASH() { return getToken(OEFParser.SLASH, 0); }
		public MulDivContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterMulDiv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitMulDiv(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(134);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
			case MINUS:
				{
				_localctx = new PluMinContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(125);
				_la = _input.LA(1);
				if ( !(_la==PLUS || _la==MINUS) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(126);
				expression(5);
				}
				break;
			case OP:
				{
				_localctx = new ParensContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(127);
				match(OP);
				setState(128);
				expression(0);
				setState(129);
				match(CP);
				}
				break;
			case DRAW:
			case RANDINT:
				{
				_localctx = new KeyRandintContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(131);
				callcommand();
				}
				break;
			case BSLASH:
				{
				_localctx = new IdContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(132);
				callvariable();
				}
				break;
			case NUM_INT:
				{
				_localctx = new NumIntContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(133);
				match(NUM_INT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(147);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(145);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
					case 1:
						{
						_localctx = new AddSubContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(136);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(137);
						((AddSubContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
							((AddSubContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(138);
						expression(9);
						}
						break;
					case 2:
						{
						_localctx = new MulDivContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(139);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(140);
						((MulDivContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==STAR || _la==SLASH) ) {
							((MulDivContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(141);
						expression(8);
						}
						break;
					case 3:
						{
						_localctx = new PowerContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(142);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(143);
						match(POWER);
						setState(144);
						expression(7);
						}
						break;
					}
					} 
				}
				setState(149);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ListContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<StringInListGroupContext> stringInListGroup() {
			return getRuleContexts(StringInListGroupContext.class);
		}
		public StringInListGroupContext stringInListGroup(int i) {
			return getRuleContext(StringInListGroupContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(OEFParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(OEFParser.COMMA, i);
		}
		public ListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitList(this);
		}
	}

	public final ListContext list() throws RecognitionException {
		ListContext _localctx = new ListContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OP:
			case NUM_INT:
			case PLUS:
			case MINUS:
			case DRAW:
			case RANDINT:
			case BSLASH:
				{
				setState(150);
				expression(0);
				}
				break;
			case CR_OTHER:
				{
				setState(151);
				stringInListGroup();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(161);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(154);
				match(COMMA);
				setState(157);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case OP:
				case NUM_INT:
				case PLUS:
				case MINUS:
				case DRAW:
				case RANDINT:
				case BSLASH:
					{
					setState(155);
					expression(0);
					}
					break;
				case CR_OTHER:
					{
					setState(156);
					stringInListGroup();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				setState(163);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixContext extends ParserRuleContext {
		public MatrixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrix; }
	 
		public MatrixContext() { }
		public void copyFrom(MatrixContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExpMatrixContext extends MatrixContext {
		public List<ListContext> list() {
			return getRuleContexts(ListContext.class);
		}
		public ListContext list(int i) {
			return getRuleContext(ListContext.class,i);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(OEFParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(OEFParser.NEWLINE, i);
		}
		public ExpMatrixContext(MatrixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterExpMatrix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitExpMatrix(this);
		}
	}

	public final MatrixContext matrix() throws RecognitionException {
		MatrixContext _localctx = new MatrixContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_matrix);
		int _la;
		try {
			_localctx = new ExpMatrixContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			list();
			setState(167); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(165);
				match(NEWLINE);
				setState(166);
				list();
				}
				}
				setState(169); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==NEWLINE );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallvariableContext extends ParserRuleContext {
		public TerminalNode BSLASH() { return getToken(OEFParser.BSLASH, 0); }
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public CallvariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callvariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallvariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallvariable(this);
		}
	}

	public final CallvariableContext callvariable() throws RecognitionException {
		CallvariableContext _localctx = new CallvariableContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_callvariable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171);
			match(BSLASH);
			setState(172);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
	 
		public RelationalExpressionContext() { }
		public void copyFrom(RelationalExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EqualNotEqualContext extends RelationalExpressionContext {
		public Token rel;
		public List<RelationalExpressionContext> relationalExpression() {
			return getRuleContexts(RelationalExpressionContext.class);
		}
		public RelationalExpressionContext relationalExpression(int i) {
			return getRuleContext(RelationalExpressionContext.class,i);
		}
		public TerminalNode DBEQUAL() { return getToken(OEFParser.DBEQUAL, 0); }
		public TerminalNode NOTEQUAL() { return getToken(OEFParser.NOTEQUAL, 0); }
		public EqualNotEqualContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterEqualNotEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitEqualNotEqual(this);
		}
	}
	public static class OrAndContext extends RelationalExpressionContext {
		public Token rel;
		public List<RelationalExpressionContext> relationalExpression() {
			return getRuleContexts(RelationalExpressionContext.class);
		}
		public RelationalExpressionContext relationalExpression(int i) {
			return getRuleContext(RelationalExpressionContext.class,i);
		}
		public TerminalNode OR() { return getToken(OEFParser.OR, 0); }
		public TerminalNode AND() { return getToken(OEFParser.AND, 0); }
		public OrAndContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterOrAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitOrAnd(this);
		}
	}
	public static class ParensREContext extends RelationalExpressionContext {
		public TerminalNode OP() { return getToken(OEFParser.OP, 0); }
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public ParensREContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterParensRE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitParensRE(this);
		}
	}
	public static class LessGreaterContext extends RelationalExpressionContext {
		public Token rel;
		public List<RelationalExpressionContext> relationalExpression() {
			return getRuleContexts(RelationalExpressionContext.class);
		}
		public RelationalExpressionContext relationalExpression(int i) {
			return getRuleContext(RelationalExpressionContext.class,i);
		}
		public TerminalNode LESS() { return getToken(OEFParser.LESS, 0); }
		public TerminalNode LESSEQUAL() { return getToken(OEFParser.LESSEQUAL, 0); }
		public TerminalNode GREATER() { return getToken(OEFParser.GREATER, 0); }
		public TerminalNode GREATEREQUAL() { return getToken(OEFParser.GREATEREQUAL, 0); }
		public LessGreaterContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterLessGreater(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitLessGreater(this);
		}
	}
	public static class RelExpContext extends RelationalExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RelExpContext(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterRelExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitRelExp(this);
		}
	}

	public final RelationalExpressionContext relationalExpression() throws RecognitionException {
		return relationalExpression(0);
	}

	private RelationalExpressionContext relationalExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, _parentState);
		RelationalExpressionContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_relationalExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				_localctx = new ParensREContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(175);
				match(OP);
				setState(176);
				relationalExpression(0);
				setState(177);
				match(CP);
				}
				break;
			case 2:
				{
				_localctx = new RelExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(179);
				expression(0);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(193);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(191);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
					case 1:
						{
						_localctx = new OrAndContext(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(182);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(183);
						((OrAndContext)_localctx).rel = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
							((OrAndContext)_localctx).rel = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(184);
						relationalExpression(6);
						}
						break;
					case 2:
						{
						_localctx = new LessGreaterContext(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(185);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(186);
						((LessGreaterContext)_localctx).rel = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LESS) | (1L << LESSEQUAL) | (1L << GREATER) | (1L << GREATEREQUAL))) != 0)) ) {
							((LessGreaterContext)_localctx).rel = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(187);
						relationalExpression(5);
						}
						break;
					case 3:
						{
						_localctx = new EqualNotEqualContext(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(188);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(189);
						((EqualNotEqualContext)_localctx).rel = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==DBEQUAL || _la==NOTEQUAL) ) {
							((EqualNotEqualContext)_localctx).rel = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(190);
						relationalExpression(4);
						}
						break;
					}
					} 
				}
				setState(195);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AssignementintegerContext extends ParserRuleContext {
		public AssignementintegerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignementinteger; }
	 
		public AssignementintegerContext() { }
		public void copyFrom(AssignementintegerContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssignIntegerContext extends AssignementintegerContext {
		public Token ID;
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(OEFParser.EQUAL, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignIntegerContext(AssignementintegerContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAssignInteger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAssignInteger(this);
		}
	}

	public final AssignementintegerContext assignementinteger() throws RecognitionException {
		AssignementintegerContext _localctx = new AssignementintegerContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_assignementinteger);
		try {
			_localctx = new AssignIntegerContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			((AssignIntegerContext)_localctx).ID = match(ID);
			setState(197);
			match(EQUAL);
			setState(198);
			expression(0);
			System.out.println("asignement:"+(((AssignIntegerContext)_localctx).ID!=null?((AssignIntegerContext)_localctx).ID.getText():null));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignementmatrixContext extends ParserRuleContext {
		public AssignementmatrixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignementmatrix; }
	 
		public AssignementmatrixContext() { }
		public void copyFrom(AssignementmatrixContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssignMatrixContext extends AssignementmatrixContext {
		public Token ID;
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(OEFParser.EQUAL, 0); }
		public MatrixContext matrix() {
			return getRuleContext(MatrixContext.class,0);
		}
		public AssignMatrixContext(AssignementmatrixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterAssignMatrix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitAssignMatrix(this);
		}
	}

	public final AssignementmatrixContext assignementmatrix() throws RecognitionException {
		AssignementmatrixContext _localctx = new AssignementmatrixContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_assignementmatrix);
		try {
			_localctx = new AssignMatrixContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(201);
			((AssignMatrixContext)_localctx).ID = match(ID);
			setState(202);
			match(EQUAL);
			setState(203);
			matrix();
			System.out.println("asignement:"+(((AssignMatrixContext)_localctx).ID!=null?((AssignMatrixContext)_localctx).ID.getText():null));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringInListContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(OEFParser.COMMA, 0); }
		public TerminalNode CB() { return getToken(OEFParser.CB, 0); }
		public TerminalNode NEWLINE() { return getToken(OEFParser.NEWLINE, 0); }
		public List<TerminalNode> CR_OTHER() { return getTokens(OEFParser.CR_OTHER); }
		public TerminalNode CR_OTHER(int i) {
			return getToken(OEFParser.CR_OTHER, i);
		}
		public StringInListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringInList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterStringInList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitStringInList(this);
		}
	}

	public final StringInListContext stringInList() throws RecognitionException {
		StringInListContext _localctx = new StringInListContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_stringInList);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(207); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(206);
					match(CR_OTHER);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(209); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(211);
			_la = _input.LA(1);
			if ( _la <= 0 || ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CB) | (1L << COMMA) | (1L << NEWLINE))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringInListGroupContext extends ParserRuleContext {
		public List<StringInListContext> stringInList() {
			return getRuleContexts(StringInListContext.class);
		}
		public StringInListContext stringInList(int i) {
			return getRuleContext(StringInListContext.class,i);
		}
		public StringInListGroupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringInListGroup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterStringInListGroup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitStringInListGroup(this);
		}
	}

	public final StringInListGroupContext stringInListGroup() throws RecognitionException {
		StringInListGroupContext _localctx = new StringInListGroupContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_stringInListGroup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(213);
				stringInList();
				}
				}
				setState(216); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CR_OTHER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public List<TerminalNode> CR_OTHER() { return getTokens(OEFParser.CR_OTHER); }
		public TerminalNode CR_OTHER(int i) {
			return getToken(OEFParser.CR_OTHER, i);
		}
		public TerminalNode ID() { return getToken(OEFParser.ID, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_string);
		try {
			int _alt;
			setState(224);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CR_OTHER:
				enterOuterAlt(_localctx, 1);
				{
				setState(219); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(218);
						match(CR_OTHER);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(221); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(223);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringGroupContext extends ParserRuleContext {
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public StringGroupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringGroup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterStringGroup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitStringGroup(this);
		}
	}

	public final StringGroupContext stringGroup() throws RecognitionException {
		StringGroupContext _localctx = new StringGroupContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_stringGroup);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(227); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(226);
					string();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(229); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallcommandContext extends ParserRuleContext {
		public TerminalNode DRAW() { return getToken(OEFParser.DRAW, 0); }
		public TerminalNode COMMA() { return getToken(OEFParser.COMMA, 0); }
		public TerminalNode CP() { return getToken(OEFParser.CP, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<StringInListGroupContext> stringInListGroup() {
			return getRuleContexts(StringInListGroupContext.class);
		}
		public StringInListGroupContext stringInListGroup(int i) {
			return getRuleContext(StringInListGroupContext.class,i);
		}
		public List<CallcommandflydrawContext> callcommandflydraw() {
			return getRuleContexts(CallcommandflydrawContext.class);
		}
		public CallcommandflydrawContext callcommandflydraw(int i) {
			return getRuleContext(CallcommandflydrawContext.class,i);
		}
		public TerminalNode RANDINT() { return getToken(OEFParser.RANDINT, 0); }
		public TerminalNode DOTS() { return getToken(OEFParser.DOTS, 0); }
		public CallcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallcommand(this);
		}
	}

	public final CallcommandContext callcommand() throws RecognitionException {
		CallcommandContext _localctx = new CallcommandContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_callcommand);
		int _la;
		try {
			setState(255);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DRAW:
				enterOuterAlt(_localctx, 1);
				{
				setState(231);
				match(DRAW);
				setState(234);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case OP:
				case NUM_INT:
				case PLUS:
				case MINUS:
				case DRAW:
				case RANDINT:
				case BSLASH:
					{
					setState(232);
					expression(0);
					}
					break;
				case CR_OTHER:
					{
					setState(233);
					stringInListGroup();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(236);
				match(COMMA);
				setState(239);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case OP:
				case NUM_INT:
				case PLUS:
				case MINUS:
				case DRAW:
				case RANDINT:
				case BSLASH:
					{
					setState(237);
					expression(0);
					}
					break;
				case CR_OTHER:
					{
					setState(238);
					stringInListGroup();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(244);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==POINT) {
					{
					{
					setState(241);
					callcommandflydraw();
					}
					}
					setState(246);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(247);
				match(CP);
				}
				break;
			case RANDINT:
				enterOuterAlt(_localctx, 2);
				{
				setState(249);
				match(RANDINT);
				setState(250);
				expression(0);
				setState(251);
				match(DOTS);
				setState(252);
				expression(0);
				setState(253);
				match(CP);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallcommandflydrawContext extends ParserRuleContext {
		public TerminalNode POINT() { return getToken(OEFParser.POINT, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode COMMA() { return getToken(OEFParser.COMMA, 0); }
		public CallcommandflydrawContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callcommandflydraw; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).enterCallcommandflydraw(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OEFParserListener ) ((OEFParserListener)listener).exitCallcommandflydraw(this);
		}
	}

	public final CallcommandflydrawContext callcommandflydraw() throws RecognitionException {
		CallcommandflydrawContext _localctx = new CallcommandflydrawContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_callcommandflydraw);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(257);
			match(POINT);
			setState(258);
			expression(0);
			setState(259);
			match(COMMA);
			setState(260);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 12:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		case 16:
			return relationalExpression_sempred((RelationalExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		}
		return true;
	}
	private boolean relationalExpression_sempred(RelationalExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 4);
		case 5:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3*\u0109\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3?\n\3\f\3\16\3B"+
		"\13\3\3\3\3\3\7\3F\n\3\f\3\16\3I\13\3\3\4\3\4\3\4\7\4N\n\4\f\4\16\4Q\13"+
		"\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b"+
		"\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3"+
		"\f\3\r\3\r\3\r\7\rx\n\r\f\r\16\r{\13\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\5\16\u0089\n\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\7\16\u0094\n\16\f\16\16\16\u0097\13\16\3\17\3\17\5\17"+
		"\u009b\n\17\3\17\3\17\3\17\5\17\u00a0\n\17\7\17\u00a2\n\17\f\17\16\17"+
		"\u00a5\13\17\3\20\3\20\3\20\6\20\u00aa\n\20\r\20\16\20\u00ab\3\21\3\21"+
		"\3\21\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u00b7\n\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\7\22\u00c2\n\22\f\22\16\22\u00c5\13\22\3\23"+
		"\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\25\6\25\u00d2\n\25\r\25"+
		"\16\25\u00d3\3\25\3\25\3\26\6\26\u00d9\n\26\r\26\16\26\u00da\3\27\6\27"+
		"\u00de\n\27\r\27\16\27\u00df\3\27\5\27\u00e3\n\27\3\30\6\30\u00e6\n\30"+
		"\r\30\16\30\u00e7\3\31\3\31\3\31\5\31\u00ed\n\31\3\31\3\31\3\31\5\31\u00f2"+
		"\n\31\3\31\7\31\u00f5\n\31\f\31\16\31\u00f8\13\31\3\31\3\31\3\31\3\31"+
		"\3\31\3\31\3\31\3\31\5\31\u0102\n\31\3\32\3\32\3\32\3\32\3\32\3\32\2\4"+
		"\32\"\33\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\2\b\3\2"+
		"\26\27\3\2\30\31\3\2\33\34\3\2\35 \3\2!\"\5\2\17\17\23\23\'\'\2\u010e"+
		"\2\64\3\2\2\2\4\67\3\2\2\2\6J\3\2\2\2\bT\3\2\2\2\nX\3\2\2\2\f\\\3\2\2"+
		"\2\16`\3\2\2\2\20d\3\2\2\2\22h\3\2\2\2\24l\3\2\2\2\26p\3\2\2\2\30t\3\2"+
		"\2\2\32\u0088\3\2\2\2\34\u009a\3\2\2\2\36\u00a6\3\2\2\2 \u00ad\3\2\2\2"+
		"\"\u00b6\3\2\2\2$\u00c6\3\2\2\2&\u00cb\3\2\2\2(\u00d1\3\2\2\2*\u00d8\3"+
		"\2\2\2,\u00e2\3\2\2\2.\u00e5\3\2\2\2\60\u0101\3\2\2\2\62\u0103\3\2\2\2"+
		"\64\65\5\4\3\2\65\66\7\2\2\3\66\3\3\2\2\2\678\5\6\4\289\5\b\5\29:\5\n"+
		"\6\2:;\5\f\7\2;<\5\16\b\2<@\5\20\t\2=?\5\22\n\2>=\3\2\2\2?B\3\2\2\2@>"+
		"\3\2\2\2@A\3\2\2\2AG\3\2\2\2B@\3\2\2\2CF\5\24\13\2DF\5\26\f\2EC\3\2\2"+
		"\2ED\3\2\2\2FI\3\2\2\2GE\3\2\2\2GH\3\2\2\2H\5\3\2\2\2IG\3\2\2\2JO\7\3"+
		"\2\2KN\5.\30\2LN\5 \21\2MK\3\2\2\2ML\3\2\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2"+
		"\2\2PR\3\2\2\2QO\3\2\2\2RS\7\17\2\2S\7\3\2\2\2TU\7\4\2\2UV\5,\27\2VW\7"+
		"\17\2\2W\t\3\2\2\2XY\7\5\2\2YZ\5.\30\2Z[\7\17\2\2[\13\3\2\2\2\\]\7\6\2"+
		"\2]^\5.\30\2^_\7\17\2\2_\r\3\2\2\2`a\7\7\2\2ab\5,\27\2bc\7\17\2\2c\17"+
		"\3\2\2\2de\7\b\2\2ef\7\21\2\2fg\7\17\2\2g\21\3\2\2\2hi\7\t\2\2ij\5.\30"+
		"\2jk\7\17\2\2k\23\3\2\2\2lm\7\n\2\2mn\5$\23\2no\7\17\2\2o\25\3\2\2\2p"+
		"q\7\13\2\2qr\5&\24\2rs\7\17\2\2s\27\3\2\2\2ty\7\f\2\2ux\5.\30\2vx\5 \21"+
		"\2wu\3\2\2\2wv\3\2\2\2x{\3\2\2\2yw\3\2\2\2yz\3\2\2\2z|\3\2\2\2{y\3\2\2"+
		"\2|}\7\17\2\2}\31\3\2\2\2~\177\b\16\1\2\177\u0080\t\2\2\2\u0080\u0089"+
		"\5\32\16\7\u0081\u0082\7\r\2\2\u0082\u0083\5\32\16\2\u0083\u0084\7\20"+
		"\2\2\u0084\u0089\3\2\2\2\u0085\u0089\5\60\31\2\u0086\u0089\5 \21\2\u0087"+
		"\u0089\7\21\2\2\u0088~\3\2\2\2\u0088\u0081\3\2\2\2\u0088\u0085\3\2\2\2"+
		"\u0088\u0086\3\2\2\2\u0088\u0087\3\2\2\2\u0089\u0095\3\2\2\2\u008a\u008b"+
		"\f\n\2\2\u008b\u008c\t\2\2\2\u008c\u0094\5\32\16\13\u008d\u008e\f\t\2"+
		"\2\u008e\u008f\t\3\2\2\u008f\u0094\5\32\16\n\u0090\u0091\f\b\2\2\u0091"+
		"\u0092\7\32\2\2\u0092\u0094\5\32\16\t\u0093\u008a\3\2\2\2\u0093\u008d"+
		"\3\2\2\2\u0093\u0090\3\2\2\2\u0094\u0097\3\2\2\2\u0095\u0093\3\2\2\2\u0095"+
		"\u0096\3\2\2\2\u0096\33\3\2\2\2\u0097\u0095\3\2\2\2\u0098\u009b\5\32\16"+
		"\2\u0099\u009b\5*\26\2\u009a\u0098\3\2\2\2\u009a\u0099\3\2\2\2\u009b\u00a3"+
		"\3\2\2\2\u009c\u009f\7\23\2\2\u009d\u00a0\5\32\16\2\u009e\u00a0\5*\26"+
		"\2\u009f\u009d\3\2\2\2\u009f\u009e\3\2\2\2\u00a0\u00a2\3\2\2\2\u00a1\u009c"+
		"\3\2\2\2\u00a2\u00a5\3\2\2\2\u00a3\u00a1\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4"+
		"\35\3\2\2\2\u00a5\u00a3\3\2\2\2\u00a6\u00a9\5\34\17\2\u00a7\u00a8\7\'"+
		"\2\2\u00a8\u00aa\5\34\17\2\u00a9\u00a7\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab"+
		"\u00a9\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\37\3\2\2\2\u00ad\u00ae\7(\2\2"+
		"\u00ae\u00af\7)\2\2\u00af!\3\2\2\2\u00b0\u00b1\b\22\1\2\u00b1\u00b2\7"+
		"\r\2\2\u00b2\u00b3\5\"\22\2\u00b3\u00b4\7\20\2\2\u00b4\u00b7\3\2\2\2\u00b5"+
		"\u00b7\5\32\16\2\u00b6\u00b0\3\2\2\2\u00b6\u00b5\3\2\2\2\u00b7\u00c3\3"+
		"\2\2\2\u00b8\u00b9\f\7\2\2\u00b9\u00ba\t\4\2\2\u00ba\u00c2\5\"\22\b\u00bb"+
		"\u00bc\f\6\2\2\u00bc\u00bd\t\5\2\2\u00bd\u00c2\5\"\22\7\u00be\u00bf\f"+
		"\5\2\2\u00bf\u00c0\t\6\2\2\u00c0\u00c2\5\"\22\6\u00c1\u00b8\3\2\2\2\u00c1"+
		"\u00bb\3\2\2\2\u00c1\u00be\3\2\2\2\u00c2\u00c5\3\2\2\2\u00c3\u00c1\3\2"+
		"\2\2\u00c3\u00c4\3\2\2\2\u00c4#\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6\u00c7"+
		"\7)\2\2\u00c7\u00c8\7\25\2\2\u00c8\u00c9\5\32\16\2\u00c9\u00ca\b\23\1"+
		"\2\u00ca%\3\2\2\2\u00cb\u00cc\7)\2\2\u00cc\u00cd\7\25\2\2\u00cd\u00ce"+
		"\5\36\20\2\u00ce\u00cf\b\24\1\2\u00cf\'\3\2\2\2\u00d0\u00d2\7*\2\2\u00d1"+
		"\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3\u00d4\3\2"+
		"\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d6\n\7\2\2\u00d6)\3\2\2\2\u00d7\u00d9"+
		"\5(\25\2\u00d8\u00d7\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00d8\3\2\2\2\u00da"+
		"\u00db\3\2\2\2\u00db+\3\2\2\2\u00dc\u00de\7*\2\2\u00dd\u00dc\3\2\2\2\u00de"+
		"\u00df\3\2\2\2\u00df\u00dd\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0\u00e3\3\2"+
		"\2\2\u00e1\u00e3\7)\2\2\u00e2\u00dd\3\2\2\2\u00e2\u00e1\3\2\2\2\u00e3"+
		"-\3\2\2\2\u00e4\u00e6\5,\27\2\u00e5\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2"+
		"\u00e7\u00e5\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8/\3\2\2\2\u00e9\u00ec\7"+
		"#\2\2\u00ea\u00ed\5\32\16\2\u00eb\u00ed\5*\26\2\u00ec\u00ea\3\2\2\2\u00ec"+
		"\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00f1\7\23\2\2\u00ef\u00f2\5"+
		"\32\16\2\u00f0\u00f2\5*\26\2\u00f1\u00ef\3\2\2\2\u00f1\u00f0\3\2\2\2\u00f2"+
		"\u00f6\3\2\2\2\u00f3\u00f5\5\62\32\2\u00f4\u00f3\3\2\2\2\u00f5\u00f8\3"+
		"\2\2\2\u00f6\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f9\3\2\2\2\u00f8"+
		"\u00f6\3\2\2\2\u00f9\u00fa\7\20\2\2\u00fa\u0102\3\2\2\2\u00fb\u00fc\7"+
		"$\2\2\u00fc\u00fd\5\32\16\2\u00fd\u00fe\7\24\2\2\u00fe\u00ff\5\32\16\2"+
		"\u00ff\u0100\7\20\2\2\u0100\u0102\3\2\2\2\u0101\u00e9\3\2\2\2\u0101\u00fb"+
		"\3\2\2\2\u0102\61\3\2\2\2\u0103\u0104\7%\2\2\u0104\u0105\5\32\16\2\u0105"+
		"\u0106\7\23\2\2\u0106\u0107\5\32\16\2\u0107\63\3\2\2\2\34@EGMOwy\u0088"+
		"\u0093\u0095\u009a\u009f\u00a3\u00ab\u00b6\u00c1\u00c3\u00d3\u00da\u00df"+
		"\u00e2\u00e7\u00ec\u00f1\u00f6\u0101";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}