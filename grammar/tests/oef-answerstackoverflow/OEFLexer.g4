lexer grammar OEFLexer;

TITLE   			 : '\\title' S? OB 				-> pushMode(CALLINSTRUCTION);
LANGUAGE  			 : '\\language' S? OB 			-> pushMode(CALLINSTRUCTION);
AUTHOR  			 : '\\author' S? OB 			-> pushMode(CALLINSTRUCTION);
EMAIL   			 : '\\email' S? OB 				-> pushMode(CALLINSTRUCTION);
COMPUTEANSWER 		 : '\\computeanswer' S? OB 		-> pushMode(CALLINSTRUCTION);
PRECISION 			 : '\\precision' S? OB S?			-> pushMode(CALLINSTRUCTION);
RANGEDECLARATION 	 : '\\rangedeclaration' S? OB 	-> pushMode(CALLINSTRUCTION);
INTEGER 			    : '\\integer' S? OB 	{System.out.println("INTEGER ENTER");}		-> pushMode(CALLINSTRUCTION);
MATRIX              : '\\matrix' S? OB   {System.out.println("Matrix ENTER");}   -> pushMode(CALLINSTRUCTION);

STATEMENT           : '\\statement' S? OB       -> pushMode(CALLINSTRUCTION);
  


fragment OB : '{' (S|NEWLINE)*;
OP : '(' ;

S : [ \t\r]+;

mode CALLINSTRUCTION;

  	CB       : (S|NEWLINE)* '}' (S|NEWLINE)*   {System.out.println("pop CB");}         -> popMode;
  	CP       : (S|NEWLINE)* ')' (S|NEWLINE)*  ;


   NUM_INT           : S? [0-9]+ S?;
   NUM_REAL       : S? [0-9]+(('.' [0-9] + (EXPONENT)?)? | EXPONENT) S?;
   fragment EXPONENT : ('e') ('+' | '-')? [0-9] +;

//SEPARATORS
   COMMA          :S?','S?;    

//OPERATORS TOKEN
   DOTS           : S? '..' S?;
   EQUAL          :S? '=' S?;
	PLUS           : S?'+'S?;
   MINUS          : S?'-'S?;
   STAR           :S? '*'S?;
   SLASH          : S?'/'S?;
   POWER          : '^';

//TEST TOKENS
   AND            :S? 'and' S?;
   OR             :S? 'or'  S?;
   LESS           :S? '<'   S?;
   LESSEQUAL      :S? '<='  S?;
   GREATER        :S? '>'   S?;
   GREATEREQUAL   :S? '>='  S?;
   DBEQUAL        :S? '=='  S?;
   NOTEQUAL       :S? '!='  S?;

  //COMMANDS
	 DRAW     : 'draw'  S? OP ; 
   RANDINT :   'randint' S? OP ;


   //COMMAND-FLYDRAW
   POINT    :  NEWLINE S? 'point' S?;

   //INSTRUCTION
  	IF       : '\\if'  OB   -> pushMode(CALLINSTRUCTION);

   NEWLINE : '\n';

	 BSLASH   : '\\';
   ID       : [a-zA-Z]+;

   CR_OTHER : .;



   
