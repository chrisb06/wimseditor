parser grammar OEFParser;

options { tokenVocab=OEFLexer; }


document 		: 	pre EOF ;

pre 			:	title 
					language 
					author  email computeanswer  precision (rangedeclaration)* (integerinstruction | matrixinstruction )*;


//INSTRUCTIONS RULES
title 				: TITLE 			(stringGroup | callvariable)* CB;
language 			: LANGUAGE 			string CB;
author				: AUTHOR 			stringGroup CB;
email				: EMAIL 			stringGroup  CB;
computeanswer 		: COMPUTEANSWER 	string CB;
precision			: PRECISION 	 	NUM_INT  CB;
rangedeclaration 	: RANGEDECLARATION 	stringGroup  CB;
integerinstruction	: INTEGER 			assignementinteger CB;  // {System.out.println("Integer");}
matrixinstruction	: MATRIX 			assignementmatrix CB;
statement 			: STATEMENT 		(stringGroup | callvariable)*  CB;


expression
	: expression op=(PLUS | MINUS) expression			#addSub 
	| expression op=(STAR | SLASH) expression			#mulDiv
	| expression POWER expression						#power //operator is right associative normally add <assoc=right> but generate warning
	| (PLUS | MINUS) expression							#pluMin
	| OP expression CP									#parens //{System.out.println("expression-( )");}	
	| callcommand 										#keyRandint //{System.out.println("expression-command");}	
	| callvariable										#id
	| NUM_INT											#numInt
	;	


list
	: (expression | stringInListGroup) (COMMA (expression | stringInListGroup))*
	;

matrix
	: list (NEWLINE list)+		#expMatrix // At least a matrix of 1 col and 2 rows
	;


callvariable
 :  BSLASH ID // {System.out.println("callvariable:"+$ID.text);}	
 ;


relationalExpression
	: relationalExpression rel=(OR | AND) relationalExpression										#orAnd
	| relationalExpression rel=(LESS | LESSEQUAL | GREATER | GREATEREQUAL ) relationalExpression	#lessGreater
    | relationalExpression rel=(DBEQUAL | NOTEQUAL) relationalExpression							#equalNotEqual
  	| OP relationalExpression CP																	#parensRE
    | expression 																					#relExp
    ;

assignementinteger
	: ID EQUAL expression	{System.out.println("asignement:"+$ID.text);}							#assignInteger
	;

assignementmatrix
	: ID EQUAL matrix	{System.out.println("asignement:"+$ID.text);}							#assignMatrix
	;


stringInList
 : (CR_OTHER+~(COMMA|CB|NEWLINE))
 ;

stringInListGroup
 : stringInList+
 ;

string
 : CR_OTHER+
 | ID
 ;

stringGroup
 : string+
 ;

///LIST OF COMMANDS
callcommand
 : DRAW (expression | stringInListGroup) COMMA ( expression | stringInListGroup) callcommandflydraw* CP
 | RANDINT expression DOTS expression CP	
 ;

callcommandflydraw
:   POINT expression COMMA expression
; 


