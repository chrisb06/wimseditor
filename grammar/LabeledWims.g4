grammar LabeledWims;


NUM_INT   			: [0-9]+;
NUM_REAL			: [0-9]+(('.' [0-9] + (EXPONENT)?)? | EXPONENT);
fragment EXPONENT	: ('e') ('+' | '-')? [0-9] +;

PLUS				: '+';
MINUS				: '-';
STAR				: '*';
SLASH				: '/';
POWER 				: '^';
DOT					: '.';
OB					:  '{';
CB					:  '}';
OP					:  '(';
CP					:  ')';


////////TEST TOKENS
AND   				: 'and';
OR  				: 'or';
LESS 				: '<';
LESSEQUAL 			: '<=';
GREATER 			: '>';
GREATEREQUAL		: '>=';
DBEQUAL 			: '==';
NOTEQUAL 			: '!=';

YES					: 'yes';
NO					: 'no';
DOTS				: '..';
EQUAL				: '=';
BSLASH				: '\\';
SC 					: ';';


CRLF				: '\r'? '\n' | '\r';
WS					:  (' ' | '\t' | '\r' | '\n') -> skip;
//NEWLINE				: '\n';
/*==============================================================*/				
/*======================== Keywords ============================*/
/*==============================================================*/	

RANDINT : 'randint';

KEYWORD
					: RANDINT 
					| 'shuffle'
					| 'rows'
					;			// TO BE COMPLETED...

TYPE				: 'type'; 	//TYPE OF ANSWER DECLARATION
ANSWERTYPE
					: 'numeric' | 'numexp'  | 'units' | 'range'| 'vector' | 'matrix' 	//NUMERICAL VALUES
					| 'correspond' | 'reorder'											//ASSOCIATION OF OBJECTS
					| 'case' | 'nocase' | 'atext'										//WORD
					| 'menu' | 'radio' | 'click' | 'checkbox' | 'flashcard' | 'mark'	//MULTIPLE CHOICE 1
					| 'dragfill' | 'clickfill'											//MULTIPLE CHOICE 1
					| 'function' | 'algexp' | 'litexp'									//MATH EXPRESSION																// MATH FORMULA
					;


ID					: [a-zA-Z] ([a-zA-Z] | [0-9] | '-' | '_' )*;

// Doc https://github.com/antlr/antlr4/blob/master/doc/lexer-rules.md#lexer-rule-elements
// Include these following TOKENS makes unicode chars include in string rule
EMOJI : [\p{Emoji}] ;
JAPANESE : [\p{Script=Hiragana}\p{Script=Katakana}\p{Script=Han}] ;
NOT_CYRILLIC : [\P{Script=Cyrillic}] ;


/*==============================================================*/
/*=================== WIMS Commands ============================*/
/*==============================================================*/
TITLE				: BSLASH 'title';
LANGUAGE			: BSLASH 'language';
AUTHOR 				: BSLASH 'author';
EMAIL				: BSLASH 'email';
COMPUTEANSWER		: BSLASH 'computeanswer';
PRECISION			: BSLASH 'precision';
RANGE				: BSLASH 'range';
STATEMENT			: BSLASH 'statement';
HINT				: BSLASH 'hint';
HELP				: BSLASH 'help';

/*=================== Assignement Commands =====================*/
INTEGER				: BSLASH 'integer';
RATIONAL			: BSLASH 'rational';
REAL				: BSLASH 'real';
COMPLEX				: BSLASH 'complex';
TEXT				: BSLASH 'text';
MATRIX				: BSLASH 'matrix';
FUNCTION			: BSLASH 'function';
//ASSIGNEMENT         : INTEGER | RATIONAL | REAL | COMPLEX | TEXT | MATRIX | FUNCTION;
/*=================== Answer Commands ==========================*/
ANSWER				: BSLASH 'answer';

CONDITION			: BSLASH 'condition';
FEEDBACK			: BSLASH 'feedback';
SOLUTION			: BSLASH 'solution';

								
/*===========After all keywords, let s authorize the rest in unicode ========*/

sentence 
	: ( string | callvariable | KEYWORD OP )+?
	;
	

string
 : (~( BSLASH | CB))+ ; 
 

/*==============================================================*/				
/*==================== Variable Assignements ===================*/
/*==============================================================*/		

integer
	: INTEGER OB assignement CB #printInt
	;
rational
	: RATIONAL OB assignement CB
	;
real
	: REAL OB assignement CB
	;
complex
	: COMPLEX OB assignement CB
	;
text
	: TEXT OB assignement CB
	;
matrix
	: MATRIX OB assignement CB
	;
founction
	: FUNCTION OB assignement CB
	;
	
assignement
	: ID EQUAL expression								#assign
	;
	
/*==============================================================*/					
/*====================== EXPRESSIONS ===========================*/
/*==============================================================*/					

expression
	: expression op=(PLUS | MINUS) expression			#addSub
	| expression op=(STAR | SLASH) expression			#mulDiv
	| expression POWER expression						#power //operator is right associative normally add <assoc=right> but generate warning
	| (PLUS | MINUS) expression							#pluMin
	| OP expression CP									#parens
	| RANDINT OP expression DOTS expression CP			#keyRandint
	| callvariable										#id
	| NUM_INT											#numInt
	| NUM_REAL											#numReal
	;	

relationalExpression
	: relationalExpression rel=(OR | AND) relationalExpression										#orAnd
	| relationalExpression rel=(LESS | LESSEQUAL | GREATER | GREATEREQUAL ) relationalExpression	#lessGreater
    | relationalExpression rel=(DBEQUAL | NOTEQUAL) relationalExpression							#equalNotEqual
  	| OP relationalExpression CP																	#parensRE
    | expression 																					#relExp
    ;


callvariable
	: BSLASH ID
	;


/*==============================================================*/					
/*=================== WIMS EXERCICE ============================*/
/*==============================================================*/					
//PRE EXERCISE PART
title       
	: TITLE OB string CB
	;
language    
	: LANGUAGE OB string CB
	;
author
	: AUTHOR OB string CB
	;	
email
	: EMAIL OB string CB
	;	
computeanswer
	: COMPUTEANSWER OB ( YES| NO) CB
	;
precision
	: PRECISION OB NUM_INT CB
	;
rangedeclaration
	: RANGE OB expression DOTS expression CB
	;

//DURING EXERCISE PART
statement
	: STATEMENT OB sentence CB #statementExercise
	;
hint
	: HINT OB sentence CB
	;
help
	: HELP OB sentence CB
	;

//POST EXERCISE PART
answer
	: ANSWER OB sentence CB
	OB callvariable CB
	answertype
	;
answertype
	: OB TYPE EQUAL ANSWERTYPE CB
	;
condition
	: OB sentence CB
	;
feedback
	: FEEDBACK OB relationalExpression CB OB sentence CB
	;
solution
	: SOLUTION OB sentence CB
	;

pre
	: 	title ('\n')+
	  	language ('\n')+
		author ('\n')+
		email ('\n')+
		computeanswer ('\n')+
		precision  ('\n')+
		(rangedeclaration ('\n')+)*						
		(integer ('\n')+)*
	;				

peri
	: 	(statement ('\n')+)*
		(answer ('\n')+)*
	;	


post
	:	(feedback ('\n')+)*	
	;



wimsoef
	: 	pre peri post
	;

	