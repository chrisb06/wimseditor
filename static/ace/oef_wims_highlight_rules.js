/* ***** BEGIN LICENSE BLOCK *****
 * Distributed under the BSD license:
 *
 * Copyright (c) 2012, Ajax.org B.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Ajax.org B.V. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL AJAX.ORG B.V. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

/* This file was autogenerated from /Users/unice/Desktop/OEF-WIMS.tmLanguage (uuid: ) */
/****************************************************************************************
 * IT MIGHT NOT BE PERFECT ...But it's a good start from an existing *.tmlanguage file. *
 * fileTypes                                                                            *
 ****************************************************************************************/

define(function(require, exports, module) {
"use strict";

var oop = require("./oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var OEFWIMSHighlightRules = function() {
    // regexp must not have capturing parentheses. Use (?:) instead.
    // regexps are ordered -> the first match is used

    this.$rules = {
        start: [{
            include: "text.html.basic",
            comment: "Du moment que la ligne n'est pas détectée comme étant de l'OEF, elle sera colorée par HTML"
        }, {
            token: "comment.line.number-sign",
            regex: /^\s*#.*/,
            comment: "Commentaires commencant par #..."
        }, {
            token: "comment.line.%%",
            regex: /^\s*%%.*/,
            comment: "Commentaires commencant par %%..."
        }, {
            token: "keyword.operator.compare.wims",
            regex: /\b(?:==|!=|!=| < |<=| > |>=|isin|notin|iswordof|notwordof|isvarof|notvarof|isvariableof|notvariableof|isitemof|notitemof|islineof|notlineof|issamecase|notsamecase|issametext|notsametext|or|and)\b/,
            comment: "comparisons operators.."
        }, {
            token: "constant.language",
            regex: /\b(?:to|of|within|in|into|by|internal)\b/,
            comment: "langage reserved words"
        }, {
            token: "constant.language.wims",
            regex: /\b(?:append|nonempty|getopt|replace|embraced|randitem|text|char|charcnt|select|upper|nospace|sort|makelist|for|values|rows2lines|lines2items|items2words|tolower)\b/,
            comment: "wims functions callable in a wims() instruction"
        }, {
            token: "constant.language.pari",
            regex: /\bdivrem\b/,
            comment: "functions callable in a pari() instruction"
        }, {
            token: "storage.type",
            regex: /\\(?:real|complex|text|integer|rational|function|matrix)\b/,
            comment: "déclarations de variables"
        }, {
            token: "constant.language.types",
            regex: /\b(?:items|item|words|word|lines|line|rows|row|columns|column|position|asis|htmlmath|texmath)\b/,
            comment: "types d'éléments reconnus"
        }, {
            token: "entity.name.function.wims.maths",
            regex: /\b(?:evalue|solve|simplify|diff|int|int=|det|abs|sqrt|binomial|ceil|floor|rint|e|erf|erfc|Euler|exp|factorial|Inf|gcd|lcm|%|max|min|lg|lgamma|ln|log2|pow|sgn|PI|sin|acos|sh|Argch)(?=\()/,
            comment: "Maths functions"
        }, {
            token: "entity.name.function.wims.random",
            regex: /(?:random|randint|shuffle|randomitem|randomrow)(?=\()/,
            comment: "Wims random functions"
        }, {
            token: "entity.name.function.wims.external",
            regex: /(?:pari|maxima|yacas|wims|draw|slib|teximg)(?=\()/,
            comment: "Wims external functions"
        }, {
            token: "entity.name.function.wims.special",
            regex: /\b(?:reply|choice|step|sc_reply|reply_|help_subject|oef_firstname|oef_lastname|oef_login|oef_now|oef_lang)\b/,
            comment: "Wims special vars"
        }, {
            token: "entity.name.function.wims.oefcommands",
            regex: /^:?\\(?:title|language|author|email|format|css|keywords|credits|description|observation|precision|range|computeanswer|statement|answer|choice|condition|solution|hint|help|feedback|steps|nextstep|conditions|latex)\b/,
            comment: "OEF commands, starts at line beginning"
        }, {
            token: "entity.name.function.wims.oef.answer",
            regex: /(?<=\{)\s*type\s*(?=\=)/,
            comment: "answer parameters like \"{type=\""
        }, {
            token: "entity.name.tag.wims.record",
            regex: /^\:.*/,
            comment: "Record, \":\" at line beginning"
        }, {
            token: "entity.name.function.wims.oefcommands",
            regex: /\\embed\b/,
            comment: "OEF commands, starting everywhere"
        }, {
            token: "entity.name.function.wims.oefspecials",
            regex: /\\special{\s*(?:expandlines|imagefill|help|tabs2lines|rename|tooltip|codeinput|imageinput|mathmlinput|drawinput|glossary)\b/,
            comment: "OEF special commands"
        }, {
            token: "entity.name.function.wims",
            regex: /\b(?:algexp|aset|atext|case|checkbox|chembrut|chemclick|chemdraw|chemeq|chessgame|chset|click|clickfill|clicktile|compose|coord|correspond|crossword|default|dragfill|equation|flashcard|formal|fset|function|geogebra|javacurve|jmolclick|jsxgraph|jsxgraphcurve|keyboard|litexp|mark|matrix|menu|multipleclick|nocase|numeric|numexp|puzzle|radio|range|raw|reorder|set|sigunits|symtext|units|vector|wlist)\b/,
            comment: "OEF special commands"
        }, {
            token: "entity.name.function.wimscommand",
            regex: /\\(?:for|if|else)\b/,
            comment: "Wims functions, starting with \"!\" (like \"!changeto\")..."
        }, {
            token: "variable.parameter.wims",
            regex: /\\[a-zA-Z0-9_]+/,
            comment: "Variables like \\PARAM1, \\TM_SELECTION..."
        }, {
            token: "keyword.operator",
            regex: /\*|\+| - |\^|:=|=| \/\/ | \? /,
            comment: "numerical operators..."
        }, {
            token: "constant.numeric",
            regex: /\b[0-9]+\b/,
            comment: "constantes numériques"
        }, {
            token: "entity.name.function.wims.oef.models",
            regex: /^(?:type|modele_jalon|textarea|asis|random|computed|iEdit)(?=\=)/,
            comment: "OEF commands for models, starts at line beginning"
        }, {
            token: "variable.parameter.wims",
            regex: /\$(?:embraced_randitem|oef_help|math_help)/,
            comment: "Wims known variables like $embraced_randitem, $oef_help..."
        }, {
            token: "variable.parameter.wims",
            regex: /\$(?:embraced_randitem|oef_help|math_help)/,
            comment: "Wims known variables like $embraced_randitem, $oef_help..."
        }, {
            token: "text",
            regex: /\\\(/,
            push: [{
                token: "text",
                regex: /\)/,
                next: "pop"
            }, {
                include: "text.tex"
            }],
            comment: "Inclusions LaTeX Maths \\(\\sqrt{\\frac{\\x}{\\y}}\\)"
        }]
    }
    
    this.normalizeRules();
};

OEFWIMSHighlightRules.metaData = {
    fileTypes: ["oef", "oef.dtml"],
    name: "OEF WIMS",
    scopeName: "source.oef"
}


oop.inherits(OEFWIMSHighlightRules, TextHighlightRules);

exports.OEFWIMSHighlightRules = OEFWIMSHighlightRules;
});