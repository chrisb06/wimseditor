
document.getElementById("parse").addEventListener("click",parse);
document.getElementById("somedebugactions").addEventListener("click",somedebugactions);


var DEBUG = true;
var ERROR = true;

var antlr4 = require('antlr4/index');
var MyWimsLexer = require('./generated-parser/LabeledWimsLexer').LabeledWimsLexer;
var MyWimsParser = require('./generated-parser/LabeledWimsParser').LabeledWimsParser;

var {LabeledWimsVisitor} = require('./generated-parser/LabeledWimsVisitor');
var {WimsLexerParser} = require('./objects/WimsLexerParser');
var {WimsLexerParserErrorListener} = require('./objects/WimsLexerParserErrorListener');
var {WimsCommands} = require('./objects/WimsCommands');


var {WimsNumeric} = require('./objects/WimsElement');
var {WimsInteger} = require('./objects/WimsElement');
var {WimsString} = require('./objects/WimsElement');

var {WimsExercise} = require('./objects/WimsExercise');
var {WimsAnswer} = require('./objects/WimsAnswer');
var {WimsNumericAnswer} = require('./objects/WimsAnswer');


var {WimsInt} = require('./objects/WimsElement');
var {WimsExercise} = require('./objects/WimsExercise');

var exercise;

function parse() {
    clearBox('statement');
    clearBox('answer');
    clearBox('feedback');
    clearBox('error');
    clearBox('debug');

    DEBUG = document.getElementById("debugmode").checked;
    ERROR = document.getElementById("errormode").checked;

    //DEPUIS UN FICHIER
    //Pour prise en compte UTF-8 : https://github.com/antlr/antlr4/blob/master/doc/unicode.md
    //var input = fs.readFileSync("3.oef", 'utf-8');
    //var chars = new antlr4.InputStream(input);

    //DEPUIS LE FORMULAIRE
    var input = editor.getValue();
    if (hasUnicode(input))
    {
        appendDEBUG("OEF contains Unicode Char : ANTLR4 JS Lexer and Parser with UTF-8 support is imcomplete.");
        appendDEBUG("Input OEF : "+input);
    }
    exercise = new WimsExercise(input);
}

function somedebugactions(event){
    clearBox('error');
    clearBox('debug');    
}



function hasUnicode (str) {
    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 127) return true;
    }
    return false;
}

function clearBox(elementID){
    document.getElementById(elementID).innerHTML = "";
}


function appendDEBUG(text,balise="p"){
    if (DEBUG)
    {
        var node = document.createElement(balise);                 
        var textnode = document.createTextNode(text);  
        node.appendChild(textnode);                              
        document.getElementById("debug").appendChild(node);
    }
}
