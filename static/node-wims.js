
console.log("Start");

const fs = require("fs");
const antlr4 = require('antlr4/index');
const TodoLexer = require('../static/generated-parser/LabeledWimsLexer');
const TodoParser = require('../static/generated-parser/LabeledWimsParser');
const MyWimsListener = require("../static/generated-parser/MyWimsListener").MyWimsListener;
    
    //DEPUIS UN FICHIER
    //Pour prise en compte UTF-8 : https://github.com/antlr/antlr4/blob/master/doc/unicode.md
    var input = fs.readFileSync("../static/3.oef", 'utf-8');
    var chars = new antlr4.InputStream(input);

    //DEPUIS LE FORMULAIRE
  //   var input = document.getElementById("code").value;

//    var chars = antlr4.CharStreams.fromString(input);

        console.log("Parsed: "+ chars);

    //OU
    //var chars = new antlr4.InputStream(input);
    
    //console.log("Parsed: "+ chars);
    var lexer = new TodoLexer.LabeledWimsLexer(chars);
    var tokens  = new antlr4.CommonTokenStream(lexer);
    console.log("Parsed: "+ tokens);
    var parser = new TodoParser.LabeledWimsParser(tokens);
    parser.buildParseTrees = true;
    var tree = parser.wims();

    var extractor = new MyWimsListener();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(extractor, tree);

    appendHTML("Tree in LISP display:","p");
    appendHTML(tree.toStringTree(parser.ruleNames),"p");
   



function appendHTML(text,balise="p"){                    // Append the text to <li>
    console.log(text);

}
