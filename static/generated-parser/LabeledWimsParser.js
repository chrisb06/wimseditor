// Generated from LabeledWims.g4 by ANTLR 4.7.2
// jshint ignore: start
var antlr4 = require('antlr4/index');
var LabeledWimsVisitor = require('./LabeledWimsVisitor').LabeledWimsVisitor;

var grammarFileName = "LabeledWims.g4";


var serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
    "\u0003>\u0170\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004\t",
    "\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007\u0004",
    "\b\t\b\u0004\t\t\t\u0004\n\t\n\u0004\u000b\t\u000b\u0004\f\t\f\u0004",
    "\r\t\r\u0004\u000e\t\u000e\u0004\u000f\t\u000f\u0004\u0010\t\u0010\u0004",
    "\u0011\t\u0011\u0004\u0012\t\u0012\u0004\u0013\t\u0013\u0004\u0014\t",
    "\u0014\u0004\u0015\t\u0015\u0004\u0016\t\u0016\u0004\u0017\t\u0017\u0004",
    "\u0018\t\u0018\u0004\u0019\t\u0019\u0004\u001a\t\u001a\u0004\u001b\t",
    "\u001b\u0004\u001c\t\u001c\u0004\u001d\t\u001d\u0004\u001e\t\u001e\u0004",
    "\u001f\t\u001f\u0004 \t \u0004!\t!\u0004\"\t\"\u0003\u0002\u0005\u0002",
    "F\n\u0002\u0003\u0002\u0003\u0002\u0003\u0002\u0005\u0002K\n\u0002\u0003",
    "\u0002\u0005\u0002N\n\u0002\u0007\u0002P\n\u0002\f\u0002\u000e\u0002",
    "S\u000b\u0002\u0003\u0003\u0003\u0003\u0003\u0004\u0006\u0004X\n\u0004",
    "\r\u0004\u000e\u0004Y\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0003\u0006\u0003\u0006\u0003\u0006\u0003\u0006\u0003\u0006",
    "\u0003\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0003\b",
    "\u0003\b\u0003\b\u0003\b\u0003\b\u0003\t\u0003\t\u0003\t\u0003\t\u0003",
    "\t\u0003\n\u0003\n\u0003\n\u0003\n\u0003\n\u0003\u000b\u0003\u000b\u0003",
    "\u000b\u0003\u000b\u0003\u000b\u0003\f\u0003\f\u0003\f\u0003\f\u0003",
    "\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003",
    "\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0005\r\u0094",
    "\n\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r",
    "\u0003\r\u0007\r\u009f\n\r\f\r\u000e\r\u00a2\u000b\r\u0003\u000e\u0003",
    "\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0005\u000e\u00aa",
    "\n\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0003\u000e",
    "\u0003\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0007\u000e\u00b5\n",
    "\u000e\f\u000e\u000e\u000e\u00b8\u000b\u000e\u0003\u000f\u0003\u000f",
    "\u0003\u000f\u0003\u0010\u0003\u0010\u0003\u0010\u0003\u0010\u0003\u0010",
    "\u0003\u0011\u0003\u0011\u0003\u0011\u0003\u0011\u0003\u0011\u0003\u0012",
    "\u0003\u0012\u0003\u0012\u0003\u0012\u0003\u0012\u0003\u0013\u0003\u0013",
    "\u0003\u0013\u0003\u0013\u0003\u0013\u0003\u0014\u0003\u0014\u0003\u0014",
    "\u0003\u0014\u0003\u0014\u0003\u0015\u0003\u0015\u0003\u0015\u0003\u0015",
    "\u0003\u0015\u0003\u0016\u0003\u0016\u0003\u0016\u0003\u0016\u0003\u0016",
    "\u0003\u0016\u0003\u0016\u0003\u0017\u0003\u0017\u0003\u0017\u0003\u0017",
    "\u0003\u0017\u0003\u0018\u0003\u0018\u0003\u0018\u0003\u0018\u0003\u0018",
    "\u0003\u0019\u0003\u0019\u0003\u0019\u0003\u0019\u0003\u0019\u0003\u001a",
    "\u0003\u001a\u0003\u001a\u0003\u001a\u0003\u001a\u0003\u001a\u0003\u001a",
    "\u0003\u001a\u0003\u001a\u0003\u001b\u0003\u001b\u0003\u001b\u0003\u001b",
    "\u0003\u001b\u0003\u001b\u0003\u001c\u0003\u001c\u0003\u001c\u0003\u001c",
    "\u0003\u001d\u0003\u001d\u0003\u001d\u0003\u001d\u0003\u001d\u0003\u001d",
    "\u0003\u001d\u0003\u001d\u0003\u001e\u0003\u001e\u0003\u001e\u0003\u001e",
    "\u0003\u001e\u0003\u001f\u0003\u001f\u0006\u001f\u0113\n\u001f\r\u001f",
    "\u000e\u001f\u0114\u0003\u001f\u0003\u001f\u0006\u001f\u0119\n\u001f",
    "\r\u001f\u000e\u001f\u011a\u0003\u001f\u0003\u001f\u0006\u001f\u011f",
    "\n\u001f\r\u001f\u000e\u001f\u0120\u0003\u001f\u0003\u001f\u0006\u001f",
    "\u0125\n\u001f\r\u001f\u000e\u001f\u0126\u0003\u001f\u0003\u001f\u0006",
    "\u001f\u012b\n\u001f\r\u001f\u000e\u001f\u012c\u0003\u001f\u0003\u001f",
    "\u0006\u001f\u0131\n\u001f\r\u001f\u000e\u001f\u0132\u0003\u001f\u0003",
    "\u001f\u0006\u001f\u0137\n\u001f\r\u001f\u000e\u001f\u0138\u0007\u001f",
    "\u013b\n\u001f\f\u001f\u000e\u001f\u013e\u000b\u001f\u0003\u001f\u0003",
    "\u001f\u0006\u001f\u0142\n\u001f\r\u001f\u000e\u001f\u0143\u0007\u001f",
    "\u0146\n\u001f\f\u001f\u000e\u001f\u0149\u000b\u001f\u0003 \u0003 \u0006",
    " \u014d\n \r \u000e \u014e\u0007 \u0151\n \f \u000e \u0154\u000b \u0003",
    " \u0003 \u0006 \u0158\n \r \u000e \u0159\u0007 \u015c\n \f \u000e \u015f",
    "\u000b \u0003!\u0003!\u0006!\u0163\n!\r!\u000e!\u0164\u0007!\u0167\n",
    "!\f!\u000e!\u016a\u000b!\u0003\"\u0003\"\u0003\"\u0003\"\u0003\"\u0002",
    "\u0004\u0018\u001a#\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012\u0014",
    "\u0016\u0018\u001a\u001c\u001e \"$&(*,.02468:<>@B\u0002\n\u0003\u0002",
    " !\u0004\u0002\r\r\u001e\u001e\u0003\u0002\u0006\u0007\u0003\u0002\b",
    "\t\u0003\u0002\u0012\u0013\u0003\u0002\u0014\u0017\u0003\u0002\u0018",
    "\u0019\u0003\u0002\u001a\u001b\u0002\u016f\u0002E\u0003\u0002\u0002",
    "\u0002\u0004T\u0003\u0002\u0002\u0002\u0006W\u0003\u0002\u0002\u0002",
    "\b[\u0003\u0002\u0002\u0002\n`\u0003\u0002\u0002\u0002\fe\u0003\u0002",
    "\u0002\u0002\u000ej\u0003\u0002\u0002\u0002\u0010o\u0003\u0002\u0002",
    "\u0002\u0012t\u0003\u0002\u0002\u0002\u0014y\u0003\u0002\u0002\u0002",
    "\u0016~\u0003\u0002\u0002\u0002\u0018\u0093\u0003\u0002\u0002\u0002",
    "\u001a\u00a9\u0003\u0002\u0002\u0002\u001c\u00b9\u0003\u0002\u0002\u0002",
    "\u001e\u00bc\u0003\u0002\u0002\u0002 \u00c1\u0003\u0002\u0002\u0002",
    "\"\u00c6\u0003\u0002\u0002\u0002$\u00cb\u0003\u0002\u0002\u0002&\u00d0",
    "\u0003\u0002\u0002\u0002(\u00d5\u0003\u0002\u0002\u0002*\u00da\u0003",
    "\u0002\u0002\u0002,\u00e1\u0003\u0002\u0002\u0002.\u00e6\u0003\u0002",
    "\u0002\u00020\u00eb\u0003\u0002\u0002\u00022\u00f0\u0003\u0002\u0002",
    "\u00024\u00f9\u0003\u0002\u0002\u00026\u00ff\u0003\u0002\u0002\u0002",
    "8\u0103\u0003\u0002\u0002\u0002:\u010b\u0003\u0002\u0002\u0002<\u0110",
    "\u0003\u0002\u0002\u0002>\u0152\u0003\u0002\u0002\u0002@\u0168\u0003",
    "\u0002\u0002\u0002B\u016b\u0003\u0002\u0002\u0002DF\u0005\u0004\u0003",
    "\u0002ED\u0003\u0002\u0002\u0002EF\u0003\u0002\u0002\u0002FQ\u0003\u0002",
    "\u0002\u0002GK\u0005\u001c\u000f\u0002HI\u0007#\u0002\u0002IK\u0007",
    "\u000e\u0002\u0002JG\u0003\u0002\u0002\u0002JH\u0003\u0002\u0002\u0002",
    "KM\u0003\u0002\u0002\u0002LN\u0005\u0004\u0003\u0002ML\u0003\u0002\u0002",
    "\u0002MN\u0003\u0002\u0002\u0002NP\u0003\u0002\u0002\u0002OJ\u0003\u0002",
    "\u0002\u0002PS\u0003\u0002\u0002\u0002QO\u0003\u0002\u0002\u0002QR\u0003",
    "\u0002\u0002\u0002R\u0003\u0003\u0002\u0002\u0002SQ\u0003\u0002\u0002",
    "\u0002TU\t\u0002\u0002\u0002U\u0005\u0003\u0002\u0002\u0002VX\n\u0003",
    "\u0002\u0002WV\u0003\u0002\u0002\u0002XY\u0003\u0002\u0002\u0002YW\u0003",
    "\u0002\u0002\u0002YZ\u0003\u0002\u0002\u0002Z\u0007\u0003\u0002\u0002",
    "\u0002[\\\u00074\u0002\u0002\\]\u0007\f\u0002\u0002]^\u0005\u0016\f",
    "\u0002^_\u0007\r\u0002\u0002_\t\u0003\u0002\u0002\u0002`a\u00075\u0002",
    "\u0002ab\u0007\f\u0002\u0002bc\u0005\u0016\f\u0002cd\u0007\r\u0002\u0002",
    "d\u000b\u0003\u0002\u0002\u0002ef\u00076\u0002\u0002fg\u0007\f\u0002",
    "\u0002gh\u0005\u0016\f\u0002hi\u0007\r\u0002\u0002i\r\u0003\u0002\u0002",
    "\u0002jk\u00077\u0002\u0002kl\u0007\f\u0002\u0002lm\u0005\u0016\f\u0002",
    "mn\u0007\r\u0002\u0002n\u000f\u0003\u0002\u0002\u0002op\u00078\u0002",
    "\u0002pq\u0007\f\u0002\u0002qr\u0005\u0016\f\u0002rs\u0007\r\u0002\u0002",
    "s\u0011\u0003\u0002\u0002\u0002tu\u00079\u0002\u0002uv\u0007\f\u0002",
    "\u0002vw\u0005\u0016\f\u0002wx\u0007\r\u0002\u0002x\u0013\u0003\u0002",
    "\u0002\u0002yz\u0007:\u0002\u0002z{\u0007\f\u0002\u0002{|\u0005\u0016",
    "\f\u0002|}\u0007\r\u0002\u0002}\u0015\u0003\u0002\u0002\u0002~\u007f",
    "\u0007&\u0002\u0002\u007f\u0080\u0007\u001d\u0002\u0002\u0080\u0081",
    "\u0005\u0018\r\u0002\u0081\u0017\u0003\u0002\u0002\u0002\u0082\u0083",
    "\b\r\u0001\u0002\u0083\u0084\t\u0004\u0002\u0002\u0084\u0094\u0005\u0018",
    "\r\b\u0085\u0086\u0007\u000e\u0002\u0002\u0086\u0087\u0005\u0018\r\u0002",
    "\u0087\u0088\u0007\u000f\u0002\u0002\u0088\u0094\u0003\u0002\u0002\u0002",
    "\u0089\u008a\u0007\"\u0002\u0002\u008a\u008b\u0007\u000e\u0002\u0002",
    "\u008b\u008c\u0005\u0018\r\u0002\u008c\u008d\u0007\u001c\u0002\u0002",
    "\u008d\u008e\u0005\u0018\r\u0002\u008e\u008f\u0007\u000f\u0002\u0002",
    "\u008f\u0094\u0003\u0002\u0002\u0002\u0090\u0094\u0005\u001c\u000f\u0002",
    "\u0091\u0094\u0007\u0004\u0002\u0002\u0092\u0094\u0007\u0005\u0002\u0002",
    "\u0093\u0082\u0003\u0002\u0002\u0002\u0093\u0085\u0003\u0002\u0002\u0002",
    "\u0093\u0089\u0003\u0002\u0002\u0002\u0093\u0090\u0003\u0002\u0002\u0002",
    "\u0093\u0091\u0003\u0002\u0002\u0002\u0093\u0092\u0003\u0002\u0002\u0002",
    "\u0094\u00a0\u0003\u0002\u0002\u0002\u0095\u0096\f\u000b\u0002\u0002",
    "\u0096\u0097\t\u0004\u0002\u0002\u0097\u009f\u0005\u0018\r\f\u0098\u0099",
    "\f\n\u0002\u0002\u0099\u009a\t\u0005\u0002\u0002\u009a\u009f\u0005\u0018",
    "\r\u000b\u009b\u009c\f\t\u0002\u0002\u009c\u009d\u0007\n\u0002\u0002",
    "\u009d\u009f\u0005\u0018\r\n\u009e\u0095\u0003\u0002\u0002\u0002\u009e",
    "\u0098\u0003\u0002\u0002\u0002\u009e\u009b\u0003\u0002\u0002\u0002\u009f",
    "\u00a2\u0003\u0002\u0002\u0002\u00a0\u009e\u0003\u0002\u0002\u0002\u00a0",
    "\u00a1\u0003\u0002\u0002\u0002\u00a1\u0019\u0003\u0002\u0002\u0002\u00a2",
    "\u00a0\u0003\u0002\u0002\u0002\u00a3\u00a4\b\u000e\u0001\u0002\u00a4",
    "\u00a5\u0007\u000e\u0002\u0002\u00a5\u00a6\u0005\u001a\u000e\u0002\u00a6",
    "\u00a7\u0007\u000f\u0002\u0002\u00a7\u00aa\u0003\u0002\u0002\u0002\u00a8",
    "\u00aa\u0005\u0018\r\u0002\u00a9\u00a3\u0003\u0002\u0002\u0002\u00a9",
    "\u00a8\u0003\u0002\u0002\u0002\u00aa\u00b6\u0003\u0002\u0002\u0002\u00ab",
    "\u00ac\f\u0007\u0002\u0002\u00ac\u00ad\t\u0006\u0002\u0002\u00ad\u00b5",
    "\u0005\u001a\u000e\b\u00ae\u00af\f\u0006\u0002\u0002\u00af\u00b0\t\u0007",
    "\u0002\u0002\u00b0\u00b5\u0005\u001a\u000e\u0007\u00b1\u00b2\f\u0005",
    "\u0002\u0002\u00b2\u00b3\t\b\u0002\u0002\u00b3\u00b5\u0005\u001a\u000e",
    "\u0006\u00b4\u00ab\u0003\u0002\u0002\u0002\u00b4\u00ae\u0003\u0002\u0002",
    "\u0002\u00b4\u00b1\u0003\u0002\u0002\u0002\u00b5\u00b8\u0003\u0002\u0002",
    "\u0002\u00b6\u00b4\u0003\u0002\u0002\u0002\u00b6\u00b7\u0003\u0002\u0002",
    "\u0002\u00b7\u001b\u0003\u0002\u0002\u0002\u00b8\u00b6\u0003\u0002\u0002",
    "\u0002\u00b9\u00ba\u0007\u001e\u0002\u0002\u00ba\u00bb\u0007&\u0002",
    "\u0002\u00bb\u001d\u0003\u0002\u0002\u0002\u00bc\u00bd\u0007*\u0002",
    "\u0002\u00bd\u00be\u0007\f\u0002\u0002\u00be\u00bf\u0005\u0006\u0004",
    "\u0002\u00bf\u00c0\u0007\r\u0002\u0002\u00c0\u001f\u0003\u0002\u0002",
    "\u0002\u00c1\u00c2\u0007+\u0002\u0002\u00c2\u00c3\u0007\f\u0002\u0002",
    "\u00c3\u00c4\u0005\u0006\u0004\u0002\u00c4\u00c5\u0007\r\u0002\u0002",
    "\u00c5!\u0003\u0002\u0002\u0002\u00c6\u00c7\u0007,\u0002\u0002\u00c7",
    "\u00c8\u0007\f\u0002\u0002\u00c8\u00c9\u0005\u0006\u0004\u0002\u00c9",
    "\u00ca\u0007\r\u0002\u0002\u00ca#\u0003\u0002\u0002\u0002\u00cb\u00cc",
    "\u0007-\u0002\u0002\u00cc\u00cd\u0007\f\u0002\u0002\u00cd\u00ce\u0005",
    "\u0006\u0004\u0002\u00ce\u00cf\u0007\r\u0002\u0002\u00cf%\u0003\u0002",
    "\u0002\u0002\u00d0\u00d1\u0007.\u0002\u0002\u00d1\u00d2\u0007\f\u0002",
    "\u0002\u00d2\u00d3\t\t\u0002\u0002\u00d3\u00d4\u0007\r\u0002\u0002\u00d4",
    "\'\u0003\u0002\u0002\u0002\u00d5\u00d6\u0007/\u0002\u0002\u00d6\u00d7",
    "\u0007\f\u0002\u0002\u00d7\u00d8\u0007\u0004\u0002\u0002\u00d8\u00d9",
    "\u0007\r\u0002\u0002\u00d9)\u0003\u0002\u0002\u0002\u00da\u00db\u0007",
    "0\u0002\u0002\u00db\u00dc\u0007\f\u0002\u0002\u00dc\u00dd\u0005\u0018",
    "\r\u0002\u00dd\u00de\u0007\u001c\u0002\u0002\u00de\u00df\u0005\u0018",
    "\r\u0002\u00df\u00e0\u0007\r\u0002\u0002\u00e0+\u0003\u0002\u0002\u0002",
    "\u00e1\u00e2\u00071\u0002\u0002\u00e2\u00e3\u0007\f\u0002\u0002\u00e3",
    "\u00e4\u0005\u0002\u0002\u0002\u00e4\u00e5\u0007\r\u0002\u0002\u00e5",
    "-\u0003\u0002\u0002\u0002\u00e6\u00e7\u00072\u0002\u0002\u00e7\u00e8",
    "\u0007\f\u0002\u0002\u00e8\u00e9\u0005\u0002\u0002\u0002\u00e9\u00ea",
    "\u0007\r\u0002\u0002\u00ea/\u0003\u0002\u0002\u0002\u00eb\u00ec\u0007",
    "3\u0002\u0002\u00ec\u00ed\u0007\f\u0002\u0002\u00ed\u00ee\u0005\u0002",
    "\u0002\u0002\u00ee\u00ef\u0007\r\u0002\u0002\u00ef1\u0003\u0002\u0002",
    "\u0002\u00f0\u00f1\u0007;\u0002\u0002\u00f1\u00f2\u0007\f\u0002\u0002",
    "\u00f2\u00f3\u0005\u0002\u0002\u0002\u00f3\u00f4\u0007\r\u0002\u0002",
    "\u00f4\u00f5\u0007\f\u0002\u0002\u00f5\u00f6\u0005\u001c\u000f\u0002",
    "\u00f6\u00f7\u0007\r\u0002\u0002\u00f7\u00f8\u00054\u001b\u0002\u00f8",
    "3\u0003\u0002\u0002\u0002\u00f9\u00fa\u0007\f\u0002\u0002\u00fa\u00fb",
    "\u0007$\u0002\u0002\u00fb\u00fc\u0007\u001d\u0002\u0002\u00fc\u00fd",
    "\u0007%\u0002\u0002\u00fd\u00fe\u0007\r\u0002\u0002\u00fe5\u0003\u0002",
    "\u0002\u0002\u00ff\u0100\u0007\f\u0002\u0002\u0100\u0101\u0005\u0002",
    "\u0002\u0002\u0101\u0102\u0007\r\u0002\u0002\u01027\u0003\u0002\u0002",
    "\u0002\u0103\u0104\u0007=\u0002\u0002\u0104\u0105\u0007\f\u0002\u0002",
    "\u0105\u0106\u0005\u001a\u000e\u0002\u0106\u0107\u0007\r\u0002\u0002",
    "\u0107\u0108\u0007\f\u0002\u0002\u0108\u0109\u0005\u0002\u0002\u0002",
    "\u0109\u010a\u0007\r\u0002\u0002\u010a9\u0003\u0002\u0002\u0002\u010b",
    "\u010c\u0007>\u0002\u0002\u010c\u010d\u0007\f\u0002\u0002\u010d\u010e",
    "\u0005\u0002\u0002\u0002\u010e\u010f\u0007\r\u0002\u0002\u010f;\u0003",
    "\u0002\u0002\u0002\u0110\u0112\u0005\u001e\u0010\u0002\u0111\u0113\u0007",
    "\u0003\u0002\u0002\u0112\u0111\u0003\u0002\u0002\u0002\u0113\u0114\u0003",
    "\u0002\u0002\u0002\u0114\u0112\u0003\u0002\u0002\u0002\u0114\u0115\u0003",
    "\u0002\u0002\u0002\u0115\u0116\u0003\u0002\u0002\u0002\u0116\u0118\u0005",
    " \u0011\u0002\u0117\u0119\u0007\u0003\u0002\u0002\u0118\u0117\u0003",
    "\u0002\u0002\u0002\u0119\u011a\u0003\u0002\u0002\u0002\u011a\u0118\u0003",
    "\u0002\u0002\u0002\u011a\u011b\u0003\u0002\u0002\u0002\u011b\u011c\u0003",
    "\u0002\u0002\u0002\u011c\u011e\u0005\"\u0012\u0002\u011d\u011f\u0007",
    "\u0003\u0002\u0002\u011e\u011d\u0003\u0002\u0002\u0002\u011f\u0120\u0003",
    "\u0002\u0002\u0002\u0120\u011e\u0003\u0002\u0002\u0002\u0120\u0121\u0003",
    "\u0002\u0002\u0002\u0121\u0122\u0003\u0002\u0002\u0002\u0122\u0124\u0005",
    "$\u0013\u0002\u0123\u0125\u0007\u0003\u0002\u0002\u0124\u0123\u0003",
    "\u0002\u0002\u0002\u0125\u0126\u0003\u0002\u0002\u0002\u0126\u0124\u0003",
    "\u0002\u0002\u0002\u0126\u0127\u0003\u0002\u0002\u0002\u0127\u0128\u0003",
    "\u0002\u0002\u0002\u0128\u012a\u0005&\u0014\u0002\u0129\u012b\u0007",
    "\u0003\u0002\u0002\u012a\u0129\u0003\u0002\u0002\u0002\u012b\u012c\u0003",
    "\u0002\u0002\u0002\u012c\u012a\u0003\u0002\u0002\u0002\u012c\u012d\u0003",
    "\u0002\u0002\u0002\u012d\u012e\u0003\u0002\u0002\u0002\u012e\u0130\u0005",
    "(\u0015\u0002\u012f\u0131\u0007\u0003\u0002\u0002\u0130\u012f\u0003",
    "\u0002\u0002\u0002\u0131\u0132\u0003\u0002\u0002\u0002\u0132\u0130\u0003",
    "\u0002\u0002\u0002\u0132\u0133\u0003\u0002\u0002\u0002\u0133\u013c\u0003",
    "\u0002\u0002\u0002\u0134\u0136\u0005*\u0016\u0002\u0135\u0137\u0007",
    "\u0003\u0002\u0002\u0136\u0135\u0003\u0002\u0002\u0002\u0137\u0138\u0003",
    "\u0002\u0002\u0002\u0138\u0136\u0003\u0002\u0002\u0002\u0138\u0139\u0003",
    "\u0002\u0002\u0002\u0139\u013b\u0003\u0002\u0002\u0002\u013a\u0134\u0003",
    "\u0002\u0002\u0002\u013b\u013e\u0003\u0002\u0002\u0002\u013c\u013a\u0003",
    "\u0002\u0002\u0002\u013c\u013d\u0003\u0002\u0002\u0002\u013d\u0147\u0003",
    "\u0002\u0002\u0002\u013e\u013c\u0003\u0002\u0002\u0002\u013f\u0141\u0005",
    "\b\u0005\u0002\u0140\u0142\u0007\u0003\u0002\u0002\u0141\u0140\u0003",
    "\u0002\u0002\u0002\u0142\u0143\u0003\u0002\u0002\u0002\u0143\u0141\u0003",
    "\u0002\u0002\u0002\u0143\u0144\u0003\u0002\u0002\u0002\u0144\u0146\u0003",
    "\u0002\u0002\u0002\u0145\u013f\u0003\u0002\u0002\u0002\u0146\u0149\u0003",
    "\u0002\u0002\u0002\u0147\u0145\u0003\u0002\u0002\u0002\u0147\u0148\u0003",
    "\u0002\u0002\u0002\u0148=\u0003\u0002\u0002\u0002\u0149\u0147\u0003",
    "\u0002\u0002\u0002\u014a\u014c\u0005,\u0017\u0002\u014b\u014d\u0007",
    "\u0003\u0002\u0002\u014c\u014b\u0003\u0002\u0002\u0002\u014d\u014e\u0003",
    "\u0002\u0002\u0002\u014e\u014c\u0003\u0002\u0002\u0002\u014e\u014f\u0003",
    "\u0002\u0002\u0002\u014f\u0151\u0003\u0002\u0002\u0002\u0150\u014a\u0003",
    "\u0002\u0002\u0002\u0151\u0154\u0003\u0002\u0002\u0002\u0152\u0150\u0003",
    "\u0002\u0002\u0002\u0152\u0153\u0003\u0002\u0002\u0002\u0153\u015d\u0003",
    "\u0002\u0002\u0002\u0154\u0152\u0003\u0002\u0002\u0002\u0155\u0157\u0005",
    "2\u001a\u0002\u0156\u0158\u0007\u0003\u0002\u0002\u0157\u0156\u0003",
    "\u0002\u0002\u0002\u0158\u0159\u0003\u0002\u0002\u0002\u0159\u0157\u0003",
    "\u0002\u0002\u0002\u0159\u015a\u0003\u0002\u0002\u0002\u015a\u015c\u0003",
    "\u0002\u0002\u0002\u015b\u0155\u0003\u0002\u0002\u0002\u015c\u015f\u0003",
    "\u0002\u0002\u0002\u015d\u015b\u0003\u0002\u0002\u0002\u015d\u015e\u0003",
    "\u0002\u0002\u0002\u015e?\u0003\u0002\u0002\u0002\u015f\u015d\u0003",
    "\u0002\u0002\u0002\u0160\u0162\u00058\u001d\u0002\u0161\u0163\u0007",
    "\u0003\u0002\u0002\u0162\u0161\u0003\u0002\u0002\u0002\u0163\u0164\u0003",
    "\u0002\u0002\u0002\u0164\u0162\u0003\u0002\u0002\u0002\u0164\u0165\u0003",
    "\u0002\u0002\u0002\u0165\u0167\u0003\u0002\u0002\u0002\u0166\u0160\u0003",
    "\u0002\u0002\u0002\u0167\u016a\u0003\u0002\u0002\u0002\u0168\u0166\u0003",
    "\u0002\u0002\u0002\u0168\u0169\u0003\u0002\u0002\u0002\u0169A\u0003",
    "\u0002\u0002\u0002\u016a\u0168\u0003\u0002\u0002\u0002\u016b\u016c\u0005",
    "<\u001f\u0002\u016c\u016d\u0005> \u0002\u016d\u016e\u0005@!\u0002\u016e",
    "C\u0003\u0002\u0002\u0002\u001dEJMQY\u0093\u009e\u00a0\u00a9\u00b4\u00b6",
    "\u0114\u011a\u0120\u0126\u012c\u0132\u0138\u013c\u0143\u0147\u014e\u0152",
    "\u0159\u015d\u0164\u0168"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ null, "'\n'", null, null, "'+'", "'-'", "'*'", "'/'", 
                     "'^'", "'.'", "'{'", "'}'", "'('", "')'", "'&'", "'#'", 
                     "'and'", "'or'", "'<'", "'<='", "'>'", "'>='", "'=='", 
                     "'!='", "'yes'", "'no'", "'..'", "'='", "'\\'", "';'", 
                     null, null, "'randint'", null, "'type'" ];

var symbolicNames = [ null, null, "NUM_INT", "NUM_REAL", "PLUS", "MINUS", 
                      "STAR", "SLASH", "POWER", "DOT", "OB", "CB", "OP", 
                      "CP", "AMPERSAND", "HASH", "AND", "OR", "LESS", "LESSEQUAL", 
                      "GREATER", "GREATEREQUAL", "DBEQUAL", "NOTEQUAL", 
                      "YES", "NO", "DOTS", "EQUAL", "BSLASH", "SC", "SEA_WS", 
                      "TEXTE", "RANDINT", "KEYWORD", "TYPE", "ANSWERTYPE", 
                      "ID", "EMOJI", "JAPANESE", "NOT_CYRILLIC", "TITLE", 
                      "LANGUAGE", "AUTHOR", "EMAIL", "COMPUTEANSWER", "PRECISION", 
                      "RANGE", "STATEMENT", "HINT", "HELP", "INTEGER", "RATIONAL", 
                      "REAL", "COMPLEX", "TEXT", "MATRIX", "FUNCTION", "ANSWER", 
                      "CONDITION", "FEEDBACK", "SOLUTION" ];

var ruleNames =  [ "sentence", "chardata", "string", "integer", "rational", 
                   "real", "complex", "text", "matrix", "founction", "assignement", 
                   "expression", "relationalExpression", "callvariable", 
                   "title", "language", "author", "email", "computeanswer", 
                   "precision", "rangedeclaration", "statement", "hint", 
                   "help", "answer", "answertype", "condition", "feedback", 
                   "solution", "pre", "peri", "post", "wimsoef" ];

function LabeledWimsParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

LabeledWimsParser.prototype = Object.create(antlr4.Parser.prototype);
LabeledWimsParser.prototype.constructor = LabeledWimsParser;

Object.defineProperty(LabeledWimsParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

LabeledWimsParser.EOF = antlr4.Token.EOF;
LabeledWimsParser.T__0 = 1;
LabeledWimsParser.NUM_INT = 2;
LabeledWimsParser.NUM_REAL = 3;
LabeledWimsParser.PLUS = 4;
LabeledWimsParser.MINUS = 5;
LabeledWimsParser.STAR = 6;
LabeledWimsParser.SLASH = 7;
LabeledWimsParser.POWER = 8;
LabeledWimsParser.DOT = 9;
LabeledWimsParser.OB = 10;
LabeledWimsParser.CB = 11;
LabeledWimsParser.OP = 12;
LabeledWimsParser.CP = 13;
LabeledWimsParser.AMPERSAND = 14;
LabeledWimsParser.HASH = 15;
LabeledWimsParser.AND = 16;
LabeledWimsParser.OR = 17;
LabeledWimsParser.LESS = 18;
LabeledWimsParser.LESSEQUAL = 19;
LabeledWimsParser.GREATER = 20;
LabeledWimsParser.GREATEREQUAL = 21;
LabeledWimsParser.DBEQUAL = 22;
LabeledWimsParser.NOTEQUAL = 23;
LabeledWimsParser.YES = 24;
LabeledWimsParser.NO = 25;
LabeledWimsParser.DOTS = 26;
LabeledWimsParser.EQUAL = 27;
LabeledWimsParser.BSLASH = 28;
LabeledWimsParser.SC = 29;
LabeledWimsParser.SEA_WS = 30;
LabeledWimsParser.TEXTE = 31;
LabeledWimsParser.RANDINT = 32;
LabeledWimsParser.KEYWORD = 33;
LabeledWimsParser.TYPE = 34;
LabeledWimsParser.ANSWERTYPE = 35;
LabeledWimsParser.ID = 36;
LabeledWimsParser.EMOJI = 37;
LabeledWimsParser.JAPANESE = 38;
LabeledWimsParser.NOT_CYRILLIC = 39;
LabeledWimsParser.TITLE = 40;
LabeledWimsParser.LANGUAGE = 41;
LabeledWimsParser.AUTHOR = 42;
LabeledWimsParser.EMAIL = 43;
LabeledWimsParser.COMPUTEANSWER = 44;
LabeledWimsParser.PRECISION = 45;
LabeledWimsParser.RANGE = 46;
LabeledWimsParser.STATEMENT = 47;
LabeledWimsParser.HINT = 48;
LabeledWimsParser.HELP = 49;
LabeledWimsParser.INTEGER = 50;
LabeledWimsParser.RATIONAL = 51;
LabeledWimsParser.REAL = 52;
LabeledWimsParser.COMPLEX = 53;
LabeledWimsParser.TEXT = 54;
LabeledWimsParser.MATRIX = 55;
LabeledWimsParser.FUNCTION = 56;
LabeledWimsParser.ANSWER = 57;
LabeledWimsParser.CONDITION = 58;
LabeledWimsParser.FEEDBACK = 59;
LabeledWimsParser.SOLUTION = 60;

LabeledWimsParser.RULE_sentence = 0;
LabeledWimsParser.RULE_chardata = 1;
LabeledWimsParser.RULE_string = 2;
LabeledWimsParser.RULE_integer = 3;
LabeledWimsParser.RULE_rational = 4;
LabeledWimsParser.RULE_real = 5;
LabeledWimsParser.RULE_complex = 6;
LabeledWimsParser.RULE_text = 7;
LabeledWimsParser.RULE_matrix = 8;
LabeledWimsParser.RULE_founction = 9;
LabeledWimsParser.RULE_assignement = 10;
LabeledWimsParser.RULE_expression = 11;
LabeledWimsParser.RULE_relationalExpression = 12;
LabeledWimsParser.RULE_callvariable = 13;
LabeledWimsParser.RULE_title = 14;
LabeledWimsParser.RULE_language = 15;
LabeledWimsParser.RULE_author = 16;
LabeledWimsParser.RULE_email = 17;
LabeledWimsParser.RULE_computeanswer = 18;
LabeledWimsParser.RULE_precision = 19;
LabeledWimsParser.RULE_rangedeclaration = 20;
LabeledWimsParser.RULE_statement = 21;
LabeledWimsParser.RULE_hint = 22;
LabeledWimsParser.RULE_help = 23;
LabeledWimsParser.RULE_answer = 24;
LabeledWimsParser.RULE_answertype = 25;
LabeledWimsParser.RULE_condition = 26;
LabeledWimsParser.RULE_feedback = 27;
LabeledWimsParser.RULE_solution = 28;
LabeledWimsParser.RULE_pre = 29;
LabeledWimsParser.RULE_peri = 30;
LabeledWimsParser.RULE_post = 31;
LabeledWimsParser.RULE_wimsoef = 32;


function SentenceContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_sentence;
    return this;
}

SentenceContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
SentenceContext.prototype.constructor = SentenceContext;

SentenceContext.prototype.chardata = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ChardataContext);
    } else {
        return this.getTypedRuleContext(ChardataContext,i);
    }
};

SentenceContext.prototype.callvariable = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(CallvariableContext);
    } else {
        return this.getTypedRuleContext(CallvariableContext,i);
    }
};

SentenceContext.prototype.KEYWORD = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.KEYWORD);
    } else {
        return this.getToken(LabeledWimsParser.KEYWORD, i);
    }
};


SentenceContext.prototype.OP = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.OP);
    } else {
        return this.getToken(LabeledWimsParser.OP, i);
    }
};


SentenceContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitSentence(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.SentenceContext = SentenceContext;

LabeledWimsParser.prototype.sentence = function() {

    var localctx = new SentenceContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, LabeledWimsParser.RULE_sentence);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 67;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===LabeledWimsParser.SEA_WS || _la===LabeledWimsParser.TEXTE) {
            this.state = 66;
            this.chardata();
        }

        this.state = 79;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===LabeledWimsParser.BSLASH || _la===LabeledWimsParser.KEYWORD) {
            this.state = 72;
            this._errHandler.sync(this);
            switch(this._input.LA(1)) {
            case LabeledWimsParser.BSLASH:
                this.state = 69;
                this.callvariable();
                break;
            case LabeledWimsParser.KEYWORD:
                this.state = 70;
                this.match(LabeledWimsParser.KEYWORD);
                this.state = 71;
                this.match(LabeledWimsParser.OP);
                break;
            default:
                throw new antlr4.error.NoViableAltException(this);
            }
            this.state = 75;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            if(_la===LabeledWimsParser.SEA_WS || _la===LabeledWimsParser.TEXTE) {
                this.state = 74;
                this.chardata();
            }

            this.state = 81;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function ChardataContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_chardata;
    return this;
}

ChardataContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ChardataContext.prototype.constructor = ChardataContext;

ChardataContext.prototype.TEXTE = function() {
    return this.getToken(LabeledWimsParser.TEXTE, 0);
};

ChardataContext.prototype.SEA_WS = function() {
    return this.getToken(LabeledWimsParser.SEA_WS, 0);
};

ChardataContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitChardata(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.ChardataContext = ChardataContext;

LabeledWimsParser.prototype.chardata = function() {

    var localctx = new ChardataContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, LabeledWimsParser.RULE_chardata);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 82;
        _la = this._input.LA(1);
        if(!(_la===LabeledWimsParser.SEA_WS || _la===LabeledWimsParser.TEXTE)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function StringContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_string;
    return this;
}

StringContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
StringContext.prototype.constructor = StringContext;

StringContext.prototype.BSLASH = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.BSLASH);
    } else {
        return this.getToken(LabeledWimsParser.BSLASH, i);
    }
};


StringContext.prototype.CB = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.CB);
    } else {
        return this.getToken(LabeledWimsParser.CB, i);
    }
};


StringContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitString(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.StringContext = StringContext;

LabeledWimsParser.prototype.string = function() {

    var localctx = new StringContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, LabeledWimsParser.RULE_string);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 85; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 84;
            _la = this._input.LA(1);
            if(_la<=0 || _la===LabeledWimsParser.CB || _la===LabeledWimsParser.BSLASH) {
            this._errHandler.recoverInline(this);
            }
            else {
            	this._errHandler.reportMatch(this);
                this.consume();
            }
            this.state = 87; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LabeledWimsParser.T__0) | (1 << LabeledWimsParser.NUM_INT) | (1 << LabeledWimsParser.NUM_REAL) | (1 << LabeledWimsParser.PLUS) | (1 << LabeledWimsParser.MINUS) | (1 << LabeledWimsParser.STAR) | (1 << LabeledWimsParser.SLASH) | (1 << LabeledWimsParser.POWER) | (1 << LabeledWimsParser.DOT) | (1 << LabeledWimsParser.OB) | (1 << LabeledWimsParser.OP) | (1 << LabeledWimsParser.CP) | (1 << LabeledWimsParser.AMPERSAND) | (1 << LabeledWimsParser.HASH) | (1 << LabeledWimsParser.AND) | (1 << LabeledWimsParser.OR) | (1 << LabeledWimsParser.LESS) | (1 << LabeledWimsParser.LESSEQUAL) | (1 << LabeledWimsParser.GREATER) | (1 << LabeledWimsParser.GREATEREQUAL) | (1 << LabeledWimsParser.DBEQUAL) | (1 << LabeledWimsParser.NOTEQUAL) | (1 << LabeledWimsParser.YES) | (1 << LabeledWimsParser.NO) | (1 << LabeledWimsParser.DOTS) | (1 << LabeledWimsParser.EQUAL) | (1 << LabeledWimsParser.SC) | (1 << LabeledWimsParser.SEA_WS) | (1 << LabeledWimsParser.TEXTE))) !== 0) || ((((_la - 32)) & ~0x1f) == 0 && ((1 << (_la - 32)) & ((1 << (LabeledWimsParser.RANDINT - 32)) | (1 << (LabeledWimsParser.KEYWORD - 32)) | (1 << (LabeledWimsParser.TYPE - 32)) | (1 << (LabeledWimsParser.ANSWERTYPE - 32)) | (1 << (LabeledWimsParser.ID - 32)) | (1 << (LabeledWimsParser.EMOJI - 32)) | (1 << (LabeledWimsParser.JAPANESE - 32)) | (1 << (LabeledWimsParser.NOT_CYRILLIC - 32)) | (1 << (LabeledWimsParser.TITLE - 32)) | (1 << (LabeledWimsParser.LANGUAGE - 32)) | (1 << (LabeledWimsParser.AUTHOR - 32)) | (1 << (LabeledWimsParser.EMAIL - 32)) | (1 << (LabeledWimsParser.COMPUTEANSWER - 32)) | (1 << (LabeledWimsParser.PRECISION - 32)) | (1 << (LabeledWimsParser.RANGE - 32)) | (1 << (LabeledWimsParser.STATEMENT - 32)) | (1 << (LabeledWimsParser.HINT - 32)) | (1 << (LabeledWimsParser.HELP - 32)) | (1 << (LabeledWimsParser.INTEGER - 32)) | (1 << (LabeledWimsParser.RATIONAL - 32)) | (1 << (LabeledWimsParser.REAL - 32)) | (1 << (LabeledWimsParser.COMPLEX - 32)) | (1 << (LabeledWimsParser.TEXT - 32)) | (1 << (LabeledWimsParser.MATRIX - 32)) | (1 << (LabeledWimsParser.FUNCTION - 32)) | (1 << (LabeledWimsParser.ANSWER - 32)) | (1 << (LabeledWimsParser.CONDITION - 32)) | (1 << (LabeledWimsParser.FEEDBACK - 32)) | (1 << (LabeledWimsParser.SOLUTION - 32)))) !== 0));
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function IntegerContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_integer;
    return this;
}

IntegerContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
IntegerContext.prototype.constructor = IntegerContext;


 
IntegerContext.prototype.copyFrom = function(ctx) {
    antlr4.ParserRuleContext.prototype.copyFrom.call(this, ctx);
};


function PrintIntContext(parser, ctx) {
	IntegerContext.call(this, parser);
    IntegerContext.prototype.copyFrom.call(this, ctx);
    return this;
}

PrintIntContext.prototype = Object.create(IntegerContext.prototype);
PrintIntContext.prototype.constructor = PrintIntContext;

LabeledWimsParser.PrintIntContext = PrintIntContext;

PrintIntContext.prototype.INTEGER = function() {
    return this.getToken(LabeledWimsParser.INTEGER, 0);
};

PrintIntContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

PrintIntContext.prototype.assignement = function() {
    return this.getTypedRuleContext(AssignementContext,0);
};

PrintIntContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};
PrintIntContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitPrintInt(this);
    } else {
        return visitor.visitChildren(this);
    }
};



LabeledWimsParser.IntegerContext = IntegerContext;

LabeledWimsParser.prototype.integer = function() {

    var localctx = new IntegerContext(this, this._ctx, this.state);
    this.enterRule(localctx, 6, LabeledWimsParser.RULE_integer);
    try {
        localctx = new PrintIntContext(this, localctx);
        this.enterOuterAlt(localctx, 1);
        this.state = 89;
        this.match(LabeledWimsParser.INTEGER);
        this.state = 90;
        this.match(LabeledWimsParser.OB);
        this.state = 91;
        this.assignement();
        this.state = 92;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function RationalContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_rational;
    return this;
}

RationalContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RationalContext.prototype.constructor = RationalContext;

RationalContext.prototype.RATIONAL = function() {
    return this.getToken(LabeledWimsParser.RATIONAL, 0);
};

RationalContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

RationalContext.prototype.assignement = function() {
    return this.getTypedRuleContext(AssignementContext,0);
};

RationalContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

RationalContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitRational(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.RationalContext = RationalContext;

LabeledWimsParser.prototype.rational = function() {

    var localctx = new RationalContext(this, this._ctx, this.state);
    this.enterRule(localctx, 8, LabeledWimsParser.RULE_rational);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 94;
        this.match(LabeledWimsParser.RATIONAL);
        this.state = 95;
        this.match(LabeledWimsParser.OB);
        this.state = 96;
        this.assignement();
        this.state = 97;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function RealContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_real;
    return this;
}

RealContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RealContext.prototype.constructor = RealContext;

RealContext.prototype.REAL = function() {
    return this.getToken(LabeledWimsParser.REAL, 0);
};

RealContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

RealContext.prototype.assignement = function() {
    return this.getTypedRuleContext(AssignementContext,0);
};

RealContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

RealContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitReal(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.RealContext = RealContext;

LabeledWimsParser.prototype.real = function() {

    var localctx = new RealContext(this, this._ctx, this.state);
    this.enterRule(localctx, 10, LabeledWimsParser.RULE_real);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 99;
        this.match(LabeledWimsParser.REAL);
        this.state = 100;
        this.match(LabeledWimsParser.OB);
        this.state = 101;
        this.assignement();
        this.state = 102;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function ComplexContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_complex;
    return this;
}

ComplexContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ComplexContext.prototype.constructor = ComplexContext;

ComplexContext.prototype.COMPLEX = function() {
    return this.getToken(LabeledWimsParser.COMPLEX, 0);
};

ComplexContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

ComplexContext.prototype.assignement = function() {
    return this.getTypedRuleContext(AssignementContext,0);
};

ComplexContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

ComplexContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitComplex(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.ComplexContext = ComplexContext;

LabeledWimsParser.prototype.complex = function() {

    var localctx = new ComplexContext(this, this._ctx, this.state);
    this.enterRule(localctx, 12, LabeledWimsParser.RULE_complex);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 104;
        this.match(LabeledWimsParser.COMPLEX);
        this.state = 105;
        this.match(LabeledWimsParser.OB);
        this.state = 106;
        this.assignement();
        this.state = 107;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function TextContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_text;
    return this;
}

TextContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
TextContext.prototype.constructor = TextContext;

TextContext.prototype.TEXT = function() {
    return this.getToken(LabeledWimsParser.TEXT, 0);
};

TextContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

TextContext.prototype.assignement = function() {
    return this.getTypedRuleContext(AssignementContext,0);
};

TextContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

TextContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitText(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.TextContext = TextContext;

LabeledWimsParser.prototype.text = function() {

    var localctx = new TextContext(this, this._ctx, this.state);
    this.enterRule(localctx, 14, LabeledWimsParser.RULE_text);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 109;
        this.match(LabeledWimsParser.TEXT);
        this.state = 110;
        this.match(LabeledWimsParser.OB);
        this.state = 111;
        this.assignement();
        this.state = 112;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function MatrixContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_matrix;
    return this;
}

MatrixContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
MatrixContext.prototype.constructor = MatrixContext;

MatrixContext.prototype.MATRIX = function() {
    return this.getToken(LabeledWimsParser.MATRIX, 0);
};

MatrixContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

MatrixContext.prototype.assignement = function() {
    return this.getTypedRuleContext(AssignementContext,0);
};

MatrixContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

MatrixContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitMatrix(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.MatrixContext = MatrixContext;

LabeledWimsParser.prototype.matrix = function() {

    var localctx = new MatrixContext(this, this._ctx, this.state);
    this.enterRule(localctx, 16, LabeledWimsParser.RULE_matrix);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 114;
        this.match(LabeledWimsParser.MATRIX);
        this.state = 115;
        this.match(LabeledWimsParser.OB);
        this.state = 116;
        this.assignement();
        this.state = 117;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function FounctionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_founction;
    return this;
}

FounctionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FounctionContext.prototype.constructor = FounctionContext;

FounctionContext.prototype.FUNCTION = function() {
    return this.getToken(LabeledWimsParser.FUNCTION, 0);
};

FounctionContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

FounctionContext.prototype.assignement = function() {
    return this.getTypedRuleContext(AssignementContext,0);
};

FounctionContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

FounctionContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitFounction(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.FounctionContext = FounctionContext;

LabeledWimsParser.prototype.founction = function() {

    var localctx = new FounctionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 18, LabeledWimsParser.RULE_founction);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 119;
        this.match(LabeledWimsParser.FUNCTION);
        this.state = 120;
        this.match(LabeledWimsParser.OB);
        this.state = 121;
        this.assignement();
        this.state = 122;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function AssignementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_assignement;
    return this;
}

AssignementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
AssignementContext.prototype.constructor = AssignementContext;


 
AssignementContext.prototype.copyFrom = function(ctx) {
    antlr4.ParserRuleContext.prototype.copyFrom.call(this, ctx);
};


function AssignContext(parser, ctx) {
	AssignementContext.call(this, parser);
    AssignementContext.prototype.copyFrom.call(this, ctx);
    return this;
}

AssignContext.prototype = Object.create(AssignementContext.prototype);
AssignContext.prototype.constructor = AssignContext;

LabeledWimsParser.AssignContext = AssignContext;

AssignContext.prototype.ID = function() {
    return this.getToken(LabeledWimsParser.ID, 0);
};

AssignContext.prototype.EQUAL = function() {
    return this.getToken(LabeledWimsParser.EQUAL, 0);
};

AssignContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};
AssignContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitAssign(this);
    } else {
        return visitor.visitChildren(this);
    }
};



LabeledWimsParser.AssignementContext = AssignementContext;

LabeledWimsParser.prototype.assignement = function() {

    var localctx = new AssignementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 20, LabeledWimsParser.RULE_assignement);
    try {
        localctx = new AssignContext(this, localctx);
        this.enterOuterAlt(localctx, 1);
        this.state = 124;
        this.match(LabeledWimsParser.ID);
        this.state = 125;
        this.match(LabeledWimsParser.EQUAL);
        this.state = 126;
        this.expression(0);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function ExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_expression;
    return this;
}

ExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ExpressionContext.prototype.constructor = ExpressionContext;


 
ExpressionContext.prototype.copyFrom = function(ctx) {
    antlr4.ParserRuleContext.prototype.copyFrom.call(this, ctx);
};

function NumIntContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

NumIntContext.prototype = Object.create(ExpressionContext.prototype);
NumIntContext.prototype.constructor = NumIntContext;

LabeledWimsParser.NumIntContext = NumIntContext;

NumIntContext.prototype.NUM_INT = function() {
    return this.getToken(LabeledWimsParser.NUM_INT, 0);
};
NumIntContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitNumInt(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function ParensContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

ParensContext.prototype = Object.create(ExpressionContext.prototype);
ParensContext.prototype.constructor = ParensContext;

LabeledWimsParser.ParensContext = ParensContext;

ParensContext.prototype.OP = function() {
    return this.getToken(LabeledWimsParser.OP, 0);
};

ParensContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

ParensContext.prototype.CP = function() {
    return this.getToken(LabeledWimsParser.CP, 0);
};
ParensContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitParens(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function KeyRandintContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

KeyRandintContext.prototype = Object.create(ExpressionContext.prototype);
KeyRandintContext.prototype.constructor = KeyRandintContext;

LabeledWimsParser.KeyRandintContext = KeyRandintContext;

KeyRandintContext.prototype.RANDINT = function() {
    return this.getToken(LabeledWimsParser.RANDINT, 0);
};

KeyRandintContext.prototype.OP = function() {
    return this.getToken(LabeledWimsParser.OP, 0);
};

KeyRandintContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

KeyRandintContext.prototype.DOTS = function() {
    return this.getToken(LabeledWimsParser.DOTS, 0);
};

KeyRandintContext.prototype.CP = function() {
    return this.getToken(LabeledWimsParser.CP, 0);
};
KeyRandintContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitKeyRandint(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function AddSubContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    this.op = null; // Token;
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

AddSubContext.prototype = Object.create(ExpressionContext.prototype);
AddSubContext.prototype.constructor = AddSubContext;

LabeledWimsParser.AddSubContext = AddSubContext;

AddSubContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

AddSubContext.prototype.PLUS = function() {
    return this.getToken(LabeledWimsParser.PLUS, 0);
};

AddSubContext.prototype.MINUS = function() {
    return this.getToken(LabeledWimsParser.MINUS, 0);
};
AddSubContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitAddSub(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function IdContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

IdContext.prototype = Object.create(ExpressionContext.prototype);
IdContext.prototype.constructor = IdContext;

LabeledWimsParser.IdContext = IdContext;

IdContext.prototype.callvariable = function() {
    return this.getTypedRuleContext(CallvariableContext,0);
};
IdContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitId(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function PowerContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

PowerContext.prototype = Object.create(ExpressionContext.prototype);
PowerContext.prototype.constructor = PowerContext;

LabeledWimsParser.PowerContext = PowerContext;

PowerContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

PowerContext.prototype.POWER = function() {
    return this.getToken(LabeledWimsParser.POWER, 0);
};
PowerContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitPower(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function PluMinContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

PluMinContext.prototype = Object.create(ExpressionContext.prototype);
PluMinContext.prototype.constructor = PluMinContext;

LabeledWimsParser.PluMinContext = PluMinContext;

PluMinContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

PluMinContext.prototype.PLUS = function() {
    return this.getToken(LabeledWimsParser.PLUS, 0);
};

PluMinContext.prototype.MINUS = function() {
    return this.getToken(LabeledWimsParser.MINUS, 0);
};
PluMinContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitPluMin(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function MulDivContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    this.op = null; // Token;
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

MulDivContext.prototype = Object.create(ExpressionContext.prototype);
MulDivContext.prototype.constructor = MulDivContext;

LabeledWimsParser.MulDivContext = MulDivContext;

MulDivContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

MulDivContext.prototype.STAR = function() {
    return this.getToken(LabeledWimsParser.STAR, 0);
};

MulDivContext.prototype.SLASH = function() {
    return this.getToken(LabeledWimsParser.SLASH, 0);
};
MulDivContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitMulDiv(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function NumRealContext(parser, ctx) {
	ExpressionContext.call(this, parser);
    ExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

NumRealContext.prototype = Object.create(ExpressionContext.prototype);
NumRealContext.prototype.constructor = NumRealContext;

LabeledWimsParser.NumRealContext = NumRealContext;

NumRealContext.prototype.NUM_REAL = function() {
    return this.getToken(LabeledWimsParser.NUM_REAL, 0);
};
NumRealContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitNumReal(this);
    } else {
        return visitor.visitChildren(this);
    }
};



LabeledWimsParser.prototype.expression = function(_p) {
	if(_p===undefined) {
	    _p = 0;
	}
    var _parentctx = this._ctx;
    var _parentState = this.state;
    var localctx = new ExpressionContext(this, this._ctx, _parentState);
    var _prevctx = localctx;
    var _startState = 22;
    this.enterRecursionRule(localctx, 22, LabeledWimsParser.RULE_expression, _p);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 145;
        this._errHandler.sync(this);
        switch(this._input.LA(1)) {
        case LabeledWimsParser.PLUS:
        case LabeledWimsParser.MINUS:
            localctx = new PluMinContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;

            this.state = 129;
            _la = this._input.LA(1);
            if(!(_la===LabeledWimsParser.PLUS || _la===LabeledWimsParser.MINUS)) {
            this._errHandler.recoverInline(this);
            }
            else {
            	this._errHandler.reportMatch(this);
                this.consume();
            }
            this.state = 130;
            this.expression(6);
            break;
        case LabeledWimsParser.OP:
            localctx = new ParensContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;
            this.state = 131;
            this.match(LabeledWimsParser.OP);
            this.state = 132;
            this.expression(0);
            this.state = 133;
            this.match(LabeledWimsParser.CP);
            break;
        case LabeledWimsParser.RANDINT:
            localctx = new KeyRandintContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;
            this.state = 135;
            this.match(LabeledWimsParser.RANDINT);
            this.state = 136;
            this.match(LabeledWimsParser.OP);
            this.state = 137;
            this.expression(0);
            this.state = 138;
            this.match(LabeledWimsParser.DOTS);
            this.state = 139;
            this.expression(0);
            this.state = 140;
            this.match(LabeledWimsParser.CP);
            break;
        case LabeledWimsParser.BSLASH:
            localctx = new IdContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;
            this.state = 142;
            this.callvariable();
            break;
        case LabeledWimsParser.NUM_INT:
            localctx = new NumIntContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;
            this.state = 143;
            this.match(LabeledWimsParser.NUM_INT);
            break;
        case LabeledWimsParser.NUM_REAL:
            localctx = new NumRealContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;
            this.state = 144;
            this.match(LabeledWimsParser.NUM_REAL);
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
        this._ctx.stop = this._input.LT(-1);
        this.state = 158;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,7,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                if(this._parseListeners!==null) {
                    this.triggerExitRuleEvent();
                }
                _prevctx = localctx;
                this.state = 156;
                this._errHandler.sync(this);
                var la_ = this._interp.adaptivePredict(this._input,6,this._ctx);
                switch(la_) {
                case 1:
                    localctx = new AddSubContext(this, new ExpressionContext(this, _parentctx, _parentState));
                    this.pushNewRecursionContext(localctx, _startState, LabeledWimsParser.RULE_expression);
                    this.state = 147;
                    if (!( this.precpred(this._ctx, 9))) {
                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 9)");
                    }
                    this.state = 148;
                    localctx.op = this._input.LT(1);
                    _la = this._input.LA(1);
                    if(!(_la===LabeledWimsParser.PLUS || _la===LabeledWimsParser.MINUS)) {
                        localctx.op = this._errHandler.recoverInline(this);
                    }
                    else {
                    	this._errHandler.reportMatch(this);
                        this.consume();
                    }
                    this.state = 149;
                    this.expression(10);
                    break;

                case 2:
                    localctx = new MulDivContext(this, new ExpressionContext(this, _parentctx, _parentState));
                    this.pushNewRecursionContext(localctx, _startState, LabeledWimsParser.RULE_expression);
                    this.state = 150;
                    if (!( this.precpred(this._ctx, 8))) {
                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 8)");
                    }
                    this.state = 151;
                    localctx.op = this._input.LT(1);
                    _la = this._input.LA(1);
                    if(!(_la===LabeledWimsParser.STAR || _la===LabeledWimsParser.SLASH)) {
                        localctx.op = this._errHandler.recoverInline(this);
                    }
                    else {
                    	this._errHandler.reportMatch(this);
                        this.consume();
                    }
                    this.state = 152;
                    this.expression(9);
                    break;

                case 3:
                    localctx = new PowerContext(this, new ExpressionContext(this, _parentctx, _parentState));
                    this.pushNewRecursionContext(localctx, _startState, LabeledWimsParser.RULE_expression);
                    this.state = 153;
                    if (!( this.precpred(this._ctx, 7))) {
                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 7)");
                    }
                    this.state = 154;
                    this.match(LabeledWimsParser.POWER);
                    this.state = 155;
                    this.expression(8);
                    break;

                } 
            }
            this.state = 160;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,7,this._ctx);
        }

    } catch( error) {
        if(error instanceof antlr4.error.RecognitionException) {
	        localctx.exception = error;
	        this._errHandler.reportError(this, error);
	        this._errHandler.recover(this, error);
	    } else {
	    	throw error;
	    }
    } finally {
        this.unrollRecursionContexts(_parentctx)
    }
    return localctx;
};


function RelationalExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_relationalExpression;
    return this;
}

RelationalExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RelationalExpressionContext.prototype.constructor = RelationalExpressionContext;


 
RelationalExpressionContext.prototype.copyFrom = function(ctx) {
    antlr4.ParserRuleContext.prototype.copyFrom.call(this, ctx);
};

function EqualNotEqualContext(parser, ctx) {
	RelationalExpressionContext.call(this, parser);
    this.rel = null; // Token;
    RelationalExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

EqualNotEqualContext.prototype = Object.create(RelationalExpressionContext.prototype);
EqualNotEqualContext.prototype.constructor = EqualNotEqualContext;

LabeledWimsParser.EqualNotEqualContext = EqualNotEqualContext;

EqualNotEqualContext.prototype.relationalExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(RelationalExpressionContext);
    } else {
        return this.getTypedRuleContext(RelationalExpressionContext,i);
    }
};

EqualNotEqualContext.prototype.DBEQUAL = function() {
    return this.getToken(LabeledWimsParser.DBEQUAL, 0);
};

EqualNotEqualContext.prototype.NOTEQUAL = function() {
    return this.getToken(LabeledWimsParser.NOTEQUAL, 0);
};
EqualNotEqualContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitEqualNotEqual(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function OrAndContext(parser, ctx) {
	RelationalExpressionContext.call(this, parser);
    this.rel = null; // Token;
    RelationalExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

OrAndContext.prototype = Object.create(RelationalExpressionContext.prototype);
OrAndContext.prototype.constructor = OrAndContext;

LabeledWimsParser.OrAndContext = OrAndContext;

OrAndContext.prototype.relationalExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(RelationalExpressionContext);
    } else {
        return this.getTypedRuleContext(RelationalExpressionContext,i);
    }
};

OrAndContext.prototype.OR = function() {
    return this.getToken(LabeledWimsParser.OR, 0);
};

OrAndContext.prototype.AND = function() {
    return this.getToken(LabeledWimsParser.AND, 0);
};
OrAndContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitOrAnd(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function ParensREContext(parser, ctx) {
	RelationalExpressionContext.call(this, parser);
    RelationalExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

ParensREContext.prototype = Object.create(RelationalExpressionContext.prototype);
ParensREContext.prototype.constructor = ParensREContext;

LabeledWimsParser.ParensREContext = ParensREContext;

ParensREContext.prototype.OP = function() {
    return this.getToken(LabeledWimsParser.OP, 0);
};

ParensREContext.prototype.relationalExpression = function() {
    return this.getTypedRuleContext(RelationalExpressionContext,0);
};

ParensREContext.prototype.CP = function() {
    return this.getToken(LabeledWimsParser.CP, 0);
};
ParensREContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitParensRE(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function LessGreaterContext(parser, ctx) {
	RelationalExpressionContext.call(this, parser);
    this.rel = null; // Token;
    RelationalExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

LessGreaterContext.prototype = Object.create(RelationalExpressionContext.prototype);
LessGreaterContext.prototype.constructor = LessGreaterContext;

LabeledWimsParser.LessGreaterContext = LessGreaterContext;

LessGreaterContext.prototype.relationalExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(RelationalExpressionContext);
    } else {
        return this.getTypedRuleContext(RelationalExpressionContext,i);
    }
};

LessGreaterContext.prototype.LESS = function() {
    return this.getToken(LabeledWimsParser.LESS, 0);
};

LessGreaterContext.prototype.LESSEQUAL = function() {
    return this.getToken(LabeledWimsParser.LESSEQUAL, 0);
};

LessGreaterContext.prototype.GREATER = function() {
    return this.getToken(LabeledWimsParser.GREATER, 0);
};

LessGreaterContext.prototype.GREATEREQUAL = function() {
    return this.getToken(LabeledWimsParser.GREATEREQUAL, 0);
};
LessGreaterContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitLessGreater(this);
    } else {
        return visitor.visitChildren(this);
    }
};


function RelExpContext(parser, ctx) {
	RelationalExpressionContext.call(this, parser);
    RelationalExpressionContext.prototype.copyFrom.call(this, ctx);
    return this;
}

RelExpContext.prototype = Object.create(RelationalExpressionContext.prototype);
RelExpContext.prototype.constructor = RelExpContext;

LabeledWimsParser.RelExpContext = RelExpContext;

RelExpContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};
RelExpContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitRelExp(this);
    } else {
        return visitor.visitChildren(this);
    }
};



LabeledWimsParser.prototype.relationalExpression = function(_p) {
	if(_p===undefined) {
	    _p = 0;
	}
    var _parentctx = this._ctx;
    var _parentState = this.state;
    var localctx = new RelationalExpressionContext(this, this._ctx, _parentState);
    var _prevctx = localctx;
    var _startState = 24;
    this.enterRecursionRule(localctx, 24, LabeledWimsParser.RULE_relationalExpression, _p);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 167;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,8,this._ctx);
        switch(la_) {
        case 1:
            localctx = new ParensREContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;

            this.state = 162;
            this.match(LabeledWimsParser.OP);
            this.state = 163;
            this.relationalExpression(0);
            this.state = 164;
            this.match(LabeledWimsParser.CP);
            break;

        case 2:
            localctx = new RelExpContext(this, localctx);
            this._ctx = localctx;
            _prevctx = localctx;
            this.state = 166;
            this.expression(0);
            break;

        }
        this._ctx.stop = this._input.LT(-1);
        this.state = 180;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,10,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                if(this._parseListeners!==null) {
                    this.triggerExitRuleEvent();
                }
                _prevctx = localctx;
                this.state = 178;
                this._errHandler.sync(this);
                var la_ = this._interp.adaptivePredict(this._input,9,this._ctx);
                switch(la_) {
                case 1:
                    localctx = new OrAndContext(this, new RelationalExpressionContext(this, _parentctx, _parentState));
                    this.pushNewRecursionContext(localctx, _startState, LabeledWimsParser.RULE_relationalExpression);
                    this.state = 169;
                    if (!( this.precpred(this._ctx, 5))) {
                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 5)");
                    }
                    this.state = 170;
                    localctx.rel = this._input.LT(1);
                    _la = this._input.LA(1);
                    if(!(_la===LabeledWimsParser.AND || _la===LabeledWimsParser.OR)) {
                        localctx.rel = this._errHandler.recoverInline(this);
                    }
                    else {
                    	this._errHandler.reportMatch(this);
                        this.consume();
                    }
                    this.state = 171;
                    this.relationalExpression(6);
                    break;

                case 2:
                    localctx = new LessGreaterContext(this, new RelationalExpressionContext(this, _parentctx, _parentState));
                    this.pushNewRecursionContext(localctx, _startState, LabeledWimsParser.RULE_relationalExpression);
                    this.state = 172;
                    if (!( this.precpred(this._ctx, 4))) {
                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 4)");
                    }
                    this.state = 173;
                    localctx.rel = this._input.LT(1);
                    _la = this._input.LA(1);
                    if(!((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LabeledWimsParser.LESS) | (1 << LabeledWimsParser.LESSEQUAL) | (1 << LabeledWimsParser.GREATER) | (1 << LabeledWimsParser.GREATEREQUAL))) !== 0))) {
                        localctx.rel = this._errHandler.recoverInline(this);
                    }
                    else {
                    	this._errHandler.reportMatch(this);
                        this.consume();
                    }
                    this.state = 174;
                    this.relationalExpression(5);
                    break;

                case 3:
                    localctx = new EqualNotEqualContext(this, new RelationalExpressionContext(this, _parentctx, _parentState));
                    this.pushNewRecursionContext(localctx, _startState, LabeledWimsParser.RULE_relationalExpression);
                    this.state = 175;
                    if (!( this.precpred(this._ctx, 3))) {
                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 3)");
                    }
                    this.state = 176;
                    localctx.rel = this._input.LT(1);
                    _la = this._input.LA(1);
                    if(!(_la===LabeledWimsParser.DBEQUAL || _la===LabeledWimsParser.NOTEQUAL)) {
                        localctx.rel = this._errHandler.recoverInline(this);
                    }
                    else {
                    	this._errHandler.reportMatch(this);
                        this.consume();
                    }
                    this.state = 177;
                    this.relationalExpression(4);
                    break;

                } 
            }
            this.state = 182;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,10,this._ctx);
        }

    } catch( error) {
        if(error instanceof antlr4.error.RecognitionException) {
	        localctx.exception = error;
	        this._errHandler.reportError(this, error);
	        this._errHandler.recover(this, error);
	    } else {
	    	throw error;
	    }
    } finally {
        this.unrollRecursionContexts(_parentctx)
    }
    return localctx;
};


function CallvariableContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_callvariable;
    return this;
}

CallvariableContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CallvariableContext.prototype.constructor = CallvariableContext;

CallvariableContext.prototype.BSLASH = function() {
    return this.getToken(LabeledWimsParser.BSLASH, 0);
};

CallvariableContext.prototype.ID = function() {
    return this.getToken(LabeledWimsParser.ID, 0);
};

CallvariableContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitCallvariable(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.CallvariableContext = CallvariableContext;

LabeledWimsParser.prototype.callvariable = function() {

    var localctx = new CallvariableContext(this, this._ctx, this.state);
    this.enterRule(localctx, 26, LabeledWimsParser.RULE_callvariable);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 183;
        this.match(LabeledWimsParser.BSLASH);
        this.state = 184;
        this.match(LabeledWimsParser.ID);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function TitleContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_title;
    return this;
}

TitleContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
TitleContext.prototype.constructor = TitleContext;

TitleContext.prototype.TITLE = function() {
    return this.getToken(LabeledWimsParser.TITLE, 0);
};

TitleContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

TitleContext.prototype.string = function() {
    return this.getTypedRuleContext(StringContext,0);
};

TitleContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

TitleContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitTitle(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.TitleContext = TitleContext;

LabeledWimsParser.prototype.title = function() {

    var localctx = new TitleContext(this, this._ctx, this.state);
    this.enterRule(localctx, 28, LabeledWimsParser.RULE_title);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 186;
        this.match(LabeledWimsParser.TITLE);
        this.state = 187;
        this.match(LabeledWimsParser.OB);
        this.state = 188;
        this.string();
        this.state = 189;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function LanguageContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_language;
    return this;
}

LanguageContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LanguageContext.prototype.constructor = LanguageContext;

LanguageContext.prototype.LANGUAGE = function() {
    return this.getToken(LabeledWimsParser.LANGUAGE, 0);
};

LanguageContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

LanguageContext.prototype.string = function() {
    return this.getTypedRuleContext(StringContext,0);
};

LanguageContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

LanguageContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitLanguage(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.LanguageContext = LanguageContext;

LabeledWimsParser.prototype.language = function() {

    var localctx = new LanguageContext(this, this._ctx, this.state);
    this.enterRule(localctx, 30, LabeledWimsParser.RULE_language);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 191;
        this.match(LabeledWimsParser.LANGUAGE);
        this.state = 192;
        this.match(LabeledWimsParser.OB);
        this.state = 193;
        this.string();
        this.state = 194;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function AuthorContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_author;
    return this;
}

AuthorContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
AuthorContext.prototype.constructor = AuthorContext;

AuthorContext.prototype.AUTHOR = function() {
    return this.getToken(LabeledWimsParser.AUTHOR, 0);
};

AuthorContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

AuthorContext.prototype.string = function() {
    return this.getTypedRuleContext(StringContext,0);
};

AuthorContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

AuthorContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitAuthor(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.AuthorContext = AuthorContext;

LabeledWimsParser.prototype.author = function() {

    var localctx = new AuthorContext(this, this._ctx, this.state);
    this.enterRule(localctx, 32, LabeledWimsParser.RULE_author);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 196;
        this.match(LabeledWimsParser.AUTHOR);
        this.state = 197;
        this.match(LabeledWimsParser.OB);
        this.state = 198;
        this.string();
        this.state = 199;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function EmailContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_email;
    return this;
}

EmailContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
EmailContext.prototype.constructor = EmailContext;

EmailContext.prototype.EMAIL = function() {
    return this.getToken(LabeledWimsParser.EMAIL, 0);
};

EmailContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

EmailContext.prototype.string = function() {
    return this.getTypedRuleContext(StringContext,0);
};

EmailContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

EmailContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitEmail(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.EmailContext = EmailContext;

LabeledWimsParser.prototype.email = function() {

    var localctx = new EmailContext(this, this._ctx, this.state);
    this.enterRule(localctx, 34, LabeledWimsParser.RULE_email);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 201;
        this.match(LabeledWimsParser.EMAIL);
        this.state = 202;
        this.match(LabeledWimsParser.OB);
        this.state = 203;
        this.string();
        this.state = 204;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function ComputeanswerContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_computeanswer;
    return this;
}

ComputeanswerContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ComputeanswerContext.prototype.constructor = ComputeanswerContext;

ComputeanswerContext.prototype.COMPUTEANSWER = function() {
    return this.getToken(LabeledWimsParser.COMPUTEANSWER, 0);
};

ComputeanswerContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

ComputeanswerContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

ComputeanswerContext.prototype.YES = function() {
    return this.getToken(LabeledWimsParser.YES, 0);
};

ComputeanswerContext.prototype.NO = function() {
    return this.getToken(LabeledWimsParser.NO, 0);
};

ComputeanswerContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitComputeanswer(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.ComputeanswerContext = ComputeanswerContext;

LabeledWimsParser.prototype.computeanswer = function() {

    var localctx = new ComputeanswerContext(this, this._ctx, this.state);
    this.enterRule(localctx, 36, LabeledWimsParser.RULE_computeanswer);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 206;
        this.match(LabeledWimsParser.COMPUTEANSWER);
        this.state = 207;
        this.match(LabeledWimsParser.OB);
        this.state = 208;
        _la = this._input.LA(1);
        if(!(_la===LabeledWimsParser.YES || _la===LabeledWimsParser.NO)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 209;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function PrecisionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_precision;
    return this;
}

PrecisionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PrecisionContext.prototype.constructor = PrecisionContext;

PrecisionContext.prototype.PRECISION = function() {
    return this.getToken(LabeledWimsParser.PRECISION, 0);
};

PrecisionContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

PrecisionContext.prototype.NUM_INT = function() {
    return this.getToken(LabeledWimsParser.NUM_INT, 0);
};

PrecisionContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

PrecisionContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitPrecision(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.PrecisionContext = PrecisionContext;

LabeledWimsParser.prototype.precision = function() {

    var localctx = new PrecisionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 38, LabeledWimsParser.RULE_precision);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 211;
        this.match(LabeledWimsParser.PRECISION);
        this.state = 212;
        this.match(LabeledWimsParser.OB);
        this.state = 213;
        this.match(LabeledWimsParser.NUM_INT);
        this.state = 214;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function RangedeclarationContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_rangedeclaration;
    return this;
}

RangedeclarationContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RangedeclarationContext.prototype.constructor = RangedeclarationContext;

RangedeclarationContext.prototype.RANGE = function() {
    return this.getToken(LabeledWimsParser.RANGE, 0);
};

RangedeclarationContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

RangedeclarationContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

RangedeclarationContext.prototype.DOTS = function() {
    return this.getToken(LabeledWimsParser.DOTS, 0);
};

RangedeclarationContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

RangedeclarationContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitRangedeclaration(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.RangedeclarationContext = RangedeclarationContext;

LabeledWimsParser.prototype.rangedeclaration = function() {

    var localctx = new RangedeclarationContext(this, this._ctx, this.state);
    this.enterRule(localctx, 40, LabeledWimsParser.RULE_rangedeclaration);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 216;
        this.match(LabeledWimsParser.RANGE);
        this.state = 217;
        this.match(LabeledWimsParser.OB);
        this.state = 218;
        this.expression(0);
        this.state = 219;
        this.match(LabeledWimsParser.DOTS);
        this.state = 220;
        this.expression(0);
        this.state = 221;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function StatementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_statement;
    return this;
}

StatementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
StatementContext.prototype.constructor = StatementContext;


 
StatementContext.prototype.copyFrom = function(ctx) {
    antlr4.ParserRuleContext.prototype.copyFrom.call(this, ctx);
};


function StatementExerciseContext(parser, ctx) {
	StatementContext.call(this, parser);
    StatementContext.prototype.copyFrom.call(this, ctx);
    return this;
}

StatementExerciseContext.prototype = Object.create(StatementContext.prototype);
StatementExerciseContext.prototype.constructor = StatementExerciseContext;

LabeledWimsParser.StatementExerciseContext = StatementExerciseContext;

StatementExerciseContext.prototype.STATEMENT = function() {
    return this.getToken(LabeledWimsParser.STATEMENT, 0);
};

StatementExerciseContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

StatementExerciseContext.prototype.sentence = function() {
    return this.getTypedRuleContext(SentenceContext,0);
};

StatementExerciseContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};
StatementExerciseContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitStatementExercise(this);
    } else {
        return visitor.visitChildren(this);
    }
};



LabeledWimsParser.StatementContext = StatementContext;

LabeledWimsParser.prototype.statement = function() {

    var localctx = new StatementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 42, LabeledWimsParser.RULE_statement);
    try {
        localctx = new StatementExerciseContext(this, localctx);
        this.enterOuterAlt(localctx, 1);
        this.state = 223;
        this.match(LabeledWimsParser.STATEMENT);
        this.state = 224;
        this.match(LabeledWimsParser.OB);
        this.state = 225;
        this.sentence();
        this.state = 226;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function HintContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_hint;
    return this;
}

HintContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
HintContext.prototype.constructor = HintContext;

HintContext.prototype.HINT = function() {
    return this.getToken(LabeledWimsParser.HINT, 0);
};

HintContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

HintContext.prototype.sentence = function() {
    return this.getTypedRuleContext(SentenceContext,0);
};

HintContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

HintContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitHint(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.HintContext = HintContext;

LabeledWimsParser.prototype.hint = function() {

    var localctx = new HintContext(this, this._ctx, this.state);
    this.enterRule(localctx, 44, LabeledWimsParser.RULE_hint);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 228;
        this.match(LabeledWimsParser.HINT);
        this.state = 229;
        this.match(LabeledWimsParser.OB);
        this.state = 230;
        this.sentence();
        this.state = 231;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function HelpContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_help;
    return this;
}

HelpContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
HelpContext.prototype.constructor = HelpContext;

HelpContext.prototype.HELP = function() {
    return this.getToken(LabeledWimsParser.HELP, 0);
};

HelpContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

HelpContext.prototype.sentence = function() {
    return this.getTypedRuleContext(SentenceContext,0);
};

HelpContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

HelpContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitHelp(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.HelpContext = HelpContext;

LabeledWimsParser.prototype.help = function() {

    var localctx = new HelpContext(this, this._ctx, this.state);
    this.enterRule(localctx, 46, LabeledWimsParser.RULE_help);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 233;
        this.match(LabeledWimsParser.HELP);
        this.state = 234;
        this.match(LabeledWimsParser.OB);
        this.state = 235;
        this.sentence();
        this.state = 236;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function AnswerContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_answer;
    return this;
}

AnswerContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
AnswerContext.prototype.constructor = AnswerContext;

AnswerContext.prototype.ANSWER = function() {
    return this.getToken(LabeledWimsParser.ANSWER, 0);
};

AnswerContext.prototype.OB = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.OB);
    } else {
        return this.getToken(LabeledWimsParser.OB, i);
    }
};


AnswerContext.prototype.sentence = function() {
    return this.getTypedRuleContext(SentenceContext,0);
};

AnswerContext.prototype.CB = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.CB);
    } else {
        return this.getToken(LabeledWimsParser.CB, i);
    }
};


AnswerContext.prototype.callvariable = function() {
    return this.getTypedRuleContext(CallvariableContext,0);
};

AnswerContext.prototype.answertype = function() {
    return this.getTypedRuleContext(AnswertypeContext,0);
};

AnswerContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitAnswer(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.AnswerContext = AnswerContext;

LabeledWimsParser.prototype.answer = function() {

    var localctx = new AnswerContext(this, this._ctx, this.state);
    this.enterRule(localctx, 48, LabeledWimsParser.RULE_answer);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 238;
        this.match(LabeledWimsParser.ANSWER);
        this.state = 239;
        this.match(LabeledWimsParser.OB);
        this.state = 240;
        this.sentence();
        this.state = 241;
        this.match(LabeledWimsParser.CB);
        this.state = 242;
        this.match(LabeledWimsParser.OB);
        this.state = 243;
        this.callvariable();
        this.state = 244;
        this.match(LabeledWimsParser.CB);
        this.state = 245;
        this.answertype();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function AnswertypeContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_answertype;
    return this;
}

AnswertypeContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
AnswertypeContext.prototype.constructor = AnswertypeContext;

AnswertypeContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

AnswertypeContext.prototype.TYPE = function() {
    return this.getToken(LabeledWimsParser.TYPE, 0);
};

AnswertypeContext.prototype.EQUAL = function() {
    return this.getToken(LabeledWimsParser.EQUAL, 0);
};

AnswertypeContext.prototype.ANSWERTYPE = function() {
    return this.getToken(LabeledWimsParser.ANSWERTYPE, 0);
};

AnswertypeContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

AnswertypeContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitAnswertype(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.AnswertypeContext = AnswertypeContext;

LabeledWimsParser.prototype.answertype = function() {

    var localctx = new AnswertypeContext(this, this._ctx, this.state);
    this.enterRule(localctx, 50, LabeledWimsParser.RULE_answertype);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 247;
        this.match(LabeledWimsParser.OB);
        this.state = 248;
        this.match(LabeledWimsParser.TYPE);
        this.state = 249;
        this.match(LabeledWimsParser.EQUAL);
        this.state = 250;
        this.match(LabeledWimsParser.ANSWERTYPE);
        this.state = 251;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function ConditionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_condition;
    return this;
}

ConditionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ConditionContext.prototype.constructor = ConditionContext;

ConditionContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

ConditionContext.prototype.sentence = function() {
    return this.getTypedRuleContext(SentenceContext,0);
};

ConditionContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

ConditionContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitCondition(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.ConditionContext = ConditionContext;

LabeledWimsParser.prototype.condition = function() {

    var localctx = new ConditionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 52, LabeledWimsParser.RULE_condition);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 253;
        this.match(LabeledWimsParser.OB);
        this.state = 254;
        this.sentence();
        this.state = 255;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function FeedbackContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_feedback;
    return this;
}

FeedbackContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FeedbackContext.prototype.constructor = FeedbackContext;

FeedbackContext.prototype.FEEDBACK = function() {
    return this.getToken(LabeledWimsParser.FEEDBACK, 0);
};

FeedbackContext.prototype.OB = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.OB);
    } else {
        return this.getToken(LabeledWimsParser.OB, i);
    }
};


FeedbackContext.prototype.relationalExpression = function() {
    return this.getTypedRuleContext(RelationalExpressionContext,0);
};

FeedbackContext.prototype.CB = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LabeledWimsParser.CB);
    } else {
        return this.getToken(LabeledWimsParser.CB, i);
    }
};


FeedbackContext.prototype.sentence = function() {
    return this.getTypedRuleContext(SentenceContext,0);
};

FeedbackContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitFeedback(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.FeedbackContext = FeedbackContext;

LabeledWimsParser.prototype.feedback = function() {

    var localctx = new FeedbackContext(this, this._ctx, this.state);
    this.enterRule(localctx, 54, LabeledWimsParser.RULE_feedback);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 257;
        this.match(LabeledWimsParser.FEEDBACK);
        this.state = 258;
        this.match(LabeledWimsParser.OB);
        this.state = 259;
        this.relationalExpression(0);
        this.state = 260;
        this.match(LabeledWimsParser.CB);
        this.state = 261;
        this.match(LabeledWimsParser.OB);
        this.state = 262;
        this.sentence();
        this.state = 263;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function SolutionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_solution;
    return this;
}

SolutionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
SolutionContext.prototype.constructor = SolutionContext;

SolutionContext.prototype.SOLUTION = function() {
    return this.getToken(LabeledWimsParser.SOLUTION, 0);
};

SolutionContext.prototype.OB = function() {
    return this.getToken(LabeledWimsParser.OB, 0);
};

SolutionContext.prototype.sentence = function() {
    return this.getTypedRuleContext(SentenceContext,0);
};

SolutionContext.prototype.CB = function() {
    return this.getToken(LabeledWimsParser.CB, 0);
};

SolutionContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitSolution(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.SolutionContext = SolutionContext;

LabeledWimsParser.prototype.solution = function() {

    var localctx = new SolutionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 56, LabeledWimsParser.RULE_solution);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 265;
        this.match(LabeledWimsParser.SOLUTION);
        this.state = 266;
        this.match(LabeledWimsParser.OB);
        this.state = 267;
        this.sentence();
        this.state = 268;
        this.match(LabeledWimsParser.CB);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function PreContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_pre;
    return this;
}

PreContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PreContext.prototype.constructor = PreContext;

PreContext.prototype.title = function() {
    return this.getTypedRuleContext(TitleContext,0);
};

PreContext.prototype.language = function() {
    return this.getTypedRuleContext(LanguageContext,0);
};

PreContext.prototype.author = function() {
    return this.getTypedRuleContext(AuthorContext,0);
};

PreContext.prototype.email = function() {
    return this.getTypedRuleContext(EmailContext,0);
};

PreContext.prototype.computeanswer = function() {
    return this.getTypedRuleContext(ComputeanswerContext,0);
};

PreContext.prototype.precision = function() {
    return this.getTypedRuleContext(PrecisionContext,0);
};

PreContext.prototype.rangedeclaration = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(RangedeclarationContext);
    } else {
        return this.getTypedRuleContext(RangedeclarationContext,i);
    }
};

PreContext.prototype.integer = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(IntegerContext);
    } else {
        return this.getTypedRuleContext(IntegerContext,i);
    }
};

PreContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitPre(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.PreContext = PreContext;

LabeledWimsParser.prototype.pre = function() {

    var localctx = new PreContext(this, this._ctx, this.state);
    this.enterRule(localctx, 58, LabeledWimsParser.RULE_pre);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 270;
        this.title();
        this.state = 272; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 271;
            this.match(LabeledWimsParser.T__0);
            this.state = 274; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===LabeledWimsParser.T__0);
        this.state = 276;
        this.language();
        this.state = 278; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 277;
            this.match(LabeledWimsParser.T__0);
            this.state = 280; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===LabeledWimsParser.T__0);
        this.state = 282;
        this.author();
        this.state = 284; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 283;
            this.match(LabeledWimsParser.T__0);
            this.state = 286; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===LabeledWimsParser.T__0);
        this.state = 288;
        this.email();
        this.state = 290; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 289;
            this.match(LabeledWimsParser.T__0);
            this.state = 292; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===LabeledWimsParser.T__0);
        this.state = 294;
        this.computeanswer();
        this.state = 296; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 295;
            this.match(LabeledWimsParser.T__0);
            this.state = 298; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===LabeledWimsParser.T__0);
        this.state = 300;
        this.precision();
        this.state = 302; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 301;
            this.match(LabeledWimsParser.T__0);
            this.state = 304; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===LabeledWimsParser.T__0);
        this.state = 314;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===LabeledWimsParser.RANGE) {
            this.state = 306;
            this.rangedeclaration();
            this.state = 308; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            do {
                this.state = 307;
                this.match(LabeledWimsParser.T__0);
                this.state = 310; 
                this._errHandler.sync(this);
                _la = this._input.LA(1);
            } while(_la===LabeledWimsParser.T__0);
            this.state = 316;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 325;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===LabeledWimsParser.INTEGER) {
            this.state = 317;
            this.integer();
            this.state = 319; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            do {
                this.state = 318;
                this.match(LabeledWimsParser.T__0);
                this.state = 321; 
                this._errHandler.sync(this);
                _la = this._input.LA(1);
            } while(_la===LabeledWimsParser.T__0);
            this.state = 327;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function PeriContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_peri;
    return this;
}

PeriContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PeriContext.prototype.constructor = PeriContext;

PeriContext.prototype.statement = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(StatementContext);
    } else {
        return this.getTypedRuleContext(StatementContext,i);
    }
};

PeriContext.prototype.answer = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(AnswerContext);
    } else {
        return this.getTypedRuleContext(AnswerContext,i);
    }
};

PeriContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitPeri(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.PeriContext = PeriContext;

LabeledWimsParser.prototype.peri = function() {

    var localctx = new PeriContext(this, this._ctx, this.state);
    this.enterRule(localctx, 60, LabeledWimsParser.RULE_peri);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 336;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===LabeledWimsParser.STATEMENT) {
            this.state = 328;
            this.statement();
            this.state = 330; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            do {
                this.state = 329;
                this.match(LabeledWimsParser.T__0);
                this.state = 332; 
                this._errHandler.sync(this);
                _la = this._input.LA(1);
            } while(_la===LabeledWimsParser.T__0);
            this.state = 338;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 347;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===LabeledWimsParser.ANSWER) {
            this.state = 339;
            this.answer();
            this.state = 341; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            do {
                this.state = 340;
                this.match(LabeledWimsParser.T__0);
                this.state = 343; 
                this._errHandler.sync(this);
                _la = this._input.LA(1);
            } while(_la===LabeledWimsParser.T__0);
            this.state = 349;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function PostContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_post;
    return this;
}

PostContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PostContext.prototype.constructor = PostContext;

PostContext.prototype.feedback = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(FeedbackContext);
    } else {
        return this.getTypedRuleContext(FeedbackContext,i);
    }
};

PostContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitPost(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.PostContext = PostContext;

LabeledWimsParser.prototype.post = function() {

    var localctx = new PostContext(this, this._ctx, this.state);
    this.enterRule(localctx, 62, LabeledWimsParser.RULE_post);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 358;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===LabeledWimsParser.FEEDBACK) {
            this.state = 350;
            this.feedback();
            this.state = 352; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            do {
                this.state = 351;
                this.match(LabeledWimsParser.T__0);
                this.state = 354; 
                this._errHandler.sync(this);
                _la = this._input.LA(1);
            } while(_la===LabeledWimsParser.T__0);
            this.state = 360;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


function WimsoefContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LabeledWimsParser.RULE_wimsoef;
    return this;
}

WimsoefContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
WimsoefContext.prototype.constructor = WimsoefContext;

WimsoefContext.prototype.pre = function() {
    return this.getTypedRuleContext(PreContext,0);
};

WimsoefContext.prototype.peri = function() {
    return this.getTypedRuleContext(PeriContext,0);
};

WimsoefContext.prototype.post = function() {
    return this.getTypedRuleContext(PostContext,0);
};

WimsoefContext.prototype.accept = function(visitor) {
    if ( visitor instanceof LabeledWimsVisitor ) {
        return visitor.visitWimsoef(this);
    } else {
        return visitor.visitChildren(this);
    }
};




LabeledWimsParser.WimsoefContext = WimsoefContext;

LabeledWimsParser.prototype.wimsoef = function() {

    var localctx = new WimsoefContext(this, this._ctx, this.state);
    this.enterRule(localctx, 64, LabeledWimsParser.RULE_wimsoef);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 361;
        this.pre();
        this.state = 362;
        this.peri();
        this.state = 363;
        this.post();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


LabeledWimsParser.prototype.sempred = function(localctx, ruleIndex, predIndex) {
	switch(ruleIndex) {
	case 11:
			return this.expression_sempred(localctx, predIndex);
	case 12:
			return this.relationalExpression_sempred(localctx, predIndex);
    default:
        throw "No predicate with index:" + ruleIndex;
   }
};

LabeledWimsParser.prototype.expression_sempred = function(localctx, predIndex) {
	switch(predIndex) {
		case 0:
			return this.precpred(this._ctx, 9);
		case 1:
			return this.precpred(this._ctx, 8);
		case 2:
			return this.precpred(this._ctx, 7);
		default:
			throw "No predicate with index:" + predIndex;
	}
};

LabeledWimsParser.prototype.relationalExpression_sempred = function(localctx, predIndex) {
	switch(predIndex) {
		case 3:
			return this.precpred(this._ctx, 5);
		case 4:
			return this.precpred(this._ctx, 4);
		case 5:
			return this.precpred(this._ctx, 3);
		default:
			throw "No predicate with index:" + predIndex;
	}
};


exports.LabeledWimsParser = LabeledWimsParser;
