"use strict";

/** Class representing a Answer. it has access to user answer and goodanswer and manages the display of the input UI and its own feedback
* @property {String} title
* @property {wimsElement} userAnswer    - userAnswer is a WimsElement. Exercise->button "checkanswers" feeds it.
* @property {wimsElement} goodAnswer    - goodAnswer is a WimsElement defined by \answer in the OEF. Feed when Peri Parsing
* @property {boolean} sc_reply          - boolean (set to true when goodAnswer value == userAnswer value)
* @property {dom} inputField            - generaly dom object (may be more general in future to allow other UI
* @property {dom} feedbackField         - generaly dom object (idem)
*/
class WimsLexerParser {
  /**
  * Create a WimsAnswer
  * @constructor
  * @param {string} title - The title of the book.
  * @param {string} wims - The author of the book.
  */
  constructor(exercise,input){
    this.exercise = exercise;
    this.input = input;
    this.errorLexerParser = [];
  }

  /**
  * Init or reinit (for a second parse) ANTLR4 Lexer and Parser
  */
  initLexerParser(){
    //this.chars = antlr4.CharStreams.fromBuffer(input,'utf-8');
    DEBUG = document.getElementById("debugmode").checked;
    this.chars = antlr4.CharStreams.fromBuffer(this.input,'utf-8');
    this.lexer = new MyWimsLexer(this.chars);

    this.tokens  = new antlr4.CommonTokenStream(this.lexer);
    this.parser = new MyWimsParser(this.tokens);
    this.visitor = new LabeledWimsVisitor(this.exercise,this.parser);
   //this.errorLexerParser = new WimsLexerParserErrorListener(this.lexer,this.parser); //Doesn't work whereas it worked in the index.js (probably because I tried to include it into an OO)
    this.parser.buildParseTrees = true;
  }

  /**
  * Parse pre part of OEF
  */
  parsePre(){
    var tree = this.parser.pre();
    this.visitor.visitPre(tree);
    appendDEBUG("Pre OEF Tree in LISP form:","p");
    appendDEBUG(tree.toStringTree(this.parser.ruleNames),"p");
  }

  /**
  * Parse peri part of OEF
  */
  parsePeri(){
    var tree = this.parser.peri();
    this.visitor.visitPeri(tree);
    appendDEBUG("Peri OEF Tree in LISP form:","p");
    appendDEBUG(tree.toStringTree(this.parser.ruleNames),"p");
  }


getPostTree(){
    //Store the tree in a properties to be able to visit it several time through parsePost
    this.postTree = this.parser.post();

    appendDEBUG("Post OEF Tree in LISP form:","p");
    appendDEBUG(this.postTree.toStringTree(this.parser.ruleNames),"p");
//    this.lexer.reset();
//    this.parser.reset();
  }

  /**
  * Visit postTree, representing the OEF Post part
  */
  visitPostTree(){
    this.visitor.visitPost(this.postTree);
  }
}

module.exports = {
  WimsLexerParser: WimsLexerParser
}
