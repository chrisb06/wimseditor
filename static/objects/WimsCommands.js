"use strict";

/** Class representing commands executable by wims.
* Most of methods are static and independent to the context
*/
class WimsCommands {
  /**
  * Create a WimsElement.
  * @constructor
  */
  //TO DO : use declaration of properties or and with # prefix to strenght private properties in objects (must be available in next ECMA version)
  //Usage : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes
  constructor(){
    appendDEBUG("WimsCommands constructor");
    // this.defineProperty(writable:false);
  }

  /**
  * Return a random between two integers
  *
  */
  static getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }



}



module.exports = {
  WimsCommands: WimsCommands,
}
