"use strict";

/** Class representing a WimsElement.
* Attributes:
* - value
*/
class WimsElement {
  /**
  * Create a WimsElement.
  * @constructor
  */
  //TO DO : use declaration of properties or and with # prefix to strenght private properties in objects (must be available in next ECMA version)
  //Usage : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes
  constructor(){
    appendDEBUG("WimsElement constructor");
   // this.defineProperty(writable:false);
  }

 /**
  * Get the value.
  * @return {object} The value.
  */
  getValue(){
    return this.value;
  }

  toSVG(){
  }
}

/**
 * Class representing a Numeric.
 * @extends WimsElement
 */
class WimsNumeric extends WimsElement {
  /**
  * Create a WimsNumeric.
  * @constructor
  */
  constructor(){
  super();
  appendDEBUG("WimsNumeric constructor");
  }

  /**
  * Set the value.
  * @param {number} numeric value
  */
  setValue(numeric){
    this.value = new Number(numeric);
  }

  toSVG(){
  }

  /**
  * generateHTML node
  * @return {node} The <p> with this.toString().
  */
  toHTML(){
    var node = document.createElement("p");
    var textnode = document.createTextNode(this.toString());
    node.appendChild(textnode);
    return node;
  }
}

/**
 * Class representing a Integer.
 * @extends WimsNumeric
 */
class WimsInteger extends WimsNumeric {
  /**
  * Create a WimsInteger.
  * @constructor
  * @param {number} integer - The integer value.
  * TO DO : add constraints to manage Number as WimsInteger
  */
  constructor(integer){
    super();
    this.value = new Number(integer);
    appendDEBUG("WimsInteger constructor value input: "+ this.value + " output: "+ this.value);
  }
}

/**
 * Class representing a Rational.
 * @extends WimsNumeric
 */
class WimsRational extends WimsNumeric {
  /**
  * Create a WimsRational.
  * @constructor
  * @param {fraction} value - The fraction value.
  */
  constructor(value){
    super();
    //Value could be expressed as ('1/3') or (2,3) or ('0.(3)'). See usage : https://mathjs.org/docs/datatypes/fractions.html
    this.value = new math.fraction(value);
    appendDEBUG("WimsRational constructor value input: "+ value + " output: "+ this.value);

  }
}

/**
 * Class representing a Real.
 * @extends WimsNumeric
 */
class WimsReal extends WimsNumeric {
  /**
  * Create a WimsReal.
  * @constructor
  * @param {fraction} value - The fraction value.
  */
  constructor(value){
    super();
    this.value = new Number(value);
    appendDEBUG("WimsReal constructor value input: "+ value + " output: "+ this.value);
  }
}

/**
 * Class representing a Complex.
 * @extends WimsNumeric
 */
class WimsComplex extends WimsNumeric {
  /**
  * Create a WimsComplex. x+yi
  * @constructor
  * @param {integer} real - The real value.
  * @param {fraction} imaginary - The imaginary value.
  */
  constructor(valueRe,valueIm=0){
    super();
    //Usage : https://mathjs.org/docs/datatypes/complex_numbers.html
    this.value = new math.complex(valueRe,valueIm);
    appendDEBUG("WimsReal constructor value input: "+ value + " output: "+ this.value);
  }
}

/**
 * Class representing a String.
 * @extends WimsNumeric
 */
class WimsString extends WimsElement{
  /**
  * Create a WimsString.
  * @constructor
  * @param {string} value - The string value.
  */
  constructor(string){
    super();
    this.value = string;
    appendDEBUG("WimsString constructor value input: "+ string + " output: "+ this.value);
  }

 /**
  * Generate HTML node
  * @return {node} The <p> with this.value which is a string.
  */
  toHTML(){
    var node = document.createElement("p");
    var textnode = document.createTextNode(this.getValue());
    node.appendChild(textnode);
    return node;
  }
}

module.exports = {
  WimsElement: WimsElement,
  WimsNumeric: WimsNumeric,
  WimsInteger: WimsInteger,
  WimsRational: WimsRational,
  WimsReal: WimsReal,
  WimsComplex: WimsComplex,
  WimsString : WimsString
}
