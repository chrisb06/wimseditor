"use strict";


var AnnotatingErrorListener = function(annotations) {
        antlr4.error.ErrorListener.call(this);
        this.annotations = annotations;
        return this;
};

AnnotatingErrorListener.prototype = Object.create(antlr4.error.ErrorListener.prototype);
AnnotatingErrorListener.prototype.constructor = AnnotatingErrorListener;

AnnotatingErrorListener.prototype.syntaxError = function(recognizer, offendingSymbol, line, column, msg, e) {
    this.annotations.push({
        row: line,
        column: column,
        text: msg,
        type: "error"
    });
 };


class WimsLexerParserErrorListener {
  constructor(parser, lexer){
    ERROR = document.getElementById("errormode").checked;

    this.annotations=[];
    this.annotations = new AnnotatingErrorListener(this.annotations);
    lexer.removeErrorListeners();
    lexer.addErrorListener(this.errorLexerParserListener);
    parser.removeErrorListeners();
    parser.addErrorListener(this.errorLexerParserListener);

    this.appendERROR(this.annotations);
  }


  appendERROR(){
     appendDEBUG("huston");
    if (ERROR)
    {
        while (this.annotations.length > 0)
        {
        var node = document.createElement("p");
        var myerror = this.annotations.pop();
        var textnode = document.createTextNode("Error r:"+myerror['row']+ " c"+myerror['column']+ " "+myerror['text']);
        node.appendChild(textnode);
        document.getElementById("error").appendChild(node);
        }
    }
  }
}

module.exports = {
  WimsLexerParserErrorListener: WimsLexerParserErrorListener
}
