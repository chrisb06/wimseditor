"use strict";

/*
var {WimsNumeric} = require('../objects/WimsElement');
var {WimsInteger} = require('../objects/WimsElement');
var {WimsString} = require('../objects/WimsElement');
*/

/** Class representing a Answer. it has access to user answer and goodanswer and manages the display of the input UI and its own feedback
* @property {String} title
* @property {wimsElement} userAnswer    - userAnswer is a WimsElement. Exercise->button "checkanswers" feeds it.
* @property {wimsElement} goodAnswer    - goodAnswer is a WimsElement defined by \answer in the OEF. Feed when Peri Parsing
* @property {Float} sc_reply          - float (set to true when goodAnswer value == userAnswer value)
* @property {dom} inputField            - generaly dom object (may be more general in future to allow other UI
* @property {dom} feedbackField         - generaly dom object (idem)
*/
class WimsAnswer {
  /**
  * Create a WimsAnswer
  * @constructor
  * @param {string} title - answer label.
  * @param {string} goodAnswer - good answer.
  * @param {string} sc_reply - .
  */
  constructor(title,wimsElement){
    appendDEBUG("WimsAnswer constructor");
    this.title=title;
    this.goodAnswer=wimsElement;
    this.sc_reply=0;
    this.id=0;
  }

  getTitle(){
    return this.title;
  }

  getId(){
    return this.id;
  }

  getInputedUserAnswer(){
    return this.inputField.value;
  }

  getUserAnswer(){
    return this.userAnswer;
  }

  getGoodAnswer(){
    return this.goodAnswer;
  }

  getSc_reply(){
    return this.sc_reply;
  }

  setId(identifier){
    this.id=identifier;
  }

  setUserAnswer(){
    this.userAnswer.setValue(this.getInputedUserAnswer());
    if (parseInt(this.getUserAnswer().getValue()) == parseInt(this.getGoodAnswer().getValue())){
      this.sc_reply=1;
    } else {
      this.sc_reply=0;
    }
  }

  isGoodAnswer(){
    var bool;
    appendDEBUG("isGoodAnswer -"+this.getUserAnswer().getValue()+ "- and -" + this.getGoodAnswer().getValue()+"-");
//TO DO :A REVOIR LA COMPARAISON DES VALUERS DES WIMS ELEMENTS OBJETS
    if (parseInt(this.getUserAnswer().getValue()) == parseInt(this.getGoodAnswer().getValue())){
      return true;
    } else {
      return false;
    }
  }


  displayResultAnswerHTML(){
    this.cleanFeedback();
    if (this.goodAnswer.getValue() == this.getInputedUserAnswer())
    {
      this.feedbackField.appendChild(document.createTextNode("Right 👍🏅"));
    } else {
       this.feedbackField.appendChild(document.createTextNode("False 😬"));
    }
  }

  static methode_static() {
  }
}

/**
 * Class representing a Numeric Answer.
 * @extends WimsAnswer
 */
class WimsNumericAnswer extends WimsAnswer {

  constructor(title,wimsElement){
    super(title,wimsElement);
    this.userAnswer = new WimsNumeric(); // Instantiate an empty object following the same type than this
    appendDEBUG("WimsNumericAnswer constructor");
  }

  toHTML(){
    /* answer container */
    var answer = document.createElement("div");
    answer.setAttribute('class', 'oef_anwser');

    //answer label
    var label = document.createElement("label");
    label.setAttribute('for', 'reply'+this.id);
    label.appendChild(document.createTextNode(this.getTitle()));
    answer.appendChild(label);
    //generate inputField
    this.inputField = document.createElement("input");
    this.inputField.setAttribute('type', 'number');
    this.inputField.setAttribute('id', 'reply'+this.id);
    answer.appendChild(this.inputField);
    //generate feedbackField
    this.feedbackField = document.createElement("span");
    feedbackField.setAttribute('class', 'oef_feedback');
    answer.appendChild(this.feedbackField);
    return answer;
  }

  cleanFeedback(){
    this.feedbackField.innerHTML = '';
  }


  static methode_static() {

  }
}



module.exports = {
  WimsAnswer: WimsAnswer,
  WimsNumericAnswer: WimsNumericAnswer
}
