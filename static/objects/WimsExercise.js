"use strict";

/** Class representing a Answer.
* @property {String} title                    - the title of the exercise
* @property {CodeLang} mainLanguage           - to be done : choose main_language of exercise
* @property {String} author                   - author of the exercise
* @property {Boolean} computeAnswer           - is 3+4 accepted as an Answer or only 7
* @property {Integer} precision               - to be detailed
* @property {WimsRange} range                 - to be adapted and detailed
* @property {Array_WimsElement} variables[]   - dictionnary of exercise's variables {key:varname : wimsElement}
* @property {Array_WimsElement} statements[]  - List of wimsElement ordorned composing the whole sentence
* @property {Array_WimsAnswer} answers[]      - List of Answser
* @property {WimsHint} hint                   - Not yet implemented
* @property {WimsHelp} help                   - Not yet implemented
* @property {WimsCondition} conditions[]      - Not yet implemented
* @property {Array} feedbacks[]               - List of feedback sentence if feedback Test is true
* @property {WimsSolution} WimsSolution       - Not yet implemented
*/

class WimsExercise {
  /**
  * Create a Exercise.
  * @constructor
  * @param {Lexer} refLexer    - The instance of current Lexer.
  * @param {Parser} refParser  - The instance of current Parser.
  */
  constructor(input, title ="",variables ={},statements =[],answers =[]){
    this.input = input;
    this.title      =title;
    this.variables  =variables;
    this.statements =statements;
    this.answers    =answers;
    this.feedbacks  =[];
    appendDEBUG("WimsExercise constructor: ");
   //Init lexer and parser
    this.wimsLexerParser = new WimsLexerParser(this,this.input);
    this.wimsLexerParser.initLexerParser();

    this.wimsLexerParser.parsePre();
    this.generatePreHTML();
    this.wimsLexerParser.parsePeri();
    this.generatePeriHTML();
    this.generateAnswersHTML()
    this.generateSubmitButton();
    this.wimsLexerParser.getPostTree();
  }
  /**
  * Set Title of Exercise
  */
  setTitle(title){
    this.title=title;
  }

  getTitle(){
    return this.title;
  }

  /**
  * Get Variable named "key"
  * @param {String} key                  - The name of the variable.
  * @return {WimsElement} variable       - return the WimsElement
  */
  getVariable(key){
    appendDEBUG("Exercise getVariable(key): "+key+ " value:"+this.variables[key].getValue());
    return this.variables[key];
  }

  /**
  * Add Variable in the Dictionnary of variables
  * @param {String} key                  - The name of the variable.
  * @param {WimsElement} wimsElement     - The variable encapsulated in a WimsElement object.
  */
  addVariable(key, wimsElement){
    this.variables[key]=wimsElement;
    //appendDEBUG("Exercise addVariable: "+key+" getValue():"+wimsElement.getValue());
  }

  /**
  * Get all statements
  * @return {Array_WimsElement} statements[]     - return the list of statements of exercise (each of item list is a WimsElement).
  */
  getStatements(){
    return this.statements;
  }

  /**
  * Add Statement
  * @param {WimsElement} wimsElement     - add wimsELement into the Array statements[].
  */
  addStatement(wimsElement){
    this.statements.push(wimsElement);
    appendDEBUG("Exercise addStatement: "+ wimsElement.getValue());
  }

  /**
  * Get all answers
  * @return {Array_WimsAnswer} answers[]     - return the list of answers of exercise (each of item list is a WimsAnswer).
  */
  getAnswers(){
    return this.answers;
  }

  /**
  * Get one Answer
  * @param {Integer} i
  * @return {WimsAnswer} wimsAnswer     - return the item i of the Array answers[]
  */
  getAnswer(i){
    return this.answers[i];
  }

  /**
  * Add an Answer
  * @param {WimsAnswer} wimsAnswer - add wimsAnswer into the Array answers[].
  */
  addAnswer(wimsAnswer){
    this.answers.push(wimsAnswer);
    wimsAnswer.setId(this.answers.length);
    appendDEBUG("Exercise addAnswer: "+wimsAnswer.getTitle());
  }

  /**
  * Get all feedbacks
  * @return {} wimsAnswer     - return the item i of the Array feedbacks[] (each of item list is a String)
  */
  getFeedbacks(){
    return this.feedbacks;
  }

  /**
  * Add a feedback
  * @param {String} string - add a feedback in String into the Array feedbacks[].
  */
  addFeedback(string){
    this.feedbacks.push(string);
    //appendDEBUG("Exercise addVariable: "+key+" getValue():"+wimsElement.getValue());
  }

////////////////////////////
  /**
  * Generate Pre OEF in HTML wihtin the statement DIV
  */
  generatePreHTML(){
    var node = document.createElement("H3");
    var textnode = document.createTextNode(this.title);
    node.appendChild(textnode);
    document.getElementById("statement").appendChild(node);
  }

  /**
  * Generate Peri OEF in HTML wihtin the statetement DIV
  */
  generatePeriHTML()
  {
    var i;
    for(i=0 ; i<this.statements.length; i++){
      appendDEBUG("generateStatementsHTML:"+this.statements[i].getValue());
      document.getElementById("statement").appendChild(this.statements[i].toHTML());
    }

  }

  /**
  * Generate Answers OEF in HTML wihtin the answer DIV
  */
  generateAnswersHTML()
  {
    var i;
    for(i=0 ; i < this.answers.length; i++){
      document.getElementById("answer").appendChild(this.answers[i].toHTML());
    }
  }

  /**
  * Generate the submit button with listener (click triggers checkanswers)
  */
  generateSubmitButton(){
    var button = document.createElement('input');
    button.setAttribute('type','submit');
    button.setAttribute('value','Check answer');
    button.addEventListener("click",this.checkAnswers.bind(this)); //to pass this as the context : Ref : https://stackoverflow.com/questions/1338599/the-value-of-this-within-the-handler-using-addeventlistener
    document.getElementById("answer").appendChild(button);
  }

checkAnswers(event){
    appendDEBUG("checkAnswers");

    this.cleanGlobalFeedback();
    //feed variables[] with all replies coming from User within answers[] ( type WimsAnswer)
    var i;
    for(i=0 ; i < exercise.answers.length; i++){
      this.answers[i].setUserAnswer(); //put inputed answer into wimsanswer.answer
    }
    this.wimsLexerParser.visitPostTree();
    this.generateFeedbacks();
    this.generateGlobalFeedbacks();
}
  /**
  * Launch each displayResultAnswerHTML() of each WimsAnswer item containing into answers[]
  */
  generateFeedbacks(){
    var i;
    for(i=0 ; i < this.answers.length; i++){
     this.answers[i].displayResultAnswerHTML();
    }
  }

  /**
  * Generate Feedbacks of OEF in HTML within the feedback DIV
  */
  generateGlobalFeedbacks() {
    var i;
    for(i=0 ; i < this.feedbacks.length; i++){
      var node = document.createElement("p");
      var textnode = document.createTextNode(this.feedbacks[i]);
      node.appendChild(textnode);
      document.getElementById("feedback").appendChild(node);
    }
  }

  /**
  * Empty feedback DIV
  */
  cleanGlobalFeedback(){
    this.feedbacks=[];
    document.getElementById("feedback").innerHTML="";
  }


  static methode_static(exo) {
      appendDEBUG("methode_static");
  }
}



module.exports = {
  WimsExercise: WimsExercise
}
